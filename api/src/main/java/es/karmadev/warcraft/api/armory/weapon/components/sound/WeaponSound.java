package es.karmadev.warcraft.api.armory.weapon.components.sound;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;

/**
 * Represents a weapon sound
 */
public interface WeaponSound extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.SOUND;
    }

    /**
     * Get the soundset this sound
     * pertains to
     *
     * @return the sound soundset
     */
    WeaponSoundSet getSoundSet();

    /**
     * Get when the sound should be
     * played
     *
     * @return the sound time
     */
    String getTime();

    /**
     * Get when the sound should be
     * played, relative to the sound
     * specific time
     *
     * @return the exact time when the sound
     * should be played
     */
    long getTimeExact();

    /**
     * Get the sound name to play
     *
     * @return the sound name
     */
    String getSoundName();

    /**
     * Get the sound pitch
     *
     * @return the sound pitch
     */
    float getPitch();

    /**
     * Get the sound volume
     *
     * @return the sound volume
     */
    float getVolume();
}
