package es.karmadev.warcraft.api.util.uuns;

import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.processor.conversor.FieldTransformer;

/**
 * {@link UUNS} transformer for {@link es.karmadev.api.kson.processor.field.Transformer}
 */
public class UUNSJsonTransformer extends FieldTransformer<UUNS, JsonObject> {


    /**
     * Transform the element into a
     * string
     *
     * @param element the element
     * @return the element string element
     */
    @Override
    public JsonObject transformToValue(final Object element) {
        if (!(element instanceof UUNS)) return null;

        UUNS uuns = (UUNS) element;
        JsonObject object = JsonObject.newObject("", "uuns");
        object.put("path", uuns.getPath());
        object.put("value", uuns.getValue());

        return object;
    }

    /**
     * Transform the string into the
     * element
     *
     * @param element the element
     * @return the transformed element
     */
    @Override
    public UUNS transformFromValue(final Object element) {
        if (!(element instanceof JsonObject)) return null;

        JsonObject object = (JsonObject) element;
        if (!object.hasChild("path") || !object.hasChild("value"))
            return null;

        JsonInstance pathInstance = object.getChild("path");
        JsonInstance valueInstance = object.getChild("instance");

        if (!pathInstance.isNativeType() || !valueInstance.isNativeType())
            return null;

        JsonNative path = pathInstance.asNative();
        JsonNative value = valueInstance.asNative();

        if (!path.isString() || !value.isString())
            return null;

        return UUNS.create(
                path.getString(),
                value.getString()
        );
    }

    /**
     * Build a value out of a json instance
     *
     * @param instance the instance
     * @return the element
     */
    @Override
    public JsonObject fromElement(final JsonInstance instance) {
        if (!instance.isObjectType()) return null;
        return instance.asObject();
    }
}
