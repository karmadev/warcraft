package es.karmadev.warcraft.api.world.level;

import es.karmadev.warcraft.api.world.level.player.Client;

/**
 * Represents a team score
 */
@SuppressWarnings("unused")
public interface TeamScore {

    /**
     * Get the team score
     *
     * @return the team total score
     */
    float getScore();

    /**
     * Get the score a client issued on
     * this team
     *
     * @param client the client
     * @return the score the client
     * issued
     */
    float getScore(final Client client);

    /**
     * Get the team score leaderboard
     *
     * @param limit the leaderboard limit
     * @return the leaderboard
     */
    Client[] getLeaderBoard(final int limit);

    /**
     * Get the three best players in the
     * team
     *
     * @return the three best players
     */
    default Client[] getLeaderBoard() {
        return getLeaderBoard(3);
    }

    /**
     * Get the team MVP
     *
     * @return the MVP
     */
    default Client getMVP() {
        Client[] data = getLeaderBoard(1);
        if (data.length == 0) return null;

        return data[0];
    }

    /**
     * Add score to the team
     *
     * @param client the client who issued
     *               the score
     * @param score the score amount
     */
    void addScore(final Client client, final float score);

    /**
     * Add assisted score to the
     * team
     *
     * @param clients the clients involved in the
     *                score
     * @param score the score amount, which gets shared
     *              between all the clients
     */
    void assistedScore(final Client[] clients, final float score);
}
