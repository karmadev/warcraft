package es.karmadev.warcraft.api.script;

/**
 * Represents a lua package
 * access point. All classes on the
 * sub-package or package of this class
 * will claim the same allow
 * policy, but still respecting the
 * {@link LuaAccess}
 */
public abstract class LuaPackage {

    /**
     * Get if the package allows
     * lua API access
     *
     * @return if the package allows
     * lua access
     */
    public abstract boolean isAllowed();

    /**
     * Register the lua package into
     * the script class loader
     */
    public final void register() {
        ScriptClassLoader.packages.add(this);
    }
}