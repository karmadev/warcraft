package es.karmadev.warcraft.api.armory.weapon.components.model;

/**
 * Supported model types
 */
public enum ModelType {
    /**
     * Custom model
     */
    CUSTOM,
    /**
     * Minecraft model
     */
    MINECRAFT
}
