package es.karmadev.warcraft.api.world.level.game.tick;

import es.karmadev.warcraft.api.world.level.game.Game;

import java.util.function.Supplier;

/**
 * Represents a game tick manager
 * for a {@link Game}
 */
public interface GameTickManager {

    /**
     * Get the current tick
     * in the game
     *
     * @return the current game tick
     */
    long getTick();

    /**
     * Get the next tick
     * in the game
     *
     * @return the next game tick
     */
    long getNextTick();

    /**
     * Execute the game tick manage
     * tick function. This task should only
     * run tick tasks and change current
     * tick
     */
    void tickFunction();

    /**
     * Get if the game is currently synchronized
     * with the game tick
     *
     * @return if the game is synchronized
     */
    boolean isSynchronized();

    /**
     * Adds an interval to
     * the game
     *
     * @param interval the interval
     * @param task the interval task
     */
    void addInterval(final long interval, final Runnable task);

    /**
     * Schedule an action for the
     * next tick in the game
     *
     * @param tickTask the task to run
     */
    default void nextTick(final Runnable tickTask) {
        this.nextTick(0, new FutureGameTick() {
            @Override
            public void run() {
                tickTask.run();
            }
        });
    }

    /**
     * Schedule an action for the
     * next tick in the game
     *
     * @param tickTask the task to run
     */
    default void nextTick(final Supplier<Runnable> tickTask) {
        this.nextTick(tickTask.get());
    }

    /**
     * Schedule an action for the
     * next tick in the game
     *
     * @param tickTask the task to run
     */
    default void nextTick(final FutureGameTick tickTask) {
        this.nextTick(0, tickTask);
    }

    /**
     * Schedule an action for the
     * next tick in the game
     *
     * @param jumps the amount of jumps for
     *              the next tick
     * @param tickTask the task to run
     */
    void nextTick(final long jumps, final FutureGameTick tickTask);

    /**
     * Delay the tick prosecution
     */
    default void delayTick() {
        this.delayTick(1);
    }

    /**
     * Delay the tick prosecution
     *
     * @param jumps the amount of tick
     *              jumps to perform
     */
    void delayTick(final long jumps);
}