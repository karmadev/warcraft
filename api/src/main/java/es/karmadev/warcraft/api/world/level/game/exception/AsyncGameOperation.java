package es.karmadev.warcraft.api.world.level.game.exception;

/**
 * This exception is thrown when an operation
 * is performed for a game out of tick rate
 */
public final class AsyncGameOperation extends RuntimeException {

    /**
     * Create the exception
     *
     * @param task the task name
     * @param reason the exception reason
     */
    public AsyncGameOperation(final String task, final String reason) {
        super("Cannot perform task " + task + " out of tick rate. " + reason);
    }
}
