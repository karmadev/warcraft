package es.karmadev.warcraft.api.armory.weapon.type;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.components.explosion.WeaponExplosion;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;

/**
 * Represents an explosive weapon
 */
public interface ExplosiveWeapon extends Weapon {

    /**
     * Get the weapon type
     *
     * @return the weapon type
     */
    @Override
    default WeaponType getWeaponType() {
        return WeaponType.EXPLOSIVE;
    }

    /**
     * Get the weapon model
     *
     * @return the weapon model
     */
    WeaponModel getModel();

    /**
     * Get the explosive weapon
     * explosion
     *
     * @return the explosion
     */
    WeaponExplosion getExplosion();

    /**
     * Get the weapon visibility
     * scale
     *
     * @return the visibility scale
     */
    double getVisibilityScale();

    /**
     * Get the amount of units of
     * this weapon per instance
     *
     * @return the amount of explosives
     * per instance
     */
    int getUnits();

    /**
     * Get the explosive placement
     * cadence. In case of explosives,
     * cadence is the amount of time it
     * takes to place/throw the explosive,
     * and not the time to wait after placing
     * or throwing the explosive
     *
     * @return the explosive placement
     * cadence.
     */
    double getCadence();
}
