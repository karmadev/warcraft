package es.karmadev.warcraft.api.projectile.type;

import es.karmadev.warcraft.api.projectile.Projectile;
import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.util.uuns.UUNS;

/**
 * Represents a shell
 * projectile
 */
public final class ProjectileShell implements Projectile {

    private final static ProjectileShell INSTANCE = new ProjectileShell();

    private ProjectileShell() {}

    /**
     * Get the projectile UUNS
     *
     * @return the projectile UUNS
     */
    @Override
    public UUNS getUUNS() {
        return UUNS.warcraft("projectile_shell");
    }

    /**
     * Get the projectile's projectile type
     *
     * @return the projectile type
     */
    @Override
    public ProjectileType getType() {
        return ProjectileType.SHELL;
    }

    /**
     * Get the shell instance
     *
     * @return the shell projectile
     */
    public static ProjectileShell getInstance() {
        return ProjectileShell.INSTANCE;
    }
}
