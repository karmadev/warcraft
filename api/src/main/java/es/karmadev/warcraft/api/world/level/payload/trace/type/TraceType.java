package es.karmadev.warcraft.api.world.level.payload.trace.type;

/**
 * Payload trace type
 */
public enum TraceType {
    /**
     * No special data is
     * applied to the trace
     */
    DEFAULT,
    /**
     * Checkpoint trace
     */
    CHECKPOINT,
    /**
     * Trace is slowed down
     * based on the provided
     * fragment data
     */
    SLOW_DOWN,
    /**
     * Trace is speed up
     * based on the provided
     * fragment data
     */
    SPEED_UP,
    /**
     * Trace is stopped
     * the specified amount
     * of time in the fragment
     * data
     */
    STOP
}
