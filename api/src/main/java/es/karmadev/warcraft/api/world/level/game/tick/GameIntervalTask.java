package es.karmadev.warcraft.api.world.level.game.tick;

/**
 * Represents a game interval
 * task
 */
public final class GameIntervalTask implements Runnable {

    private final long interval;
    private final Runnable task;

    /**
     * Create the game interval task
     *
     * @param interval the interval
     * @param task the task to run
     */
    public GameIntervalTask(final long interval, final Runnable task) {
        this.interval = interval;
        this.task = task;
    }

    /**
     * Get the interval
     *
     * @return the interval
     */
    public long getInterval() {
        return this.interval;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        this.task.run();
    }
}
