package es.karmadev.warcraft.api.plugin.scheduler;

/**
 * Represents a scheduled task
 */
public abstract class Scheduled {

    private final long id;
    private final Scheduler scheduler;

    /**
     * Create a scheduled task
     *
     * @param id the task ID
     * @param scheduler the task scheduler
     */
    public Scheduled(final long id, final Scheduler scheduler) {
        this.id = id;
        this.scheduler = scheduler;
    }

    /**
     * Get the task ID
     *
     * @return the task ID
     */
    public long getId() {
        return this.id;
    }

    /**
     * Get the task scheduler
     *
     * @return the task scheduler
     */
    public Scheduler getScheduler() {
        return this.scheduler;
    }

    /**
     * Get the times this task has
     * been iterated
     *
     * @return the times the task has been
     * iterated
     */
    public abstract long getIterations();

    /**
     * Get if the task is running
     *
     * @return if the task is running
     */
    public final boolean isRunning() {
        return this.scheduler.isRunning(this.id);
    }

    /**
     * Get if the task is cancelled
     *
     * @return if the task is cancelled
     */
    public final boolean isCancelled() {
        return this.scheduler.isCancelled(this.id);
    }

    /**
     * Cancel the task
     */
    public final void cancel() {
        this.scheduler.cancelTask(this.id);
    }

    /**
     * Stop the task immediately
     */
    public final void stop() {
        this.scheduler.cancelNow(this.id);
    }
}
