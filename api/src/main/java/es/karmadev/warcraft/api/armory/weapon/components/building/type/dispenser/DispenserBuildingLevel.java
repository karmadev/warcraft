package es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser;

import es.karmadev.warcraft.api.armory.weapon.components.building.BuildingLevel;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingAmo;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingHealing;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingMetal;

/**
 * Represents a dispenser level
 */
public interface DispenserBuildingLevel extends BuildingLevel {

    /**
     * Get the dispenser healer
     *
     * @return the healer
     */
    DispenserBuildingHealing getHealRefill();

    /**
     * Get the dispenser amo re-filler
     *
     * @return the amo re-filler
     */
    DispenserBuildingAmo getAmoRefill();

    /**
     * Get the dispenser metal re-filler
     *
     * @return the metal re-filler
     */
    DispenserBuildingMetal getMetalRefill();

    /**
     * Get the dispenser metal capacity
     *
     * @return the metal capacity
     */
    int getMaxMetal();
}
