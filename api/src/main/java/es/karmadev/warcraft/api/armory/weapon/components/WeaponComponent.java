package es.karmadev.warcraft.api.armory.weapon.components;

/**
 * Represents a weapon component
 */
public interface WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    ComponentType getComponentType();
}
