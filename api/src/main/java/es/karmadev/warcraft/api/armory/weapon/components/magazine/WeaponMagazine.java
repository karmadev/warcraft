package es.karmadev.warcraft.api.armory.weapon.components.magazine;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;

/**
 * Represents a weapon magazine
 */
public interface WeaponMagazine extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.MAGAZINE;
    }

    /**
     * Get the magazine chamber
     *
     * @return the magazine chamber
     */
    int getChamber();

    /**
     * Get the weapon magazine capacity
     *
     * @return the magazine capacity
     */
    int getCapacity();

    /**
     * Get the sounds to play when the
     * magazine is empty
     *
     * @return the empty sounds
     */
    WeaponSoundSet getEmptySounds();
}
