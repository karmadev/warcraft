package es.karmadev.warcraft.api.armory.weapon.components.sound;

import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;

import java.util.Collection;

/**
 * Represents a set of {@link WeaponSound}
 */
public interface WeaponSoundSet extends Iterable<WeaponSound> {

    /**
     * Get the component this sound set
     * pertains to
     *
     * @return the soundset component
     */
    WeaponComponent getComponent();

    /**
     * Get all the sounds in the sound
     * set
     *
     * @return the sounds
     */
    Collection<? extends WeaponSound> getSounds();

    /**
     * Get all the sounds in the sound
     * set which play in the specified time
     *
     * @param time the time when the
     *             song plays
     * @return the time sounds
     */
    Collection<? extends WeaponSound> getSounds(final long time);

    /**
     * Get the soundset size
     *
     * @return the soundset size
     */
    int size();

    /**
     * Get if the sounds are empty
     *
     * @return if the soundset is empty
     */
    boolean isEmpty();
}
