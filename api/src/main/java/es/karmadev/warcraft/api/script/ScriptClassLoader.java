package es.karmadev.warcraft.api.script;

import es.karmadev.warcraft.api.WarCraft;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Defines the script class loader. The script
 * class loader is the class loader used for
 * script environments (LUA by default) to ensure
 * a safe and secure script environment, in where
 * only API methods and java elements are accessible
 */
public final class ScriptClassLoader extends ClassLoader {

    private final static Pattern PROTECTED_PACKAGES = Pattern.compile("^es\\.karmadev\\.warcraft\\.((v[a-zA-Z][0-9]+\\.)?common|spigot)(\\.?(?!lua).)*",
            Pattern.CASE_INSENSITIVE);
    private final static Pattern DANGEROUS_PACKAGES = Pattern.compile("^java\\.((lang\\.(reflect|invoke))|(io\\.(Runtime|ProcessBuilder))|(awt\\.(Desktop))).*");

    static {
        registerAsParallelCapable();
    }

    final static Set<LuaPackage> packages = ConcurrentHashMap.newKeySet();

    private final Map<String, Boolean> secured = new ConcurrentHashMap<>();
    private final Set<String> released = ConcurrentHashMap.newKeySet();

    /**
     * Protect a package
     *
     * @param pack the package to protect
     * @param warns if there should be a warning
     *              for the package
     */
    public void protect(final Package pack, final boolean warns) {
        if (pack == null) return;
        this.protect(pack.getName(), warns);
    }

    /**
     * Protect a single class
     *
     * @param clazz the class to protect
     * @param warns if there should be a warning
     *              for the class
     */
    public void protect(final Class<?> clazz, final boolean warns) {
        if (clazz == null) return;
        this.protect(clazz.getCanonicalName(), warns);
    }

    /**
     * Protect a target
     *
     * @param target the target name
     * @param warns if there should be a warning
     *              for the target
     */
    public void protect(final String target, final boolean warns) {
        if (target == null || target.isEmpty()) return;
        this.secured.put(target, warns);
        this.released.remove(target);
    }

    /**
     * Removes protection for a protected
     * package
     *
     * @param pack the package to release
     */
    public void release(final Package pack) {
        if (pack == null) return;
        this.secured.remove(pack.getName());
    }

    /**
     * Release a class from protection, regardless
     * if it was protected by a package protection, or
     * by a class protection
     *
     * @param clazz the class to release
     */
    public void release(final Class<?> clazz) {
        if (clazz == null) return;
        String name = clazz.getCanonicalName();
        this.released.add(name);
    }

    /**
     * Release a class name (or raw class/package)
     * from protected targets
     *
     * @param name the element to unprotect
     */
    public void release(final String name) {
        if (name == null || name.isEmpty()) return;
        this.released.add(name);
    }

    /**
     * Removes release state for a protected
     * package
     *
     * @param pack the package to un release
     */
    public void unRelease(final Package pack) {
        if (pack == null) return;
        this.released.remove(pack.getName());
    }

    /**
     * Removes release state for a class
     *
     * @param clazz the class to un release
     */
    public void unRelease(final Class<?> clazz) {
        if (clazz == null) return;
        String name = clazz.getCanonicalName();

        this.released.remove(name);
    }

    /**
     * Removes release state for the
     * element
     *
     * @param name the element to un release
     */
    public void unRelease(final String name) {
        if (name == null || name.isEmpty()) return;
        this.released.remove(name);
    }

    /**
     * Loads the class with the specified <a href="#binary-name">binary name</a>.  The
     * default implementation of this method searches for classes in the
     * following order:
     *
     * <ol>
     *
     *   <li><p> Invoke {@link #findLoadedClass(String)} to check if the class
     *   has already been loaded.  </p></li>
     *
     *   <li><p> Invoke the {@link #loadClass(String) loadClass} method
     *   on the parent class loader.  If the parent is {@code null} the class
     *   loader built into the virtual machine is used, instead.  </p></li>
     *
     *   <li><p> Invoke the {@link #findClass(String)} method to find the
     *   class.  </p></li>
     *
     * </ol>
     *
     * <p> If the class was found using the above steps, and the
     * {@code resolve} flag is true, this method will then invoke the {@link
     * #resolveClass(Class)} method on the resulting {@code Class} object.
     *
     * <p> Subclasses of {@code ClassLoader} are encouraged to override {@link
     * #findClass(String)}, rather than this method.  </p>
     *
     * <p> Unless overridden, this method synchronizes on the result of
     * {@link #getClassLoadingLock getClassLoadingLock} method
     * during the entire class loading process.
     *
     * @param name    The <a href="#binary-name">binary name</a> of the class
     * @param resolve If {@code true} then resolve the class
     * @return The resulting {@code Class} object
     * @throws ClassNotFoundException If the class could not be found
     */
    @Override
    protected Class<?> loadClass(final String name, final boolean resolve) throws ClassNotFoundException {
        if (name == null) throw new NullPointerException();
        if (name.equalsIgnoreCase("java.lang.runtime"))
            throw new ClassNotFoundException(name); //Runtime allows to execute system commands, not good

        if (name.startsWith("jdk.") || name.startsWith("com.sun"))
            throw new ClassNotFoundException(name); //Prevent internal classes to be loaded

        if (name.startsWith("java.")) {
            Matcher danger = DANGEROUS_PACKAGES.matcher(name);
            if (danger.matches()) //But if we're loading reflection... Abort! abort!
                throw new ClassNotFoundException(name);

            return super.loadClass(name, resolve); //Skip verifications, it's just java!
        }

        Class<?> clazz = super.loadClass(name, resolve);
        if (clazz == null)
            throw new ClassNotFoundException(name);

        ensureLuaAccess(clazz);
        return clazz;
    }

    private void ensureLuaAccess(final Class<?> clazz) throws ClassNotFoundException {
        String cName = clazz.getCanonicalName();

        if (cName.startsWith("es.karmadev.warcraft.api")) return;
        if (clazz.isAssignableFrom(LuaPackage.class)) {
            Class<? extends LuaPackage> luaPackage = clazz.asSubclass(LuaPackage.class);
            try {
                LuaPackage instance = luaPackage.getConstructor().newInstance();
                packages.add(instance); //If it's not registered yet, register it

                if (!instance.isAllowed())
                    throw new ClassNotFoundException(cName);
            } catch (ReflectiveOperationException ex) {
                throw new IllegalStateException(ex);
            }
        }

        if (clazz.isAnnotationPresent(LuaAccess.class)) {
            LuaAccess access = clazz.getAnnotation(LuaAccess.class);
            if (!access.allow()) {
                throw new ClassNotFoundException(cName);
            } else {
                return;
            }
        }

        LuaPackage bestPackage = findNearestLuaPackage(cName);
        if (bestPackage != null && bestPackage.isAllowed()) {
            return;
        } else if (bestPackage != null)
            throw new ClassNotFoundException(cName);

        if (ensureAccessible(cName)) return;

        //At this point, LuaAccess, LuaPackage, and explicit access has failed
        throw new ClassNotFoundException(cName);
    }

    private static @Nullable LuaPackage findNearestLuaPackage(final String cName) {
        String bestKey = null;
        LuaPackage bestPackage = null;
        for (LuaPackage pack : packages) {
            Package packTarget = pack.getClass().getPackage();
            String packName = packTarget.getName();

            if (!cName.startsWith(packName)) continue;

            Matcher matcher = PROTECTED_PACKAGES.matcher(packName);
            boolean matches = matcher.matches();
            if (matches && pack.isAllowed()) continue; //Ignore bypass hacks

            if (bestKey == null) {
                bestKey = packName;
                bestPackage = pack;
            } else {
                if (bestKey.length() < packName.length()) {
                    bestKey = packName;
                    bestPackage = pack;
                }
            }
        }

        return bestPackage;
    }

    private boolean ensureAccessible(final String name) {
        if (released.stream().anyMatch((s) -> {
            if (s.endsWith(".class"))
                s = s.substring(0, s.length() - 6);

            return name.startsWith(s) || name.endsWith(s);
        })) return true;

        Collection<String> keys = this.secured.keySet();
        String bestKey = null;
        String choosenKey = null;

        if (this.secured.containsKey(name)) {
            bestKey = name;
        } else {
            for (final String key : keys) {
                boolean match;
                String subKey = key;
                if (key.endsWith(".class")) {
                    subKey = key.substring(0, key.length() - 6);
                    match = name.endsWith(subKey);
                } else {
                    match = name.startsWith(key);
                }

                if (match) {
                    if (bestKey == null) {
                        bestKey = subKey;
                        choosenKey = key;
                        continue;
                    }

                    if (bestKey.length() < key.length()) {
                        bestKey = subKey;
                        choosenKey = key;
                    }
                }
            }
        }

        if (choosenKey == null) return false;

        Boolean printWarning = this.secured.get(choosenKey);
        if (printWarning != null)
            throwClassLoadException(name, bestKey, printWarning);

        return false;
    }

    private void throwClassLoadException(final String name, final String key, final boolean warn) throws NoClassDefFoundError {
        WarCraft instance = WarCraft.getInstance();
        if (warn)
            instance.logWarning("Tried to load protected \"{}\" from script", key);

        throw new NoClassDefFoundError(name);
    }
}