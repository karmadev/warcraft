package es.karmadev.warcraft.api.armory.weapon.components.projectile;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.api.projectile.ProjectileType;

/**
 * Represents the weapon projectile
 */
public interface WeaponProjectile extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.PROJECTILE;
    }

    /**
     * Get the weapon projectile type
     *
     * @return the projectile type
     */
    ProjectileType getType();

    /**
     * Get the weapon projectile name
     *
     * @return the weapon projectile name
     */
    String getProjectileName();

    /**
     * Get the projectile speed
     * multiplier
     *
     * @return the speed multiplier
     */
    double getSpeed();

    /**
     * Get the projectile effective distance
     *
     * @return the effective distance in where
     * the projectile deals 100% of its damage
     */
    int getEffectiveDistance();

    /**
     * Get the projectile distance penalty, in
     * where the damage gets reduced for each
     * amount of meters travelled
     *
     * @return the distance penalty
     */
    int getDistancePenalty();

    /**
     * Get the weapon base damage
     *
     * @return the base damage
     */
    double getDamage();

    /**
     * Get the weapon damage penalty for
     * each {@link #getDistancePenalty()}
     *
     * @return the damage penalty
     */
    double getDamagePenalty();

    /**
     * Get the weapon projectile sounds
     *
     * @return the projectile sounds
     */
    WeaponSoundSet getSounds();
}
