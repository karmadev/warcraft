package es.karmadev.warcraft.api.world.level.payload;

import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.api.world.level.game.Game;

/**
 * Represents a payload
 */
public interface Payload {

    /**
     * Get the payload current
     * game
     *
     * @return the payload game
     */
    Game getGame();

    /**
     * Get the payload team
     * owner
     *
     * @return the payload owner
     */
    Team getTeam();
}
