package es.karmadev.warcraft.api.armory.weapon.type;

/**
 * Represents a building type
 */
public enum BuildType {
    /**
     * Sentry gun
     */
    SENTRY,
    /**
     * Dispenser
     */
    DISPENSER,
    /**
     * Teleport entrance
     */
    TELEPORT_ENTRANCE,
    /**
     * Teleport exit
     */
    TELEPORT_EXIT
}
