package es.karmadev.warcraft.api.plugin.armory;

import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

/**
 * Represents the plugin armory manager
 */
public interface ArmoryManager {

    /**
     * Register an armory into the
     * plugin
     *
     * @param armory the armory to register
     * @return if the armory was registered
     */
    boolean register(final Armory armory);

    /**
     * Get if the armory manager contains
     * the specified armory
     *
     * @param armory the armory
     * @return if the armory manager contains
     * the armory
     */
    default boolean contains(final Armory armory) {
        return this.contains(armory.getUUNS());
    }

    /**
     * Get if the armory manager contains
     * the armory with the specified name
     *
     * @param name the armory name
     * @return if there's an armory with that
     * name registered
     */
    boolean contains(final UUNS name);

    /**
     * Get all the registered armories
     *
     * @return the registered armories
     */
    Collection<Armory> getArmories();

    /**
     * Get an armory by its name
     *
     * @param name the armory name
     * @return the armory
     */
    @Nullable
    Armory getArmory(final UUNS name);

    /**
     * Get the globally registered
     * weapons
     *
     * @return the weapons
     */
    WeaponManager getWeapons();
}
