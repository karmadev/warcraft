package es.karmadev.warcraft.api.armory.weapon.components.reload;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;

/**
 * Represents a weapon reload data
 */
public interface WeaponReload extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.RELOAD;
    }

    /**
     * Get the time to take for
     * each reload
     *
     * @return the time for reload
     */
    long getTime();

    /**
     * Get the weapon reload sounds
     *
     * @return the reload sounds
     */
    WeaponSoundSet getSounds();
}