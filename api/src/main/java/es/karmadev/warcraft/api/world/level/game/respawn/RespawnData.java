package es.karmadev.warcraft.api.world.level.game.respawn;

import es.karmadev.warcraft.api.world.level.player.Client;

import java.time.DateTimeException;

/**
 * Represents a client respawn data
 */
public interface RespawnData {

    /**
     * Infinite time (-1)
     */
    int INFINITE = -1;

    /**
     * Get the respawn data client
     *
     * @return the data client
     */
    Client getClient();

    /**
     * Get when the respawn timer
     * was set on the client
     *
     * @return the respawn timer start
     */
    long getRespawnStart();

    /**
     * Get when the client will respawn
     *
     * @return the respawn timer end
     */
    long getRespawnEnd();

    /**
     * Tries to set the client
     * respawn end. The operation might
     * fail in the following cases:
     * <ul>
     *     <li>If the respawn end time is before the respawn start time</li>
     *     <li>If the respawn end time is the same as the current one</li>
     *     <li>If the respawn end time is marked to {@link RespawnData#INFINITE}</li>
     * </ul>
     *
     * @param newTime the new end time
     * @return if the operation was success
     */
    boolean setRespawnEnd(final long newTime);

    /**
     * Add time to the respawn end time
     *
     * @param seconds the seconds to add
     * @return if the operation was success
     */
    default boolean addTime(final int seconds) {
        long currentEnd = this.getRespawnEnd();
        if (seconds == 0 || currentEnd == INFINITE) return false;

        return setRespawnEnd(currentEnd + Math.abs(seconds));
    }

    /**
     * Remove time to the respawn end time
     *
     * @param seconds the seconds to remove
     * @return if the operation was success
     */
    default boolean removeTime(final int seconds) {
        long currentEnd = this.getRespawnEnd();
        if (seconds == 0 || currentEnd == INFINITE) return false;

        return setRespawnEnd(currentEnd - Math.abs(seconds));
    }

    /**
     * Get the amount of seconds the
     * client must wait before spawning
     * again
     *
     * @return the amount of seconds the client
     * must wait
     */
    default int getRespawnTime() {
        long start = this.getRespawnStart();
        long end = this.getRespawnEnd();
        if (end == INFINITE)
            return INFINITE;

        if (start > end)
            throw new DateTimeException("Cannot calculate from a future to the past");

        long diff = end - start;
        if (diff > Integer.MAX_VALUE) return INFINITE; //Infinite

        return (int) diff;
    }

    /**
     * Get the amount of seconds to wait
     * before being able to respawn
     *
     * @return the amount of seconds to respawn
     */
    default int getRemainingTime() {
        long now = System.currentTimeMillis();
        long end = this.getRespawnEnd();

        if (end == INFINITE)
            return INFINITE;

        long diff = end - now;
        if (diff <= 0) return 0;
        if (diff > Integer.MAX_VALUE)
            return INFINITE;

        return (int) diff;
    }
}