package es.karmadev.warcraft.api.projectile.type;

import es.karmadev.warcraft.api.projectile.Projectile;
import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.util.uuns.UUNS;

/**
 * Represents a weapon
 * projectile
 */
public final class ProjectileWeapon implements Projectile {

    private final static ProjectileWeapon INSTANCE = new ProjectileWeapon();

    private ProjectileWeapon() {}

    /**
     * Get the projectile UUNS
     *
     * @return the projectile UUNS
     */
    @Override
    public UUNS getUUNS() {
        return UUNS.warcraft("projectile_weapon");
    }

    /**
     * Get the projectile's projectile type
     *
     * @return the projectile type
     */
    @Override
    public ProjectileType getType() {
        return ProjectileType.WEAPON;
    }

    /**
     * Get the weapon instance
     *
     * @return the weapon projectile
     */
    public static ProjectileWeapon getInstance() {
        return ProjectileWeapon.INSTANCE;
    }
}
