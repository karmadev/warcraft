package es.karmadev.warcraft.api.armory.weapon.type;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.components.magazine.WeaponMagazine;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.api.armory.weapon.components.projectile.WeaponProjectile;
import es.karmadev.warcraft.api.armory.weapon.components.recoil.WeaponRecoil;
import es.karmadev.warcraft.api.armory.weapon.components.reload.WeaponReload;

/**
 * Represents a gun
 */
public interface GunWeapon extends Weapon {

    /**
     * Get the weapon type
     *
     * @return the weapon type
     */
    @Override
    default WeaponType getWeaponType() {
        return WeaponType.GUN;
    }

    /**
     * Get the weapon model
     *
     * @return the weapon model
     */
    WeaponModel getModel();

    /**
     * Get the weapon magazine
     *
     * @return the weapon magazine
     */
    WeaponMagazine getMagazine();

    /**
     * Get the weapon projectile
     *
     * @return the weapon projectile
     */
    WeaponProjectile getProjectile();

    /**
     * Get the weapon recoil
     *
     * @return the weapon recoil
     */
    WeaponRecoil getRecoil();

    /**
     * Get the weapon reload
     *
     * @return the weapon reload
     */
    WeaponReload getReload();

    /**
     * Get if the weapon is automatic
     *
     * @return if the weapon is auto
     * matic
     */
    boolean isAutomatic();

    /**
     * Get the weapon cadence
     *
     * @return the weapon cadence
     */
    long getCadence();
}