package es.karmadev.warcraft.api.armory.weapon.components.building;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;

/**
 * Represents a building level
 */
public interface BuildingLevel extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.BUILDING_LEVEL;
    }

    /**
     * Get the level
     *
     * @return the level
     */
    int getLevel();

    /**
     * Get the building health
     *
     * @return the building health
     * override
     */
    double getHealth();

    /**
     * Get the building radius
     *
     * @return the level radius
     */
    double getRadius();
}
