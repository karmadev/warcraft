package es.karmadev.warcraft.api.armory.weapon;

import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.type.WeaponType;
import es.karmadev.warcraft.api.util.uuns.UUNS;

import java.util.Collection;

/**
 * Represents a weapon itself
 */
public interface Weapon {

    /**
     * Get the weapon armory
     *
     * @return the armory
     */
    Armory getArmory();

    /**
     * Get the weapon name
     *
     * @return the weapon name
     */
    String getName();

    /**
     * Get the weapon description
     *
     * @return the description
     */
    Collection<String> getDescription();

    /**
     * Get the weapon type
     *
     * @return the weapon type
     */
    WeaponType getWeaponType();

    /**
     * Get a weapon component by
     * component type
     *
     * @param type the component type
     * @param cType the component object type
     * @return the weapon component
     * @param <T> the component type
     */
    <T extends WeaponComponent> T getComponent(final ComponentType type, final Class<T> cType);

    /**
     * Get all the weapon components
     *
     * @return the weapon components
     */
    Collection<? extends WeaponComponent> getComponents();

    /**
     * Get if the weapon has available
     * the specified component
     *
     * @param type the component type
     * @return if the weapon has teh component
     */
    boolean hasComponent(final ComponentType type);

    /**
     * Get the weapon UUNS
     *
     * @return the weapon UUNS
     */
    default UUNS getUUNS() {
        return UUNS.create(
                getArmory().getUUNS().toString()
                        .replace("=", "_")
                        .toLowerCase(),
                getName().toLowerCase()
        );
    }
}
