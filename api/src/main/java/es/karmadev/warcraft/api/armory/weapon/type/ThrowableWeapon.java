package es.karmadev.warcraft.api.armory.weapon.type;

import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.components.fuse.WeaponFuse;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.api.armory.weapon.components.projectile.ThrowableProjectile;
import es.karmadev.warcraft.api.util.uuns.UUNS;

import java.util.Collection;

/**
 * Represents a throwable weapon. Throwable
 * weapons does not exist by themselves, but
 * by and for other {@link Weapon}.
 */
public interface ThrowableWeapon extends Weapon {

    /**
     * Get the weapon type
     *
     * @return the weapon type
     */
    @Override
    default WeaponType getWeaponType() {
        return WeaponType.THROWABLE;
    }

    /**
     * Get the weapon base of the
     * throwable weapon
     *
     * @return the weapon base
     */
    Weapon getWeaponBase();

    /**
     * Get the weapon armory
     *
     * @return the armory
     */
    @Override
    default Armory getArmory() {
        return getWeaponBase().getArmory();
    }

    /**
     * Get the weapon UUNS
     *
     * @return the weapon UUNS
     */
    @Override
    default UUNS getUUNS() {
        return getWeaponBase().getUUNS();
    }

    /**
     * Get the weapon name
     *
     * @return the weapon name
     */
    @Override
    default String getName() {
        return getWeaponBase().getName();
    }

    /**
     * Get the weapon description
     *
     * @return the description
     */
    @Override
    default Collection<String> getDescription() {
        return getWeaponBase().getDescription();
    }

    /**
     * Get the thrown weapon model
     *
     * @return the throw model
     */
    WeaponModel getModel();

    /**
     * Get the thrown weapon fuse time
     *
     * @return the weapon fuse
     */
    WeaponFuse getFuse();

    /**
     * Get the thrown weapon projectile
     *
     * @return the weapon projectile
     */
    ThrowableProjectile getProjectile();
}