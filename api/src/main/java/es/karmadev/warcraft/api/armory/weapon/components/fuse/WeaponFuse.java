package es.karmadev.warcraft.api.armory.weapon.components.fuse;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;

/**
 * Represents a weapon fuse
 */
public interface WeaponFuse extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.FUSE;
    }

    /**
     * Get the weapon fuse time
     *
     * @return the fuse time
     */
    long getFuseTime();

    /**
     * Get the weapon ticking sound
     *
     * @return the ticking sounds
     */
    WeaponSoundSet getTickSounds();

    /**
     * Get the weapon explosion sounds
     *
     * @return the explosion sounds
     */
    WeaponSoundSet getExplodeSounds();
}
