package es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.api.projectile.ProjectileType;

/**
 * Represents a sentry gun weapon
 */
public interface SentryBuildingWeapon extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.SENTRY_WEAPON;
    }

    /**
     * Get the weapon projectile type
     *
     * @return the projectile type
     */
    ProjectileType getProjectileType();

    /**
     * Get the weapon projectile
     *
     * @return the weapon projectile
     */
    String getProjectileName();

    /**
     * Get the gun amo
     *
     * @return the amo
     */
    int getAmo();

    /**
     * Get the projectile damage
     *
     * @return the damage
     */
    double getDamage();

    /**
     * Get the projectile cadence
     *
     * @return the cadence
     */
    long getCadence();

    /**
     * Get the gun reload time
     *
     * @return the reload time
     */
    long getReloadTime();

    /**
     * Get the weapon shoot sounds
     *
     * @return the shoot sounds
     */
    WeaponSoundSet getShootSounds();

    /**
     * Get the weapon empty shoot
     * sounds
     *
     * @return the empty barrel sounds
     */
    WeaponSoundSet getEmptySounds();

    /**
     * Get the weapon reload sounds
     *
     * @return the reload sounds
     */
    WeaponSoundSet getReloadSounds();

    /**
     * Get the amount of times
     * this weapon is hold in the
     * sentry
     *
     * @return the amount of weapons of
     * this kind the sentry has
     */
    int getAmount();
}
