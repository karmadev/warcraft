package es.karmadev.warcraft.api.world.level.game.tick;

import es.karmadev.warcraft.api.world.level.game.Game;

/**
 * Represents a future tick
 * on a {@link Game}
 */
public abstract class FutureGameTick implements Runnable {

    private boolean preventDefault;

    /**
     * Prevent the default behaviour of
     * the tick
     */
    protected final void preventDefault() {
        preventDefault = true;
    }

    /**
     * Get if the game tick prevents
     * the default behaviour defined
     * in {@link Game#tick()}
     *
     * @return if the default is prevented
     */
    public boolean isPreventDefault() {
        return this.preventDefault;
    }
}
