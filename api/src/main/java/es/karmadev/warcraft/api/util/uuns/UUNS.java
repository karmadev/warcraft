package es.karmadev.warcraft.api.util.uuns;

import org.jetbrains.annotations.Contract;

/**
 * Universal-Unique-NameSpace
 */
@SuppressWarnings("unused")
public final class UUNS {

    private final String path;
    private final String value;

    /**
     * Create a new UUNS
     *
     * @param path the UUNS path
     * @param value the UUNS value
     * @throws IllegalArgumentException if the path or value are empty
     * or null
     */
    @Contract("null, null -> fail")
    public UUNS(final String path, final String value) throws IllegalArgumentException {
        if (path == null || path.trim().isEmpty())
            throw new IllegalArgumentException("Cannot create UUNS for empty or null path");
        if (value == null || value.trim().isEmpty())
            throw new IllegalArgumentException("Cannot create UUNS for empty or null value");

        this.path = path.toLowerCase().replaceAll("\\s", "_")
                .replaceAll("[^a-z0-9-_]", "");
        this.value = value.toLowerCase().replaceAll("\\s", "_")
                .replaceAll("[^a-z0-9-_]", "");
    }

    /**
     * Get the UUNS path
     *
     * @return the path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Get the UUNS value
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Get the UUNS in a string
     * representation
     *
     * @return the UUNS
     */
    @Override
    public String toString() {
        return String.format("%s=%s", this.path, this.value);
    }

    /**
     * Check if the object is the
     * same as this object
     *
     * @param obj the object to check for
     * @return if the objects are the same
     */
    @Override @Contract("null -> false")
    public boolean equals(final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof UUNS)) return false;

        UUNS other = (UUNS) obj;
        return other.path.equals(this.path)
                && other.value.equals(this.value);
    }

    /**
     * Hash the UUNS code
     *
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        return (this.path.hashCode() * (int) '=') +
                this.value.hashCode();
    }

    /**
     * Create a UUNS
     *
     * @param path the UUNS path
     * @param value the UUNS value
     * @return the UUNS
     * @throws IllegalArgumentException if the path or value are empty or null
     */
    @Contract("null, null -> fail")
    public static UUNS create(final String path, final String value) throws IllegalArgumentException {
        return new UUNS(path, value);
    }

    /**
     * Create a warcraft UUNS
     *
     * @param value the UUNS value
     * @return the UUNS
     * @throws IllegalArgumentException if the value is empty or null
     */
    @Contract("null -> fail")
    public static UUNS warcraft(final String value) throws IllegalArgumentException {
        if (value == null || value.trim().isEmpty())
            throw new IllegalArgumentException("Cannot create UUNS for empty or null value");

        return new UUNS("warcraft", value);
    }

    /**
     * Parse the UUNS
     *
     * @param string the UUNS string
     * @return the parsed UUNS
     * @throws IllegalArgumentException if the string is empty or null
     */
    @Contract("null -> fail")
    public static UUNS parse(final String string) throws IllegalArgumentException {
        if (string == null || string.trim().isEmpty())
            throw new IllegalArgumentException("Cannot create UUNS for empty or null string");

        String parseableString = string
                .toLowerCase()
                .replaceAll("[^a-z0-9-_=\\\\]", "")
                .replaceAll("(\\\\\\\\+=)", "<escaped_equals>");
        if (!parseableString.contains("="))
            return warcraft(parseableString.replace("<escaped_equals>", "="));

        String[] data = parseableString.split("=");
        if (data.length <= 1)
            return warcraft(parseableString.replace("<escaped_equals>", ""));

        String path = "";
        int index = 0;
        while (path.trim().isEmpty() && index < data.length) {
            path = data[index++];
        }
        if (index == data.length)
            return warcraft(path.replace("<escaped_equals>", "="));

        String value = data[index];
        return new UUNS(path.replace("<escaped_equals>", ""),
                value.replace("<escaped_equals>", "="));
    }
}
