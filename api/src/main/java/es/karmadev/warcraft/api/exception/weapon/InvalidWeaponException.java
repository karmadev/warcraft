package es.karmadev.warcraft.api.exception.weapon;

import es.karmadev.warcraft.api.armory.Armory;

/**
 * This exception is thrown when
 * an invalid weapon is tried to
 * be created
 */
public class InvalidWeaponException extends Exception {

    /**
     * Initialize the exception
     *
     * @param armory the armory
     * @param name the weapon name
     * @param reasonMessage the error reason
     */
    public InvalidWeaponException(final Armory armory, final String name, final String reasonMessage) {
        super(String.format("Could not create weapon %s of armory %s. %s", name, armory.getUUNS(), reasonMessage));
    }
}
