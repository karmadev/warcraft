package es.karmadev.warcraft.api.plugin.projectile;

import es.karmadev.warcraft.api.projectile.Projectile;
import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.util.uuns.UUNS;

import java.util.Collection;

/**
 * Represents a projectile manager
 */
public interface ProjectileManager {

    /**
     * Register a new projectile
     *
     * @param projectile the projectile to
     *                   register
     * @return if the registration was successful
     */
    boolean registerProjectile(final Projectile projectile);

    /**
     * Get if a projectile has been already
     * registered
     *
     * @param projectile the projectile
     * @return if the projectile has been registered
     */
    default boolean isRegistered(final Projectile projectile) {
        return projectile != null &&
                isRegistered(projectile.getUUNS(), projectile.getType());
    }

    /**
     * Get if a projectile has been already
     * registered
     *
     * @param name the projectile name
     * @param type the projectile
     * @return if the projectile has been registered
     */
    boolean isRegistered(final UUNS name, final ProjectileType type);

    /**
     * Get all the registered projectiles
     * 
     * @return the projectiles
     */
    Collection<Projectile> getProjectiles();

    /**
     * Get a projectile by type
     *
     * @param type the projectile type
     * @return the projectiles
     */
    Projectile getProjectiles(final ProjectileType type);

    /**
     * Get a projectile by name
     *
     * @param name the projectile name
     * @return the projectile
     */
    Projectile getProjectile(final UUNS name);
}
