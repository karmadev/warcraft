package es.karmadev.warcraft.api.armory.weapon.components.projectile;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;

/**
 * Represents a throwable weapon
 * projectile
 */
public interface ThrowableProjectile extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.THROWABLE_PROJECTILE;
    }

    /**
     * Get if the projectile bounces
     *
     * @return if the bounce logic
     * is enabled for this throwable
     * weapon
     */
    boolean isBounce();

    /**
     * Get the projectile speed
     *
     * @return the projectile speed
     */
    double getSpeed();

    /**
     * Get if the projectile speed is
     * constant
     *
     * @return if the speed is constant
     */
    boolean isConstantSpeed();

    /**
     * Get if the projectile explodes
     * as soon as it hits a solid
     * object
     *
     * @return if the projectile explodes
     * when it hits something
     */
    boolean isExplodeOnHit();
}
