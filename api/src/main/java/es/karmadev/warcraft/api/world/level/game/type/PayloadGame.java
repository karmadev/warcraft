package es.karmadev.warcraft.api.world.level.game.type;

import es.karmadev.warcraft.api.world.level.game.Game;
import es.karmadev.warcraft.api.world.level.game.GameType;
import es.karmadev.warcraft.api.world.level.game.exception.AsyncGameOperation;
import es.karmadev.warcraft.api.world.level.payload.Payload;
import es.karmadev.warcraft.api.world.level.payload.trace.PayloadTrace;

/**
 * Represents a payload game
 */
public interface PayloadGame extends Game {

    /**
     * Get the game mode
     *
     * @return the game mode
     */
    @Override
    default GameType getGameType() {
        return GameType.PAYLOAD;
    }

    /**
     * Get the payload
     *
     * @return the game payload
     */
    Payload getPayload();

    /**
     * Set the payload speed for the current
     * tick
     *
     * @param speed the speed
     * @throws AsyncGameOperation if the speed is set
     * out of the game defined tick
     */
    void setPayloadSpeed(final float speed) throws AsyncGameOperation;

    /**
     * Get the payload speed for the
     * current tick
     *
     * @return the speed
     */
    float getPayloadSpeed();

    /**
     * Get the payload trace
     *
     * @return the payload trace
     */
    PayloadTrace getPayloadTrace();

    /**
     * Get the time before the game
     * actually starts, in seconds
     *
     * @return the preparation time
     */
    int getPrepareTime();

    /**
     * Get the remaining seconds before
     * the preparation time ends
     *
     * @return the preparation live time
     */
    int getPrepareRemaining();

    /**
     * Get if the game is in the
     * preparation stage
     *
     * @return if the game is in the
     * preparations
     */
    boolean isPreparing();
}
