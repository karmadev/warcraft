package es.karmadev.warcraft.api.world.level.payload.trace.type.provider;

import es.karmadev.warcraft.api.world.level.payload.trace.type.TraceType;
import es.karmadev.warcraft.api.world.level.payload.trace.type.TypeData;

/**
 * Slow down trace type
 */
public final class SlowTraceType extends PayloadTraceType implements TypeData {

    private final double amount;

    /**
     * Create the slow-down trace
     * fragment type
     *
     * @param amount the amount to slow down
     *               the payload
     */
    public SlowTraceType(final double amount) {
        super(TraceType.SLOW_DOWN);
        this.amount = amount;
    }

    /**
     * Get the slow-down amount
     *
     * @return the amount
     */
    public double getAmount() {
        return this.amount;
    }
}