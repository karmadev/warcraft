package es.karmadev.warcraft.api.armory.weapon.components.explosion;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;

/**
 * Represents the explosion data of an
 * explosive weapon
 */
public interface WeaponExplosion extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.EXPLOSION;
    }

    /**
     * Get the explosive fuse time.
     * A value of zero stands for
     * no fuse
     *
     * @return the explosive fuse time
     */
    long getFuseTime();

    /**
     * Get the explosive explosion
     * effective distance
     *
     * @return the explosion distance
     */
    int getDistance();

    /**
     * Get the explosion radius distance
     * penalty. Each this amount of blocks,
     * the damage will be reduced by the
     * value of {@link #getDamagePenalty()}
     *
     * @return the distance penalty
     */
    int getDistancePenalty();

    /**
     * Get the base explosion damage
     *
     * @return the base damage
     */
    double getDamage();

    /**
     * Get the explosion damage
     * penalty
     *
     * @return the damage penalty
     */
    double getDamagePenalty();

    /**
     * Get the explosion sounds
     *
     * @return the explosion sounds
     */
    WeaponSoundSet getSounds();
}
