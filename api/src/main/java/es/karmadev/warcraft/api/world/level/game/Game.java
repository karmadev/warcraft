package es.karmadev.warcraft.api.world.level.game;

import es.karmadev.warcraft.api.util.time.TimeUtils;
import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.api.world.level.TeamScore;
import es.karmadev.warcraft.api.world.level.game.respawn.RespawnHandler;
import es.karmadev.warcraft.api.world.level.game.tick.GameTickManager;
import es.karmadev.warcraft.api.world.level.player.Client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Represents a game
 */
public interface Game {

    /**
     * Get the game name
     *
     * @return the game name
     */
    String getName();

    /**
     * Get the game respawn handler
     *
     * @return the game respawn
     * handler
     */
    RespawnHandler getRespawn();

    /**
     * Get the maximum amount of clients
     * in the game
     *
     * @return the maximum amount of
     * clients
     */
    int getMaxClients();

    /**
     * Get the amount of clients
     * in the game
     *
     * @return the amount of clients
     */
    int getClientCount();

    /**
     * Get the game time limit, in
     * milliseconds
     *
     * @return the game time limit
     */
    long getTimeLimit();

    /**
     * Get the game time remaining, in
     * milliseconds
     *
     * @return the game remaining time
     */
    long getTimeRemaining();

    /**
     * Get the game tick manager
     *
     * @return the game tick manager
     */
    GameTickManager getTickManager();

    /**
     * Perform a tick in the game
     */
    default void tick() {
        this.removeTime(1);
    }

    /**
     * Add time to the remaining
     * time of the game
     *
     * @param timeToAdd the time to add
     */
    default void addTime(final long timeToAdd) {
        long newTime = this.getTimeRemaining() + Math.abs(timeToAdd);
        setTime(Math.min(newTime, this.getTimeLimit()));
    }

    /**
     * Add time to the remaining
     * time of the game
     *
     * @param timeToRemove the time to add
     */
    default void removeTime(final long timeToRemove) {
        long newTime = this.getTimeRemaining() - Math.abs(timeToRemove);
        setTime(Math.max(0, newTime));
    }

    /**
     * Set the remaining time of
     * the game
     *
     * @param timeRemaining the new time remaining
     */
    void setTime(final long timeRemaining);

    /**
     * Get the remaining time in a
     * string format
     *
     * @return the remaining time
     */
    default String getTime() {
        return TimeUtils.formatMilliseconds(this.getTimeRemaining());
    }

    /**
     * Get if the game is currently
     * playing, which means the time is
     * counting down
     *
     * @return if the game time remaining is
     * counting down
     */
    boolean isPlaying();

    /**
     * Get if the game has finished
     *
     * @return if the game has finished
     */
    boolean hasFinished();

    /**
     * Get the game mode
     *
     * @return the game mode
     */
    GameType getGameType();

    /**
     * Get the game teams
     *
     * @return the game teams
     */
    Collection<Team> getTeams();

    /**
     * Get the score of a team
     *
     * @param team the team
     * @return the team score
     */
    TeamScore getScore(final Team team);

    /**
     * Get all the game clients
     *
     * @return the clients
     */
    default Collection<Client> getClients() {
        List<Client> output = new ArrayList<>();
        for (Team team : this.getTeams())
            output.addAll(this.getClients(team));

        return Collections.unmodifiableCollection(output);
    }

    /**
     * Get all the clients on the provided
     * team
     *
     * @param team the team
     * @return the team clients
     */
    Collection<Client> getClients(final Team team);

    /**
     * Get the team of a client
     *
     * @param client the client
     * @return the client team
     */
    Team getTeam(final Client client);

    /**
     * Tries to set a client team
     *
     * @param client the client
     * @param newTeam the client new team
     * @return if the team was changed
     * successfully
     */
    boolean setTeam(final Client client, final Team newTeam);

    /**
     * Tries to add a client into
     * the game
     *
     * @param client the client to add
     * @return if the operation was successful
     */
    boolean add(final Client client);

    /**
     * Tries to remove a client from
     * the game
     *
     * @param client the client to remove
     * @return if the client was removed
     */
    boolean remove(final Client client);

    /**
     * Tries to add a client into
     * the game
     *
     * @param client the client to add
     * @param team the team to set the client
     *             to
     * @return if the operation was successful
     */
    boolean add(final Client client, final Team team);

    /**
     * Get if a client is currently
     * playing on this game
     *
     * @param client the client
     * @return if the client is playing
     * on the game
     */
    default boolean isPlaying(final Client client) {
        return getClients().contains(client);
    }

    /**
     * Get if a team exists on the
     * current game
     *
     * @param team the team
     * @return if the team exists in the
     * game
     */
    default boolean hasTeam(final Team team) {
        return getTeams().contains(team);
    }

    /**
     * Get the game MVP
     *
     * @return the game MVP
     */
    default Client[] getMVP() {
        List<Client> mvpS = new ArrayList<>();
        float score = -1;
        for (Team team : this.getTeams()) {
            TeamScore sc = this.getScore(team);
            Client mvp = sc.getMVP();
            if (mvp == null) continue;

            float mvpScore = sc.getScore(mvp);
            if (mvpScore < score) continue;
            if (score == -1) score = mvpScore;

            if (mvpScore > score) {
                mvpS.clear();
                score = mvpScore;
            }

            mvpS.add(mvp);
        }

        return mvpS.toArray(new Client[0]);
    }
}