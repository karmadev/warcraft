package es.karmadev.warcraft.api.world.level.payload.trace.type.provider;

import es.karmadev.warcraft.api.world.level.payload.trace.type.TraceType;
import es.karmadev.warcraft.api.world.level.payload.trace.type.TypeData;

/**
 * Speed up trace type
 */
public final class SpeedTraceType extends PayloadTraceType implements TypeData {

    private final double amount;

    /**
     * Create the speed-up trace
     * fragment type
     *
     * @param amount the amount to speed up
     *               the payload
     */
    public SpeedTraceType(final double amount) {
        super(TraceType.SPEED_UP);
        this.amount = amount;
    }

    /**
     * Get the speed-up amount
     *
     * @return the amount
     */
    public double getAmount() {
        return this.amount;
    }
}
