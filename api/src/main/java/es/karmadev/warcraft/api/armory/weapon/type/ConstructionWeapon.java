package es.karmadev.warcraft.api.armory.weapon.type;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.components.building.BuildingLevel;

import java.util.Collection;

public interface ConstructionWeapon extends Weapon {

    /**
     * Get the weapon type
     *
     * @return the weapon type
     */
    @Override
    default WeaponType getWeaponType() {
        return WeaponType.CONSTRUCTION;
    }

    /**
     * Get the effective construction
     * radius
     *
     * @return the radius
     */
    default double getRadius() {
        return this.getLevel(0).getRadius();
    }

    /**
     * Get the base construction
     * health
     *
     * @return the health
     */
    default double getHealth() {
        return this.getLevel(0).getHealth();
    }

    /**
     * Get the construction type
     *
     * @return the type
     */
    BuildType getBuildingType();

    /**
     * Get the building level
     *
     * @param level the level
     * @return the building level
     */
    BuildingLevel getLevel(final int level);

    /**
     * Get all the building levels
     *
     * @return the building levels
     */
    Collection<BuildingLevel> getLevels();
}