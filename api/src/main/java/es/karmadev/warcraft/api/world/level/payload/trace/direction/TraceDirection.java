package es.karmadev.warcraft.api.world.level.payload.trace.direction;

/**
 * Represents a trace direction
 */
@SuppressWarnings("unused")
public final class TraceDirection {

    private final TraceShape shape;
    private final byte yMod;

    /**
     * Create the trace direction
     *
     * @param shape the direction shape
     * @param yMod the y mod
     */
    TraceDirection(final TraceShape shape, final byte yMod) {
        this.shape = shape;
        this.yMod = yMod;
    }

    /**
     * Get the trace shape
     *
     * @return the shape
     */
    public TraceShape getShape() {
        return this.shape;
    }

    /**
     * Get the trace Y modifier
     *
     * @return the Y modifier
     */
    public byte getYMod() {
        return this.yMod;
    }
}
