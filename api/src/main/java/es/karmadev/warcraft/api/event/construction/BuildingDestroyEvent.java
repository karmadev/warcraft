package es.karmadev.warcraft.api.event.construction;

import es.karmadev.warcraft.api.world.ent.construction.WorldConstruction;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * This event gets fired whenever a
 * construction gets destroyed. Regardless
 * if it was because of a client, or because
 * the building owner destroyed it
 */
public class BuildingDestroyEvent extends Event {

    private final static HandlerList HANDLER_LIST = new HandlerList();
    private final WorldConstruction construction;

    /**
     * Create the event
     *
     * @param construction the building weapon
     */
    public BuildingDestroyEvent(final WorldConstruction construction) {
        this.construction = construction;
    }

    /**
     * Get the construction
     *
     * @return the construction
     */
    public WorldConstruction getConstruction() {
        return this.construction;
    }

    /**
     * Get the handler list
     *
     * @return the handler list
     */
    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    /**
     * Get the handler list
     *
     * @return the handler list
     */
    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
