package es.karmadev.warcraft.api.world.level.payload.trace;

import es.karmadev.warcraft.api.world.level.payload.trace.direction.TraceDirection;
import es.karmadev.warcraft.api.world.level.payload.trace.type.TypeData;

/**
 * Represents a {@link PayloadTrace trace}
 * fragment. The fragment includes information
 * such as the direction
 */
@SuppressWarnings("unused")
public interface TraceFragment {

    /**
     * Get the fragment direction
     *
     * @return the direction
     */
    TraceDirection getDirection();

    /**
     * Get the fragment type data
     *
     * @return the type data
     */
    TypeData getTypeData();
}