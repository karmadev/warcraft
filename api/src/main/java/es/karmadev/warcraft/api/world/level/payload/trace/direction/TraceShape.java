package es.karmadev.warcraft.api.world.level.payload.trace.direction;

import static org.bukkit.block.data.Rail.Shape;

/**
 * Payload trace shape
 */
@SuppressWarnings("unused")
public enum TraceShape {
    EAST(Shape.EAST_WEST, "WEST"),
    WEST(Shape.EAST_WEST, "EAST"),
    NORTH(Shape.NORTH_SOUTH, "SOUTH"),
    SOUTH(Shape.NORTH_SOUTH, "NORTH"),
    EAST_NORTH(Shape.NORTH_EAST, "SOUTH_WEST"),
    WEST_NORTH(Shape.NORTH_WEST, "SOUTH_EAST"),
    SOUTH_WEST(Shape.SOUTH_WEST, "EAST_NORTH"),
    SOUTH_EAST(Shape.SOUTH_EAST, "WEST_NORTH"),
    NORTH_WEST(Shape.NORTH_WEST, "EAST_SOUTH"),
    NORTH_EAST(Shape.NORTH_EAST, "WEST_SOUTH"),
    WEST_SOUTH(Shape.SOUTH_WEST, "NORTH_EAST"),
    EAST_SOUTH(Shape.SOUTH_EAST, "NORTH_WEST");

    private final Shape shape;
    private final String opposite;

    TraceShape(final Shape shape, final String opposite) {
        this.shape = shape;
        this.opposite = opposite;
    }

    /**
     * Get the minecraft rail shape
     * of the trace shape
     *
     * @return the rail shape
     */
    public Shape getRailShape() {
        return this.shape;
    }

    /**
     * Get the opposite shape
     *
     * @return the opposite shape
     */
    public TraceShape getOpposite() {
        return TraceShape.valueOf(this.opposite);
    }

    /**
     * Get the trace direction
     *
     * @return the direction
     */
    public TraceDirection direction() {
        return new TraceDirection(this, (byte) 0);
    }

    /**
     * Get the trace direction for
     * up direction
     *
     * @return the direction
     */
    public TraceDirection up() {
        return new TraceDirection(this, (byte) 1);
    }

    /**
     * Get the trace direction for
     * down direction
     *
     * @return the direction
     */
    public TraceDirection down() {
        return new TraceDirection(this, (byte) -1);
    }
}
