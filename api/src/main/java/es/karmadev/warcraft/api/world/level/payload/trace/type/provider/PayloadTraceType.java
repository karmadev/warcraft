package es.karmadev.warcraft.api.world.level.payload.trace.type.provider;

import es.karmadev.warcraft.api.world.level.payload.trace.type.TraceType;
import es.karmadev.warcraft.api.world.level.payload.trace.type.TypeData;

/**
 * Payload trace type
 */
public class PayloadTraceType implements TypeData {

    public final static PayloadTraceType DEFAULT = new PayloadTraceType(TraceType.DEFAULT);
    public final static PayloadTraceType CHECKPOINT = new PayloadTraceType(TraceType.CHECKPOINT);

    protected final TraceType type;

    /**
     * Create the trace type
     *
     * @param type the trace type
     */
    public PayloadTraceType(final TraceType type) {
        this.type = type;
    }

    /**
     * Get the trace type
     *
     * @return the trace type
     */
    @Override
    public final TraceType getType() {
        return this.type;
    }
}
