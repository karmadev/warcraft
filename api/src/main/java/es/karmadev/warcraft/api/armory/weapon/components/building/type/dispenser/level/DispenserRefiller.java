package es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level;

/**
 * Represents a dispenser re-filler
 */
public interface DispenserRefiller {

    /**
     * Get the refill amount
     *
     * @return the amount
     */
    double getAmount();

    /**
     * Get the refill interval
     *
     * @return the refill interval
     */
    long getInterval();
}
