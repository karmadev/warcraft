package es.karmadev.warcraft.api.world.level.payload.trace.type.provider;

import es.karmadev.warcraft.api.world.level.payload.trace.type.TraceType;
import es.karmadev.warcraft.api.world.level.payload.trace.type.TypeData;

/**
 * Stop trace type
 */
public final class StopTraceType extends PayloadTraceType implements TypeData {

    private final long time;

    /**
     * Create the stop trace
     * fragment type
     *
     * @param time the amount of time in
     *               milliseconds to stop the
     *               payload
     */
    public StopTraceType(final long time) {
        super(TraceType.STOP);
        this.time = time;
    }

    /**
     * Get the time to stop
     *
     * @return the time
     */
    public long getTime() {
        return this.time;
    }
}
