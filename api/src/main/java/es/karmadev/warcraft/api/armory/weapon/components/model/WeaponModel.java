package es.karmadev.warcraft.api.armory.weapon.components.model;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;

/**
 * Represents a weapon model
 * data
 */
public interface WeaponModel extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.MODEL;
    }

    /**
     * Get the weapon model type
     *
     * @return the weapon model type
     */
    ModelType getType();

    /**
     * Get the weapon model data
     *
     * @return the weapon model data
     */
    String getModelData();
}
