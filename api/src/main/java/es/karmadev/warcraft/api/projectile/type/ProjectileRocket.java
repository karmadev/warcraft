package es.karmadev.warcraft.api.projectile.type;

import es.karmadev.warcraft.api.projectile.Projectile;
import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.util.uuns.UUNS;

/**
 * Represents a rocket
 * projectile
 */
public final class ProjectileRocket implements Projectile {

    private final static ProjectileRocket INSTANCE = new ProjectileRocket();

    private ProjectileRocket() {}

    /**
     * Get the projectile UUNS
     *
     * @return the projectile UUNS
     */
    @Override
    public UUNS getUUNS() {
        return UUNS.warcraft("projectile_rocket");
    }

    /**
     * Get the projectile's projectile type
     *
     * @return the projectile type
     */
    @Override
    public ProjectileType getType() {
        return ProjectileType.ROCKET;
    }

    /**
     * Get the rocket instance
     *
     * @return the rocket projectile
     */
    public static ProjectileRocket getInstance() {
        return ProjectileRocket.INSTANCE;
    }
}
