package es.karmadev.warcraft.api.world.level.player;

import es.karmadev.warcraft.api.util.uuns.UUNS;
import es.karmadev.warcraft.api.world.level.game.Game;
import es.karmadev.warcraft.api.world.level.game.stats.GameStats;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a client on a
 * game
 */
@SuppressWarnings("unused")
public interface Client {

    /**
     * Get the client name
     *
     * @return the client name
     */
    String getName();

    /**
     * Get the client UUNS
     *
     * @return the UUNS
     */
    default UUNS getUUNS() {
        return UUNS.warcraft(this.getName());
    }

    /**
     * Set the client name
     *
     * @param name the client name
     */
    void setName(final String name);

    /**
     * Get if the client is currently
     * playing on a game
     *
     * @return if the client is playing
     */
    default boolean isPlaying() {
        return getGame() != null;
    }

    /**
     * Get the client current game
     *
     * @return the client game
     */
    @Nullable
    Game getGame();

    /**
     * Tries to set the client current
     * game
     *
     * @param game the new client game
     * @return if the operation was successful
     */
    boolean setGame(final Game game);

    /**
     * Get the client statistics
     *
     * @return the client statistics
     */
    GameStats getStatistics();
}
