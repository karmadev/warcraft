package es.karmadev.warcraft.api.event.construction;

import es.karmadev.warcraft.api.armory.weapon.type.ConstructionWeapon;
import es.karmadev.warcraft.api.world.level.player.Client;
import org.bukkit.Location;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * This event gets called whenever a
 * building is tried to be placed. The event
 * should have the final word on if the
 * building can be placed, and how is it
 * placed
 */
public class BuildingPlaceEvent extends Event implements Cancellable {

    private final static HandlerList HANDLER_LIST = new HandlerList();

    private final Client player;
    private final ConstructionWeapon construction;

    private Location location;
    private boolean isCancelled = false;

    public BuildingPlaceEvent(final Client player, final ConstructionWeapon construction, final Location location) {
        this.player = player;
        this.construction = construction;
        this.location = location;
    }

    /**
     * Get the player trying to
     * place the building
     *
     * @return the player
     */
    public Client getPlayer() {
        return this.player;
    }

    /**
     * Get the construction being
     * build
     *
     * @return the construction
     */
    public ConstructionWeapon getConstruction() {
        return this.construction;
    }

    /**
     * Get the placement location
     *
     * @return the placement location
     */
    public Location getLocation() {
        return this.location;
    }

    /**
     * Set the placement location
     *
     * @param location the new placement
     *                 location
     */
    public void setLocation(final Location location) {
        this.location = location;
    }

    /**
     * Gets the cancellation state of this event. A cancelled event will not
     * be executed in the server, but will still pass to other plugins
     *
     * @return true if this event is cancelled
     */
    @Override
    public boolean isCancelled() {
        return this.isCancelled;
    }

    /**
     * Sets the cancellation state of this event. A cancelled event will not
     * be executed in the server, but will still pass to other plugins.
     *
     * @param cancel true if you wish to cancel this event
     */
    @Override
    public void setCancelled(final boolean cancel) {
        this.isCancelled = cancel;
    }

    /**
     * Get the handler list
     *
     * @return the handler list
     */
    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    /**
     * Get the handler list
     *
     * @return the handler list
     */
    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}