package es.karmadev.warcraft.api.world.level;

import es.karmadev.warcraft.api.util.uuns.UUNS;

/**
 * Represents a game team
 */
public interface Team {

    /**
     * Get the team name
     *
     * @return the team name
     */
    String getName();

    /**
     * Get the team UUNS
     *
     * @return the UUNS
     */
    UUNS getUUNS();

    /**
     * Get the team max players
     * amount
     *
     * @return the team max players amount
     */
    int getMaxPlayers();
}
