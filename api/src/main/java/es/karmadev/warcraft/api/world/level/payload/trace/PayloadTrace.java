package es.karmadev.warcraft.api.world.level.payload.trace;

/**
 * Represents a trace of a payload
 */
public interface PayloadTrace {

    /**
     * Get the next trace fragment. This
     * method works in conjunction with
     * {@link #isFinished()}, as a fragment
     * can be null but the trace could not
     * be finished yet
     *
     * @return the next fragment
     */
    TraceFragment next();

    /**
     * Get the previous trace
     * fragment. This method should
     * check for {@link es.karmadev.warcraft.api.world.level.payload.trace.type.TraceType#CHECKPOINT}
     * fragments, as a payload should never go past
     * it's last checkpoint
     *
     * @return the previous trace fragment
     */
    TraceFragment previous();

    /**
     * Get if the trace has been
     * finished
     *
     * @return if the track is finished
     */
    boolean isFinished();
}