package es.karmadev.warcraft.api.armory.weapon.type;

import es.karmadev.warcraft.api.armory.weapon.Weapon;

/**
 * Represents a melee weapon
 */
public interface MeleeWeapon extends Weapon {

    /**
     * Get the weapon type
     *
     * @return the weapon type
     */
    @Override
    default WeaponType getWeaponType() {
        return WeaponType.MELEE;
    }

    /**
     * Get the melee weapon damage
     *
     * @return the weapon damage
     */
    double getDamage();

    /**
     * Get the melee weapon usage
     * speed
     *
     * @return the weapon usage speed
     */
    long getSpeed();

    /**
     * Get if the weapon can
     * stab
     *
     * @return if the weapon can
     * perform stab damage
     */
    boolean canStab();

    /**
     * Get the weapon metal usage, if
     * this value is over zero, it means
     * it's a wrench. Other values get
     * ignored
     *
     * @return the metal usage
     */
    int getMetalUsage();
}
