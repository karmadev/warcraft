package es.karmadev.warcraft.api.armory;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.type.WeaponType;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import org.jetbrains.annotations.Contract;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Represents an armory. An Armory is nothing
 * but a class which contains a set of weapons
 */
public interface Armory extends Iterable<Weapon> {

    /**
     * Get the armory UUNS
     *
     * @return the armory UUNS
     */
    UUNS getUUNS();

    /**
     * Get the armory name
     *
     * @return the armory name
     */
    String getName();

    /**
     * Get the armory description
     *
     * @return the armory description
     */
    String getDescription();

    /**
     * Get the armory version
     *
     * @return the version
     */
    String getVersion();

    /**
     * Get the armory api version
     *
     * @return the API version of
     * the armory package
     */
    int getApiVersion();

    /**
     * Get the armory author
     *
     * @return the author
     */
    ArmoryAuthor getAuthor();

    /**
     * Get the amount of weapons
     *
     * @return the total amount of weapons
     */
    int size();

    /**
     * Get the amount of weapons for the
     * specified type
     *
     * @param type the weapon type
     * @return the type weapons amount
     */
    int size(final WeaponType type);

    /**
     * Get if the armory has a weapon
     *
     * @param weapon the weapon
     * @return if the armory contains the weapon
     */
    @Contract("null -> false")
    default boolean hasWeapon(final Weapon weapon) {
        if (weapon == null) return false;

        for (WeaponType type : WeaponType.values())
            if (hasWeapon(type, weapon)) return true;

        return false;
    }

    /**
     * Get if the armory has a weapon
     * of the specified type
     *
     * @param type the weapon type
     * @param weapon the weapon
     * @return if the armory contains the weapon
     */
    @Contract("null, null -> false")
    boolean hasWeapon(final WeaponType type, final Weapon weapon);

    /**
     * Get all the armory weapons
     *
     * @return the armory weapons
     */
    Collection<Weapon> getWeapons();

    /**
     * Get all the armory weapons of the
     * specified type
     *
     * @param type the type to filter for
     * @return the weapons of the type
     */
    default Collection<Weapon> getWeapons(final WeaponType type) {
        return getWeapons().stream().filter((wp) -> wp.getWeaponType().equals(type))
                .collect(Collectors.toList());
    }
}