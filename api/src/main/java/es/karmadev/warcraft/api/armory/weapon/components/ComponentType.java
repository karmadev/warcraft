package es.karmadev.warcraft.api.armory.weapon.components;

import es.karmadev.warcraft.api.armory.weapon.components.building.BuildingLevel;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingAmo;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingHealing;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingMetal;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry.SentryBuildingWeapon;
import es.karmadev.warcraft.api.armory.weapon.components.explosion.WeaponExplosion;
import es.karmadev.warcraft.api.armory.weapon.components.fuse.WeaponFuse;
import es.karmadev.warcraft.api.armory.weapon.components.magazine.WeaponMagazine;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.api.armory.weapon.components.projectile.ThrowableProjectile;
import es.karmadev.warcraft.api.armory.weapon.components.projectile.WeaponProjectile;
import es.karmadev.warcraft.api.armory.weapon.components.recoil.WeaponRecoil;
import es.karmadev.warcraft.api.armory.weapon.components.reload.WeaponReload;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSound;

/**
 * Represents a weapon component type
 */
public enum ComponentType {
    MAGAZINE(WeaponMagazine.class),
    MODEL(WeaponModel.class),
    PROJECTILE(WeaponProjectile.class),
    RECOIL(WeaponRecoil.class),
    RELOAD(WeaponReload.class),
    EXPLOSION(WeaponExplosion.class),
    THROWABLE_PROJECTILE(ThrowableProjectile.class),
    FUSE(WeaponFuse.class),
    SOUND(WeaponSound.class),
    BUILDING_LEVEL(BuildingLevel.class),
    SENTRY_WEAPON(SentryBuildingWeapon.class),
    DISPENSER_HEALER(DispenserBuildingHealing.class),
    DISPENSER_AMO_REFILL(DispenserBuildingAmo.class),
    DISPENSER_METAL_REFILL(DispenserBuildingMetal.class);

    private final Class<? extends WeaponComponent> type;

    ComponentType(final Class<? extends WeaponComponent> type) {
        this.type = type;
    }

    /**
     * Get the component type
     *
     * @return the type
     */
    public Class<? extends WeaponComponent> getType() {
        return this.type;
    }
}
