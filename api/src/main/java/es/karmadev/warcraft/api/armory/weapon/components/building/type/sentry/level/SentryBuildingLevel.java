package es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry.level;

import es.karmadev.warcraft.api.armory.weapon.components.building.BuildingLevel;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry.SentryBuildingWeapon;

import java.util.Collection;

/**
 * Represents a sentry gun building
 * level
 */
public interface SentryBuildingLevel extends BuildingLevel {

    /**
     * Get the level weapons
     *
     * @return the sentry gun level
     * weapons
     */
    Collection<SentryBuildingWeapon> getWeapons();
}
