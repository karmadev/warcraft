package es.karmadev.warcraft.api.util.time;

/**
 * Utility class which allows going
 * from time units to minecraft ticks
 */
public final class TimeUtils {

    /**
     * Converse from game ticks into
     * human milliseconds
     *
     * @param ticks the game ticks
     * @return the ticks in milliseconds
     */
    public static long ticksToMilliseconds(final int ticks) {
        double ratio = (ticks / 20d); //Each 20 ticks is a second
        return (long) (ratio * 1000);
    }

    /**
     * Converse from game ticks into
     * human seconds
     *
     * @param ticks the game ticks
     * @return the ticks in seconds
     */
    public static long ticksToSeconds(final int ticks) {
        long ms = ticksToMilliseconds(ticks);
        return ms / 1000;
    }

    /**
     * Converse from game ticks into
     * human minutes
     *
     * @param ticks the game ticks
     * @return the ticks in minutes
     */
    public static long ticksToMinutes(final int ticks) {
        long seconds = ticksToSeconds(ticks);
        return seconds / 60;
    }

    /**
     * Converse from game ticks into
     * human hours. This should be
     * used very rarely, in fact, is not
     * even used by the plugin, but exists
     * to keep consistency between seconds,
     * minutes and hours
     *
     * @param ticks the game ticks
     * @return the ticks in hours
     */
    public static long ticksToHours(final int ticks) {
        long minutes = ticksToMinutes(ticks);
        return minutes / 60;
    }

    /**
     * Converse from milliseconds to
     * game ticks
     *
     * @param ms the milliseconds
     * @return the milliseconds in ticks
     */
    public static int millisecondsToTicks(final long ms) {
        double ratio = (double) (ms / 1000);
        return (int) (ratio * 20);
    }

    /**
     * Converse from seconds to game
     * ticks
     *
     * @param seconds the seconds
     * @return the seconds in ticks
     */
    public static int secondsToTicks(final long seconds) {
        return millisecondsToTicks(seconds * 1000);
    }

    /**
     * Converse from minutes to game
     * ticks
     *
     * @param minutes the minutes
     * @return the minutes in ticks
     */
    public static int minutesToTicks(final long minutes) {
        return secondsToTicks(minutes * 60);
    }

    /**
     * Converse from hours to game
     * ticks
     *
     * @param hours the hours
     * @return the hours in ticks
     */
    public static int hoursToTicks(final long hours) {
        return minutesToTicks(hours * 60);
    }

    /**
     * Get the string representation of
     * the milliseconds in a string format
     *
     * @param ms the milliseconds to display
     * @return the milliseconds in a string format
     */
    public static String formatMilliseconds(final long ms) {
        long minutes = (ms / (1000 * 60)) % 60;
        long seconds = (ms / 1000) % 60;

        if (minutes == 0 && seconds == 0)
            return String.format("%03d", ms);

        return String.format("%02d:%02d", minutes, seconds);
    }

    /**
     * Get the string representation of
     * the ticks in a string format
     *
     * @param ticks the ticks to display
     * @return the ticks in a string format
     */
    public static String formatTicks(final int ticks) {
        return formatMilliseconds(
                ticksToMilliseconds(ticks)
        );
    }
}