package es.karmadev.warcraft.api.plugin.armory;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.type.WeaponType;
import es.karmadev.warcraft.api.util.uuns.UUNS;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Represents the {@link ArmoryManager} weapon
 * manager
 */
public interface WeaponManager {

    /**
     * Get if the weapon manager contains
     * the specified weapon
     *
     * @param weapon the weapon
     * @return if the weapon manager contains
     * the weapon
     */
    default boolean contains(final Weapon weapon) {
        return weapon != null &&
                this.contains(weapon.getUUNS());
    }

    /**
     * Get if the weapon manager contains
     * a weapon with the specified name
     *
     * @param name the weapon name
     * @return if there's a weapon with the
     * specified name
     */
    boolean contains(final UUNS name);

    /**
     * Get all the weapons
     *
     * @return the weapons
     */
    Collection<Weapon> getAll();

    /**
     * Get all the weapons of the specified
     * type
     *
     * @param type the weapons of the specified type
     * @return the weapons
     */
    default Collection<Weapon> get(final WeaponType type) {
        return getAll().stream().filter((wp) -> wp.getWeaponType().equals(type))
                .collect(Collectors.toList());
    }

    /**
     * Get a weapon by its name
     *
     * @param name the weapon name
     * @return the weapon
     */
    default Weapon get(final UUNS name) {
        return getAll().stream().filter((wp) -> wp.getUUNS().equals(name))
                .findAny().orElse(null);
    }
}