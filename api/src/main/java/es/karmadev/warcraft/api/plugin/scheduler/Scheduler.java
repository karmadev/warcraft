package es.karmadev.warcraft.api.plugin.scheduler;

import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * WarCraft plugin scheduler
 */
public interface Scheduler {

    /**
     * Execute a task synchronously
     *
     * @param task the task to run
     */
    void executeSync(final Runnable task);

    /**
     * Execute a task asynchronously
     *
     * @param task the task to run
     */
    void executeAsync(final Runnable task);

    /**
     * Schedule a task synchronously
     *
     * @param runner the task runner
     * @param delay the task delay
     * @param unit the delay time unit
     */
    void scheduleSync(final Consumer<Scheduled> runner, final long delay, final TimeUnit unit);

    /**
     * Schedule a task asynchronously
     *
     * @param runner the task runner
     * @param delay the task delay
     * @param unit the delay time unit
     */
    void scheduleAsync(final Consumer<Scheduled> runner, final long delay, final TimeUnit unit);

    /**
     * Schedule a repeating task synchronously
     *
     * @param runner the task runner
     * @param delay the task delay
     * @param interval the task interval
     * @param unit the time unit
     */
    void scheduleRepeatingSync(final Consumer<Scheduled> runner, final long delay, final long interval, final TimeUnit unit);

    /**
     * Schedule a repeating task asynchronously
     *
     * @param runner the task runner
     * @param delay the task delay
     * @param interval the task interval
     * @param unit the time unit
     */
    void scheduleRepeatingAsync(final Consumer<Scheduled> runner, final long delay, final long interval, final TimeUnit unit);

    /**
     * Cancel a task by its id
     *
     * @param id the task ID to cancel
     * @return if the task was cancelled
     */
    boolean cancelTask(final long id);

    /**
     * Cancel the task instantly, without
     * waiting for it to complete the next
     * iteration
     *
     * @param id the task id to cancel
     */
    void cancelNow(final long id);

    /**
     * Get if the task is running
     *
     * @param id the task ID
     * @return if the task is running
     */
    boolean isRunning(final long id);

    /**
     * Get if the task has been cancelled
     *
     * @param id the task id
     * @return if the task has been cancelled
     */
    boolean isCancelled(final long id);

    /**
     * Cancel all the tasks of the
     * scheduler
     */
    void cancelAllTasks();

    /**
     * Get all the running tasks
     *
     * @return the tasks
     */
    Collection<? extends Scheduled> getTasks();
}
