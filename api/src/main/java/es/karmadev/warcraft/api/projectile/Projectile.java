package es.karmadev.warcraft.api.projectile;

import es.karmadev.warcraft.api.util.uuns.UUNS;

/**
 * Represents a projectile
 */
public interface Projectile {

    /**
     * Get the projectile UUNS
     *
     * @return the projectile UUNS
     */
    UUNS getUUNS();

    /**
     * Get the projectile's projectile type
     *
     * @return the projectile type
     */
    ProjectileType getType();
}
