package es.karmadev.warcraft.api.world.ent.construction;

import es.karmadev.warcraft.api.armory.weapon.components.building.BuildingLevel;
import es.karmadev.warcraft.api.armory.weapon.type.ConstructionWeapon;
import es.karmadev.warcraft.api.world.level.game.Game;
import es.karmadev.warcraft.api.world.level.player.Client;
import org.bukkit.Location;

/**
 * Represents a world construction
 */
public interface WorldConstruction {

    /**
     * Get the construction game
     *
     * @return the construction game
     */
    Game getGame();

    /**
     * Get the construction current
     * level
     *
     * @return the current level
     */
    int getLevel();

    /**
     * Set the construction level
     *
     * @param newLevel the new construction level
     */
    void setLevel(final int newLevel);

    /**
     * Upgrade the construction
     */
    default void upgrade() {
        ConstructionWeapon wp = this.getWeapon();
        int current = this.getLevel();
        int maxLevel = wp.getLevels().size();

        if (current == maxLevel) return;
        this.setLevel(current + 1);
    }

    /**
     * Downgrade the construction
     */
    default void downgrade() {
        int current = this.getLevel();
        if (current == 1) return;

        this.setLevel(current - 1);
    }

    /**
     * Get the construction weapon
     * data
     *
     * @return the construction data
     */
    ConstructionWeapon getWeapon();

    /**
     * Get the construction owner
     *
     * @return the owner
     */
    Client getOwner();

    /**
     * Get the construction location
     *
     * @return the location
     */
    Location getLocation();

    /**
     * Destroy the construction
     */
    void destroy();

    /**
     * Get the construction health
     *
     * @return the health
     */
    double getHealth();

    /**
     * Set the construction health
     *
     * @param newHealth the new construction
     *                  health
     */
    void setHealth(final double newHealth);

    /**
     * Damage the construction
     *
     * @param amount the amount to damage
     */
    default void damage(final double amount) {
        double newHealth = this.getHealth() - Math.abs(amount);
        if (newHealth <= 0) {
            destroy();
            return;
        }

        this.setHealth(newHealth);
    }

    /**
     * Repair the construction
     *
     * @param amount the amount to repair
     */
    default void repair(final double amount) {
        ConstructionWeapon wp = this.getWeapon();
        BuildingLevel level = wp.getLevel(this.getLevel());

        double newHealth = this.getHealth() + Math.abs(amount);
        if (newHealth > level.getHealth()) return;

        this.setHealth(newHealth);
    }
}
