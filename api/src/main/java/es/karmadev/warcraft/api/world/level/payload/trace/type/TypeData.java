package es.karmadev.warcraft.api.world.level.payload.trace.type;

/**
 * Payload fragment type data
 */
public interface TypeData {

    /**
     * Get the trace type
     *
     * @return the trace type
     */
    TraceType getType();
}
