package es.karmadev.warcraft.api.world.level.game.stats;

import es.karmadev.warcraft.api.armory.weapon.Weapon;

/**
 * Represents a game stats set
 */
public interface GameStats {

    /**
     * Get the client most used weapon. The
     * most used weapon is counted as the most
     * shoot one
     *
     * @return the most used client weapon
     */
    Weapon getFavouriteWeapon();

    /**
     * Get the client favourite game
     *
     * @return the favourite game
     */
    GameState getFavouriteGame();

    /**
     * Get the client total damage
     *
     * @return the client total
     * damage
     */
    double getTotalDamage();

    /**
     * Get the amount of times
     * the client killed someone
     *
     * @return the amount of client kills
     */
    int getKills();

    /**
     * Get the amount of times
     * the client died by someone
     *
     * @return the amount of client deaths
     */
    int getDeaths();

    /**
     * Get the client base precision,
     * based on missed shoots
     *
     * @return the client base precision
     */
    float getPrecission();
}