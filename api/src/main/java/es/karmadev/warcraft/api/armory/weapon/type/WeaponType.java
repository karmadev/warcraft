package es.karmadev.warcraft.api.armory.weapon.type;

/**
 * Represents a weapon type
 */
public enum WeaponType {
    /**
     * Represents a gun
     */
    GUN,
    /**
     * Represents an explosive
     */
    EXPLOSIVE,
    /**
     * Represents a melee weapon
     */
    MELEE,
    /**
     * Represents a throwable weapon
     */
    THROWABLE,
    /**
     * Represents a construction
     */
    CONSTRUCTION
}
