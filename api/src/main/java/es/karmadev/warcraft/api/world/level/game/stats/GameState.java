package es.karmadev.warcraft.api.world.level.game.stats;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.api.world.level.game.GameType;

import java.time.Instant;

/**
 * Represents a cached game
 * state
 */
public interface GameState {

    /**
     * Get the game UUNS
     *
     * @return the game UUNS
     */
    UUNS getUUNS();

    /**
     * Get the game name
     *
     * @return the game name
     */
    String getName();

    /**
     * Get the game type
     *
     * @return the game type
     */
    GameType getType();

    /**
     * Get the game state date
     *
     * @return the game state date
     */
    Instant getDate();

    /**
     * Get the amount of players which
     * played on this game
     *
     * @return the amount of players in
     * this game
     */
    int getPlayers();

    /**
     * Get the client total damage
     * in this game
     *
     * @return the client total damage
     */
    double getTotalDamage();

    /**
     * Get the client precision during
     * this game
     *
     * @return the client precision
     */
    float getPrecision();

    /**
     * Get the client position in
     * the game, a position of 1 means
     * he was the MVP
     *
     * @return the client position
     */
    int getScorePosition();

    /**
     * Get the client position in
     * his team, a position of 1 means
     * he was the MVP
     *
     * @return the client position
     */
    int getTeamPosition();

    /**
     * Get the client team on this
     * game
     *
     * @return the client team
     */
    Team getTeam();

    /**
     * Get the favourite weapon of
     * the client in this game
     *
     * @return the client's favourite
     * weapon
     */
    Weapon getFavouriteWeapon();
}
