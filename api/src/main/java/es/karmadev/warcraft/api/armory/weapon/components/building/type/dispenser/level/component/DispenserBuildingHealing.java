package es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.DispenserRefiller;

/**
 * Dispenser health re-filler
 */
public interface DispenserBuildingHealing extends WeaponComponent, DispenserRefiller {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.DISPENSER_HEALER;
    }
}
