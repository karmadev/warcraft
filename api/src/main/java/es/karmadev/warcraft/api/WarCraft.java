package es.karmadev.warcraft.api;

import es.karmadev.warcraft.api.plugin.armory.ArmoryManager;
import es.karmadev.warcraft.api.plugin.projectile.ProjectileManager;
import es.karmadev.warcraft.api.plugin.scheduler.Scheduler;
import es.karmadev.warcraft.api.world.level.game.stats.GameStats;
import org.bukkit.OfflinePlayer;

import java.nio.file.Path;

/**
 * Represents the war craft plugin
 */
public abstract class WarCraft {

    private static WarCraft instance;

    /**
     * Register the current war craft
     * instance as the warcraft plugin
     */
    protected final void register() {
        if (instance != null) return;
        WarCraft.instance = this;
    }

    /**
     * Get the plugin weapons directory
     *
     * @return the plugin weapons
     * directory
     */
    public abstract Path getWeaponsDirectory();

    /**
     * Get the plugin scheduler
     *
     * @return the plugin scheduler
     */
    public abstract Scheduler getScheduler();

    /**
     * Get the plugin projectile manager
     *
     * @return the projectile manager
     */
    public abstract ProjectileManager getProjectileManager();

    /**
     * Get the plugin armory manager
     *
     * @return the armory manager
     */
    public abstract ArmoryManager getArmoryManager();

    /**
     * Logs a message
     *
     * @param message the message
     * @param replacements the message replacements
     */
    public abstract void logDebug(final String message, final Object... replacements);

    /**
     * Logs a message
     *
     * @param message the message
     * @param replacements the message replacements
     */
    public abstract void logInfo(final String message, final Object... replacements);

    /**
     * Logs a message
     *
     * @param message the message
     * @param replacements the message replacements
     */
    public abstract void logWarning(final String message, final Object... replacements);

    /**
     * Logs a message
     *
     * @param message the message
     * @param replacements the message replacements
     */
    public abstract void logSevere(final String message, final Object... replacements);

    /**
     * Logs a message
     *
     * @param error the error
     * @param message the message
     * @param replacements the message replacements
     */
    public abstract void logError(final Throwable error, final String message, final Object... replacements);

    /**
     * Get the stats of a player
     *
     * @param player the player to get
     *               stats from
     * @return the player stats
     */
    public abstract GameStats getStats(final OfflinePlayer player);

    /**
     * Get the war craft plugin instance
     *
     * @return the plugin instance
     */
    public static WarCraft getInstance() {
        return WarCraft.instance;
    }
}
