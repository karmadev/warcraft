package es.karmadev.warcraft.api.world.level.game.respawn;

import es.karmadev.warcraft.api.world.level.player.Client;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * Represents the game respawn
 * handler
 */
public interface RespawnHandler {

    /**
     * Add the client to the respawn
     * que
     *
     * @param client the client to add
     * @param respawnTime the respawn time
     */
    void addToSpawn(final Client client, final int respawnTime);

    /**
     * Removes a client from the
     * respawn queue
     *
     * @param client the client to remove
     */
    void removeFromSpawn(final Client client);

    /**
     * Get if the client is currently in
     * the respawn handler que
     *
     * @param client the client
     * @return if the client is in the queue
     */
    boolean isInQueue(final Client client);

    /**
     * Get a client respawn data
     *
     * @param client the client
     * @return the client respawn data
     */
    @Nullable
    RespawnData getRespawnData(final Client client);

    /**
     * Get if the game supports instant
     * respawn times
     *
     * @return if the game respawns the
     * clients instantly
     */
    boolean isInstantRespawn();

    /**
     * Set if the clients respawn
     * instantly
     *
     * @param status whether the clients should
     *               respawn instantly
     */
    void setInstantRespawn(final boolean status);

    /**
     * Get the respawn queue
     *
     * @return the queue
     */
    Collection<Client> getQueue();
}
