package es.karmadev.warcraft.api.script;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Defines a class that must be
 * accessible through lua API
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface LuaAccess {

    /**
     * Define the lua access
     * for the class
     *
     * @return if the access is allowed
     * (defaults to false)
     */
    boolean allow() default false;
}
