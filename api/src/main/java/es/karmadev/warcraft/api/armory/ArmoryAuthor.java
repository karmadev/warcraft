package es.karmadev.warcraft.api.armory;

/**
 * Represents an armory author
 */
public interface ArmoryAuthor {

    /**
     * Get the author name
     *
     * @return the author name
     */
    String getName();

    /**
     * Get the author website
     *
     * @return the author website
     */
    String getWebsite();

    /**
     * Get the author contact information
     *
     * @return the contact information
     */
    String getContact();
}
