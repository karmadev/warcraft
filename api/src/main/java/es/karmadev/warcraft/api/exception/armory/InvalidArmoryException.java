package es.karmadev.warcraft.api.exception.armory;

/**
 * Represents an exception when an invalid
 * armory is tried to be parsed
 */
public class InvalidArmoryException extends Exception {

    /**
     * Initialize the exception
     *
     * @param format the format that is tried to be
     *               parsed
     * @param reason the fail reason
     */
    public InvalidArmoryException(final String format, final String reason) {
        super("Failed to parse armory as " + format + ": " + reason);
    }
}
