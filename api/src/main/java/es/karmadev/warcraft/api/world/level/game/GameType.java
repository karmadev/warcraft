package es.karmadev.warcraft.api.world.level.game;

/**
 * Represents a game type
 */
public enum GameType {
    /**
     * Payload
     */
    PAYLOAD,
    /**
     * Capture the flag
     */
    CAPTURE_THE_FLAG,
    /**
     * King of the hill
     */
    KING_OF_THE_HILL,
    /**
     * Control points
     */
    CONTROL_POINTS,
    /**
     * Attack/defense
     */
    ATTACK_DEFENSE,
    /**
     * Team death match
     */
    TEAM_DEATH_MATCH,
    /**
     * Anarchy (everyone vs everyone)
     */
    ANARCHY,
    /**
     * Gun game
     */
    GUN_GAME,
    /**
     * Infection
     */
    INFECTION
}
