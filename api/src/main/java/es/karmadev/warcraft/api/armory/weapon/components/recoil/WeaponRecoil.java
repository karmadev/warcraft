package es.karmadev.warcraft.api.armory.weapon.components.recoil;

import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;

/**
 * Represents a weapon recoil
 */
public interface WeaponRecoil extends WeaponComponent {

    /**
     * Get the weapon component type
     *
     * @return the component type
     */
    @Override
    default ComponentType getComponentType() {
        return ComponentType.RECOIL;
    }

    /**
     * Get the weapon minimum recoil
     *
     * @return the min recoil
     */
    double getMin();

    /**
     * Get the weapon max recoil
     *
     * @return the max recoil
     */
    double getMax();


}
