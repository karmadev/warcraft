# Contributing to WarCraft

🎉 First off, thank you for considering contributing to WarCraft! 🎉

WarCraft is an open-source project, and we welcome contributions from everyone. By participating, you're not only improving the project but also becoming a part of the KarmaDev community. Before getting started, please take a moment to review this document to ensure a smooth contribution process.

## Ways to Contribute

There are several ways to contribute to WarCraft:

1. **Reporting Issues:** If you encounter a bug or have a suggestion for improvement, please open an issue on our [GitLab repository](https://gitlab.com/karmadev/warcraft/-/issues).

2. **Feature Requests:** Have an idea for a new feature? We'd love to hear it! Open an issue and describe the feature you have in mind.

3. **Code Contributions:** Feel free to submit pull requests with bug fixes, enhancements, or new features. Make sure to follow the guidelines outlined below.

## Getting Started

To contribute to WarCraft, follow these steps:

1. Fork the [WarCraft repository](https://gitlab.com/karmadev/warcraft) on GitLab.

2. Clone your forked repository to your local machine:

    ```bash
    git clone https://gitlab.com/your-username/WarCraft.git
    ```

3. Create a new branch for your contribution:

    ```bash
    git checkout -b feature/your-feature
    ```

4. Make your changes and commit them:

    ```bash
    git add .
    git commit -m "Add your commit message here"
    ```

5. Push your changes to your fork:

    ```bash
    git push origin feature/your-feature
    ```

6. Open a pull request on the main WarCraft repository. Be sure to provide a clear description of your changes.

## Code Style and Guidelines

To maintain consistency across the codebase, please adhere to the following guidelines:

- Follow the existing code style and naming conventions.
- Write clear and concise commit messages.
- Include comments in your code, especially for complex sections or algorithms.
- Test your changes thoroughly before submitting a pull request.

## License

By contributing to WarCraft, you agree that your contributions will be licensed under the [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

## Contact

If you have any questions or need assistance, feel free to reach out to us:

- **Website:** [KarmaDev](https://karmadev.es)
- **Donations:** [Buy Me a Coffee](https://buymeacoffee.com/karmadev)
- **GitHub:** [KarmaDev on GitHub](https://github.com/KarmaDev)
- **GitLab** [KarmaDev on GitLab](https://gitlab.com/karmadev)
- **Discord** [Discord](https://discord.reddo.es)

Thank you for your contributions and for being a part of the WarCraft project!