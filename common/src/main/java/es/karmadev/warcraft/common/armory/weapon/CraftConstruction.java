package es.karmadev.warcraft.common.armory.weapon;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.building.BuildingLevel;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry.SentryBuildingWeapon;
import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.armory.weapon.type.BuildType;
import es.karmadev.warcraft.api.armory.weapon.type.ConstructionWeapon;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.building.CraftDispenserLevel;
import es.karmadev.warcraft.common.armory.weapon.components.building.CraftSentryLevel;
import es.karmadev.warcraft.common.armory.weapon.components.building.level.CraftSentryWeapon;
import es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser.CraftDispenserAmo;
import es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser.CraftDispenserHeal;
import es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser.CraftDispenserMetal;
import es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser.CraftRefiller;
import org.jetbrains.annotations.Contract;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class CraftConstruction extends CraftWeapon implements ConstructionWeapon {

    private final Set<BuildingLevel> levels = new HashSet<>();
    private BuildType type;

    /**
     * Create a new construction
     *
     * @param armory the construction armory
     * @param name the construction name
     * @param weaponJson the weapon json
     * @param levels the construction levels
     * @throws InvalidWeaponException if the weapon data
     * is invalid
     */
    public CraftConstruction(final Armory armory, final String name,
                             final JsonObject weaponJson, Collection<JsonObject> levels) throws InvalidWeaponException {
        super(armory, name);
        map(weaponJson, levels);
    }

    /**
     * Get the construction type
     *
     * @return the type
     */
    @Override
    public BuildType getBuildingType() {
        return this.type;
    }

    /**
     * Get the building level
     *
     * @param level the level
     * @return the building level
     */
    @Override
    public BuildingLevel getLevel(final int level) {
        return this.levels.stream().filter((lvl) -> lvl.getLevel() == level)
                .findFirst().orElse(null);
    }

    /**
     * Get all the building levels
     *
     * @return the building levels
     */
    @Override
    public Collection<BuildingLevel> getLevels() {
        return this.levels.stream().sorted(Comparator.comparingInt(BuildingLevel::getLevel))
                .collect(Collectors.toList());
    }

    /**
     * Get a weapon component by
     * component type
     *
     * @param type  the component type
     * @param cType the component object type
     * @return the weapon component
     */
    @Override
    public <T extends WeaponComponent> T getComponent(final ComponentType type, final Class<T> cType) {
        throw new UnsupportedOperationException("Constructions does not allow direct component fetching");
    }

    /**
     * Get all the weapon components
     *
     * @return the weapon components
     */
    @Override
    public Collection<? extends WeaponComponent> getComponents() {
        return Collections.emptyList();
    }

    /**
     * Get if the weapon has available
     * the specified component
     *
     * @param type the component type
     * @return if the weapon has teh component
     */
    @Override
    public boolean hasComponent(final ComponentType type) {
        return false;
    }

    /**
     * Get the string representation of
     * the gun
     *
     * @return the gun as a string
     */
    @Override
    public String toString() {
        return String.format("CraftConstruction{\"armory\": %s, \"uuns\": \"%s\", \"name\": \"%s\", \"type\": %s, " +
                "\"levels\": %d}",
                this.armory, this.getUUNS(), this.name, this.type, this.levels.size());
    }

    /**
     * Map the weapon
     *
     * @param weaponJson the weapon json
     * @param levels the weapon levels
     * @throws InvalidWeaponException if the weapon is invalid
     */
    @Contract("null, null -> fail")
    private void map(final JsonObject weaponJson, final Collection<JsonObject> levels) throws InvalidWeaponException {
        if (weaponJson == null || levels == null)
            throw new InvalidWeaponException(this.armory, this.name, "Cannot parse construction for invalid data");

        ensureNotMissing(weaponJson, "description", "type");
        ensurePropType(weaponJson, JsonArray.class, "description");
        ensurePropType(weaponJson, JsonNative.class, "type");

        JsonArray description = weaponJson.asArray("description");
        mapDescription(description);

        JsonNative type = weaponJson.asNative("type");

        ensureString(type);
        this.type = BuildType.valueOf(
                type.getString().toUpperCase()
        );

        handleLevels(levels);
    }

    private void handleLevels(final Collection<JsonObject> levels) {
        AtomicInteger level = new AtomicInteger(1);
        for (JsonObject object : levels) {
            this.levels.add(processLevel(object, level));
        }
    }

    private BuildingLevel processLevel(final JsonObject level, final AtomicInteger levelValue) {
        BuildingLevel value;
        switch (this.type) {
            case SENTRY:
                value = processSentryLevel(level, levelValue.get());
                break;
            case DISPENSER:
                value = processDispenserLevel(level, levelValue.get());
                break;
            case TELEPORT_EXIT:
            case TELEPORT_ENTRANCE:
            default:
                value = null;
        }

        if (value != null)
            levelValue.getAndIncrement();

        return value;
    }

    private BuildingLevel processSentryLevel(final JsonObject object, final int level) {
        try {
            ensureNotMissing(object, "radius", "health", "weapons");

            ensurePropType(object, JsonNative.class, "radius");
            ensurePropType(object, JsonArray.class, "weapons");

            JsonNative radius = object.asNative("radius");
            JsonNative health = object.asNative("health");
            JsonArray weapons = object.asArray("weapons");

            ensureNumber(radius, health);

            Collection<SentryBuildingWeapon> sentryWeapons = parseSentryWeapons(weapons);
            return new CraftSentryLevel(level, radius.getDouble(), health.getDouble(), sentryWeapons);
        } catch (InvalidWeaponException ignored) {}

        return null;
    }

    private BuildingLevel processDispenserLevel(final JsonObject object, final int level) {
        try {
            ensureNotMissing(object, "radius", "health", "healing", "ammo", "metal", "max_metal");
            ensurePropType(object, JsonNative.class, "radius_override", "max_metal");

            ensurePropType(object, JsonObject.class, "healing", "ammo", "metal");

            JsonNative radius = object.asNative("radius");
            JsonNative health = object.asNative("health");
            JsonObject healing = object.asObject("healing");
            JsonObject ammo = object.asObject("ammo");
            JsonObject metal = object.asObject("metal");
            JsonNative maxMetal = object.asNative("max_metal");

            ensureNumber(radius, health, maxMetal);

            CraftDispenserHeal healthFiller = parseElement(healing, ComponentType.DISPENSER_HEALER, CraftDispenserHeal.class);
            CraftDispenserAmo ammoFiller = parseElement(ammo, ComponentType.DISPENSER_AMO_REFILL, CraftDispenserAmo.class);
            CraftDispenserMetal metalFiller = parseElement(metal, ComponentType.DISPENSER_METAL_REFILL, CraftDispenserMetal.class);

            return new CraftDispenserLevel(
                    level, radius.getDouble(), health.getDouble(),
                    healthFiller, ammoFiller, metalFiller,
                    maxMetal.getInteger()
            );
        } catch (InvalidWeaponException ignored) {}

        return null;
    }

    private <T extends CraftRefiller> T parseElement(final JsonObject object, final ComponentType type, final Class<T> typeClass) throws InvalidWeaponException {
        ensureNotMissing(object, "amount", "interval");
        ensurePropType(object, JsonNative.class, "amount", "interval");

        JsonNative amount = object.asNative("amount");
        JsonNative interval = object.asNative("interval");

        ensureNumber(amount);

        switch (type) {
            case DISPENSER_HEALER:
                return typeClass.cast(
                        new CraftDispenserHeal(amount.getDouble(), interval.asString())
                );
            case DISPENSER_AMO_REFILL:
                return typeClass.cast(
                    new CraftDispenserAmo(amount.getInteger(), interval.asString())
                );
            case DISPENSER_METAL_REFILL:
                return typeClass.cast(
                        new CraftDispenserMetal(amount.getInteger(), interval.asString())
                );
            default:
                throw new IllegalStateException("Unexpected component type " + type);
        }
    }

    private Collection<SentryBuildingWeapon> parseSentryWeapons(final JsonArray weapons) {
        List<SentryBuildingWeapon> weaponList = new ArrayList<>();

        for (JsonInstance instance : weapons) {
            if (!instance.isObjectType()) continue;
            CraftSentryWeapon weapon = readWeapon(instance.asObject());
            if (weapon == null) continue;

            weaponList.add(weapon);
        }

        return weaponList;
    }

    private CraftSentryWeapon readWeapon(final JsonObject object) {
        try {
            ensureNotMissing(object, "projectile", "ammo", "damage", "cadence",
                    "reload", "sounds", "sounds.shoot", "sounds.empty_shoot",
                    "sounds.reload", "amount");

            ensurePropType(object, JsonNative.class, "projectile", "ammo", "damage", "cadence",
                    "reload", "amount");
            ensurePropType(object, JsonArray.class, "sounds.shoot", "sounds.empty_shoot");
            ensurePropType(object, JsonObject.class, "sounds.reload");

            JsonNative projectile = object.asNative("projectile");
            JsonNative ammo = object.asNative("ammo");
            JsonNative damage = object.asNative("damage");
            JsonNative cadence = object.asNative("cadence");
            JsonNative reload = object.asNative("reload");

            JsonArray soundsShoot = object.asArray("sounds.shoot");
            JsonArray soundsEmptyShoot = object.asArray("sounds.empty_shoot");
            JsonObject soundsReload = object.asObject("sounds.reload");

            JsonNative amount = object.asNative("amount");

            ensureString(projectile);
            ensureNumber(ammo, damage, amount);

            String projName = projectile.getString();


            ProjectileType projType = projName.equalsIgnoreCase("rocket") ? ProjectileType.ROCKET :
                    projName.equalsIgnoreCase("shell") ? ProjectileType.SHELL : ProjectileType.WEAPON;

            CraftSoundSet shootSounds = buildSoundSet(soundsShoot, "0");
            CraftSoundSet emptyShootSounds = buildSoundSet(soundsEmptyShoot, "0");
            CraftSoundSet reloadSounds = buildSoundSet(soundsReload, reload.asString());

            return new CraftSentryWeapon(projType, projName, ammo.getInteger(),
                    damage.getDouble(), cadence.asString(), reload.asString(), shootSounds,
                    emptyShootSounds, reloadSounds, amount.getInteger());
        } catch (InvalidWeaponException ignored) {}

        return null;
    }
}