package es.karmadev.warcraft.common.lua.event.proxy;

import es.karmadev.warcraft.common.lua.event.EventProxy;
import org.bukkit.event.player.PlayerJoinEvent;

public interface PlayerJoin extends EventProxy<PlayerJoinEvent> {

    @Override
    void handle(final PlayerJoinEvent event);
}