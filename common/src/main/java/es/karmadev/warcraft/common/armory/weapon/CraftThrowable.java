package es.karmadev.warcraft.common.armory.weapon;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.fuse.WeaponFuse;
import es.karmadev.warcraft.api.armory.weapon.components.model.ModelType;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.api.armory.weapon.components.projectile.ThrowableProjectile;
import es.karmadev.warcraft.api.armory.weapon.type.ThrowableWeapon;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.fuse.CraftFuse;
import es.karmadev.warcraft.common.armory.weapon.components.model.CraftModel;
import es.karmadev.warcraft.common.armory.weapon.components.projectile.CraftThrowableProjectile;
import org.jetbrains.annotations.Contract;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;

public class CraftThrowable extends CraftWeaponContainer implements ThrowableWeapon {

    private final static EnumSet<ComponentType> supportedComponents = EnumSet.of(ComponentType.MODEL,
            ComponentType.FUSE, ComponentType.THROWABLE_PROJECTILE);

    private final Weapon base;

    private WeaponModel model;
    private WeaponFuse fuse;
    private ThrowableProjectile projectile;

    /**
     * Create a throwable weapon
     *
     * @param base the weapon base to read
     *             from
     * @param weaponJson the weapon json
     * @throws InvalidWeaponException if the weapon data
     * is invalid
     */
    public CraftThrowable(final Weapon base, final JsonObject weaponJson) throws InvalidWeaponException {
        super(base.getArmory(), base.getName());

        this.base = base;
        this.description = base.getDescription();
        map(weaponJson);
    }

    /**
     * Get the weapon base of the
     * throwable weapon
     *
     * @return the weapon base
     */
    @Override
    public Weapon getWeaponBase() {
        return this.base;
    }

    /**
     * Get the thrown weapon model
     *
     * @return the throw model
     */
    @Override
    public WeaponModel getModel() {
        return this.model;
    }

    /**
     * Get the thrown weapon fuse time
     *
     * @return the weapon fuse
     */
    @Override
    public WeaponFuse getFuse() {
        return this.fuse;
    }

    /**
     * Get the thrown weapon projectile
     *
     * @return the weapon projectile
     */
    @Override
    public ThrowableProjectile getProjectile() {
        return this.projectile;
    }

    /**
     * Get a weapon component by
     * component type
     *
     * @param type  the component type
     * @param cType the component object type
     * @return the weapon component
     */
    @Override
    public <T extends WeaponComponent> T getComponent(ComponentType type, final Class<T> cType) {
        if (type == null || cType == null)
            return null;

        if (type.equals(ComponentType.PROJECTILE) &&
                cType.isAssignableFrom(ThrowableProjectile.class))
            type = ComponentType.THROWABLE_PROJECTILE;

        if (!CraftThrowable.supportedComponents.contains(type))
            throw new IllegalArgumentException("Component " + type + " is not supported");

        switch (type) {
            case MODEL:
                if (this.model == null) return null;
                if (cType.isAssignableFrom(this.model.getClass()))
                    return cType.cast(this.model);

                throw new IllegalArgumentException("Cannot get component model of type " + cType.getSimpleName() + " for throwable weapon");
            case FUSE:
                if (this.fuse == null) return null;
                if (cType.isAssignableFrom(this.fuse.getClass()))
                    return cType.cast(this.fuse);

                throw new IllegalArgumentException("Cannot get component explosion of type " + cType.getSimpleName() + " for throwable weapon");
            case THROWABLE_PROJECTILE:
                if (this.projectile == null) return null;
                if (cType.isAssignableFrom(this.projectile.getClass()))
                    return cType.cast(this.projectile);

                throw new IllegalArgumentException("Cannot get component explosion of type " + cType.getSimpleName() + " for throwable weapon");
            default:
                throw new IllegalArgumentException("No such component of type " + cType.getSimpleName() + " for throwable weapon");
        }
    }

    /**
     * Get all the weapon components
     *
     * @return the weapon components
     */
    @Override
    public Collection<? extends WeaponComponent> getComponents() {
        return Arrays.asList(this.model, this.fuse, this.projectile);
    }

    /**
     * Get if the weapon has available
     * the specified component
     *
     * @param type the component type
     * @return if the weapon has teh component
     */
    @Override
    public boolean hasComponent(final ComponentType type) {
        return CraftThrowable.supportedComponents.contains(type);
    }

    /**
     * Get the string representation of
     * the gun
     *
     * @return the gun as a string
     */
    @Override
    public String toString() {
        return String.format("CraftThrowable{\"armory\": %s, \"uuns\": \"%s\", \"name\": \"%s\", \"model type\": %s, \"model\": \"%s\", " +
                        "\"explosion fuse\": %d, \"fuse tick sounds\": %d, \"fuse explode sounds\": %d, \"projectile bounce\": %s, " +
                        "\"projectile speed\": %.2f, \"projectile constant speed\": %s, \"projectile explode on hit\": %s, " +
                        "\"base weapon\": %s}",
                this.armory, this.getUUNS(), this.name, this.model.getType(), this.model.getModelData(),
                this.fuse.getFuseTime(), this.fuse.getTickSounds().size(), this.fuse.getExplodeSounds().size(),
                this.projectile.isBounce(), this.projectile.getSpeed(), this.projectile.isConstantSpeed(),
                this.projectile.isExplodeOnHit(), this.base);
    }

    /**
     * Map the weapon
     *
     * @param weaponJson the weapon json
     * @throws InvalidWeaponException if the weapon is invalid
     */
    @Contract("null -> fail")
    private void map(final JsonObject weaponJson) throws InvalidWeaponException {
        if (weaponJson == null)
            throw new InvalidWeaponException(this.base.getArmory(), this.base.getName(), "Cannot parse throwable weapon for invalid data");

        ensureNotMissing(weaponJson,
                "model", "model.type", "model.name",
                "fuse.time", "fuse.sounds", "fuse.sounds.tick",
                "fuse.sounds.explode", "projectile", "projectile.bounce",
                "projectile.speed", "projectile.constant_speed",
                "projectile.explode_on_hit"
        );

        ensurePropType(weaponJson, JsonObject.class, "model", "fuse", "fuse.sounds", "fuse.sounds.tick");
        ensurePropType(weaponJson, JsonArray.class, "fuse.sounds.explode");
        ensurePropType(weaponJson, JsonNative.class,
                "model.type", "model.name",
                "fuse.time", "projectile.bounce", "projectile.speed",
                "projectile.constant_speed", "projectile.explode_on_hit");

        JsonNative modelType = weaponJson.asNative("model.type");
        JsonNative modelName = weaponJson.asNative("model.name");
        JsonNative fuseTime = weaponJson.asNative("fuse.time");
        JsonObject fuseTickSounds = weaponJson.asObject("fuse.sounds.tick");
        JsonArray fuseExplosionSounds = weaponJson.asArray("fuse.sounds.explode");
        JsonNative projBounce = weaponJson.asNative("projectile.bounce");
        JsonNative projSpeed = weaponJson.asNative("projectile.speed");
        JsonNative projConsSpeed = weaponJson.asNative("projectile.constant_speed");
        JsonNative projExplodeOnHit = weaponJson.asNative("projectile.explode_on_hit");

        ensureString(modelType, modelName);
        ensureBoolean(projBounce, projConsSpeed, projExplodeOnHit);
        ensureNumber(projSpeed);

        this.model = new CraftModel(ModelType.valueOf(modelType.getString().toUpperCase()),
                modelName.getString());

        CraftSoundSet tickSounds = buildSoundSet(fuseTickSounds, fuseTime.asString());
        CraftSoundSet explosionSounds = buildSoundSet(fuseExplosionSounds, "0");
        this.fuse = new CraftFuse(fuseTime.asString(), tickSounds, explosionSounds);

        this.projectile = new CraftThrowableProjectile(projBounce.getBoolean(),
                projSpeed.getDouble(),
                projConsSpeed.getBoolean(),
                projExplodeOnHit.getBoolean());
    }
}
