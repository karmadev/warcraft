package es.karmadev.warcraft.common.armory.weapon;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.magazine.WeaponMagazine;
import es.karmadev.warcraft.api.armory.weapon.components.model.ModelType;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.armory.weapon.components.projectile.WeaponProjectile;
import es.karmadev.warcraft.api.armory.weapon.components.recoil.WeaponRecoil;
import es.karmadev.warcraft.api.armory.weapon.components.reload.WeaponReload;
import es.karmadev.warcraft.api.armory.weapon.type.GunWeapon;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.magazine.CraftMagazine;
import es.karmadev.warcraft.common.armory.weapon.components.model.CraftModel;
import es.karmadev.warcraft.common.armory.weapon.components.projectile.CraftProjectile;
import es.karmadev.warcraft.common.armory.weapon.components.recoil.CraftRecoil;
import es.karmadev.warcraft.common.armory.weapon.components.reload.CraftReload;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@SuppressWarnings("unused")
public class CraftGun extends CraftWeapon implements GunWeapon {

    private final static EnumSet<ComponentType> supportedComponents = EnumSet.of(
            ComponentType.MODEL, ComponentType.MAGAZINE, ComponentType.PROJECTILE,
            ComponentType.RECOIL, ComponentType.RELOAD
    );

    private WeaponModel model;
    private WeaponMagazine magazine;
    private WeaponProjectile projectile;
    private WeaponRecoil recoil;
    private WeaponReload reload;

    private boolean automatic = false;
    private long cadence = 0L;

    /**
     * Create a gun
     *
     * @param armory the gun armory
     * @param name the gun name
     * @param weaponObject the weapon json data
     * @throws InvalidWeaponException if the weapon data
     * is invalid
     */
    public CraftGun(final Armory armory, final String name, final JsonObject weaponObject) throws InvalidWeaponException {
        super(armory, name);
        map(weaponObject);
    }

    /**
     * Get the weapon model
     *
     * @return the weapon model
     */
    @Override
    public WeaponModel getModel() {
        return this.model;
    }

    /**
     * Get the weapon magazine
     *
     * @return the weapon magazine
     */
    @Override
    public WeaponMagazine getMagazine() {
        return this.magazine;
    }

    /**
     * Get the weapon projectile
     *
     * @return the weapon projectile
     */
    @Override
    public WeaponProjectile getProjectile() {
        return this.projectile;
    }

    /**
     * Get the weapon recoil
     *
     * @return the weapon recoil
     */
    @Override
    public WeaponRecoil getRecoil() {
        return this.recoil;
    }

    /**
     * Get the weapon reload
     *
     * @return the weapon reload
     */
    @Override
    public WeaponReload getReload() {
        return this.reload;
    }

    /**
     * Get if the weapon is automatic
     *
     * @return if the weapon is auto
     * matic
     */
    @Override
    public boolean isAutomatic() {
        return this.automatic;
    }

    /**
     * Get the weapon cadence
     *
     * @return the weapon cadence
     */
    @Override
    public long getCadence() {
        return this.cadence;
    }

    /**
     * Get a weapon component by
     * component type
     *
     * @param type  the component type
     * @param cType the component object type
     * @return the weapon component
     * @throws IllegalArgumentException if the weapon does not support
     * the component type or the component type is not supported
     * by the component implementation
     */
    @Override @Nullable
    @Contract("null, null -> null")
    public <T extends WeaponComponent> T getComponent(final ComponentType type, final Class<T> cType) throws IllegalArgumentException {
        if (type == null || cType == null)
            return null;

        if (!CraftGun.supportedComponents.contains(type))
            throw new IllegalArgumentException("Component " + type + " is not supported");

        switch (type) {
            case MODEL:
                if (this.model == null) return null;
                if (cType.isAssignableFrom(this.model.getClass()))
                    return cType.cast(this.model);

                throw new IllegalArgumentException("Cannot get component model of type " + cType.getSimpleName() + " for gun");
            case PROJECTILE:
                if (this.projectile == null) return null;
                if (cType.isAssignableFrom(this.projectile.getClass()))
                    return cType.cast(this.projectile);

                throw new IllegalArgumentException("Cannot get component projectile of type " + cType.getSimpleName() + " for gun");
            case MAGAZINE:
                if (this.magazine == null) return null;
                if (cType.isAssignableFrom(this.magazine.getClass()))
                    return cType.cast(this.magazine);

                throw new IllegalArgumentException("Cannot get component magazine of type " + cType.getSimpleName() + " for gun");
            case RECOIL:
                if (this.recoil == null) return null;
                if (cType.isAssignableFrom(this.recoil.getClass()))
                    return cType.cast(this.recoil);

                throw new IllegalArgumentException("Cannot get component recoil of type " + cType.getSimpleName() + " for gun");
            case RELOAD:
                if (this.reload == null) return null;
                if (cType.isAssignableFrom(this.reload.getClass()))
                    return cType.cast(this.reload);

                throw new IllegalArgumentException("Cannot get component reload of type " + cType.getSimpleName() + " for gun");
            case SOUND:
                throw new IllegalArgumentException("Sound is not a fetch-able component");
            default:
                throw new IllegalArgumentException("No such component of type " + cType.getSimpleName() + " for gun");
        }
    }

    /**
     * Get all the weapon components
     *
     * @return the weapon components
     */
    @Override
    public Collection<? extends WeaponComponent> getComponents() {
        return Arrays.asList(this.model, this.projectile,
                this.magazine, this.recoil, this.reload);
    }

    /**
     * Get if the weapon has available
     * the specified component
     *
     * @param type the component type
     * @return if the weapon has teh component
     */
    @Override @Contract("null -> false")
    public boolean hasComponent(final ComponentType type) {
        if (type == null)
            return false;

        return CraftGun.supportedComponents.contains(type) &&
                getComponent(type, type.getType()) != null;
    }

    /**
     * Get the string representation of
     * the gun
     *
     * @return the gun as a string
     */
    @Override
    public String toString() {
        return String.format("CraftGunWeapon{\"armory\": %s, \"uuns\": \"%s\", \"name\": \"%s\", \"model type\": %s, \"model\": \"%s\", " +
                        "\"magazine chamber\": %d, \"magazine capacity\": %d, \"magazine sounds\": %d, \"min recoil\": %.2f, \"max recoil\": %.2f, " +
                        "\"projectile\": \"%s\", \"projectile type\": %s, \"projectile speed\": %.2f, \"projectile distance\": %d, \"projectile distance penalty\": %d, " +
                        "\"projectile damage\": %.2f, \"projectile damage penalty\": %.2f, \"projectile sounds\": %d, \"reload time\": %d, " +
                        "\"reload sounds\": %d, \"automatic\": %b, \"cadence\": %d}",
                this.armory, this.getUUNS(), this.name, this.model.getType(), this.model.getModelData(),
                this.magazine.getChamber(), this.magazine.getCapacity(), this.magazine.getEmptySounds().size(),
                this.recoil.getMin(), this.recoil.getMax(), this.projectile.getProjectileName(), this.projectile.getType(),
                this.projectile.getSpeed(), this.projectile.getEffectiveDistance(), this.projectile.getDistancePenalty(),
                this.projectile.getDamage(), this.projectile.getDamagePenalty(), this.projectile.getSounds().size(),
                this.reload.getTime(), this.reload.getSounds().size(), this.automatic, this.cadence);
    }

    /**
     * Map the weapon
     *
     * @param weaponJson the weapon json
     * @throws InvalidWeaponException if the weapon is invalid
     */
    @Contract("null -> fail")
    private void map(final JsonObject weaponJson) throws InvalidWeaponException {
        if (weaponJson == null)
            throw new InvalidWeaponException(this.armory, this.name, "Cannot parse gun for invalid data");

        ensureNotMissing(weaponJson,
                "description",
                "model", "model.type", "model.name",
                "magazine", "magazine.chamber", "magazine.capacity", "magazine.empty_sounds",
                "recoil", "recoil.min", "recoil.max",
                "projectile", "projectile.type", "projectile.speed", "projectile.effective_distance",
                "projectile.distance_penalty", "projectile.damage", "projectile.damage_penalty",
                "projectile.sounds",
                "reload", "reload.time", "reload.sounds",
                "automatic", "cadence"
        );

        ensurePropType(weaponJson, JsonArray.class, "description",  "projectile.sounds", "magazine.empty_sounds");
        ensurePropType(weaponJson, JsonObject.class,
                "model", "magazine", "reload.sounds", "recoil");
        ensurePropType(weaponJson, JsonNative.class,
                "model.type", "model.name",
                "magazine.chamber", "magazine.capacity",
                "recoil.min", "recoil.max",
                "projectile.type", "projectile.speed",
                "projectile.effective_distance", "projectile.distance_penalty",
                "projectile.damage", "projectile.damage_penalty",
                "reload.time", "automatic", "cadence");

        JsonArray descriptionArray = weaponJson.asArray("description");
        JsonNative modelType = weaponJson.asNative("model.type");
        JsonNative modelName = weaponJson.asNative("model.name");
        JsonNative magChamber = weaponJson.asNative("magazine.chamber");
        JsonNative magCapacity = weaponJson.asNative("magazine.capacity");
        JsonArray magEmptySounds = weaponJson.asArray("magazine.empty_sounds");
        JsonNative recoilMin = weaponJson.asNative("recoil.min");
        JsonNative recoilMax = weaponJson.asNative("recoil.max");
        JsonNative projType = weaponJson.asNative("projectile.type");
        JsonNative projSpeed = weaponJson.asNative("projectile.speed");
        JsonNative projDist = weaponJson.asNative("projectile.effective_distance");
        JsonNative projDistPenalty = weaponJson.asNative("projectile.distance_penalty");
        JsonNative projDamage = weaponJson.asNative("projectile.damage");
        JsonNative projDamagePenalty = weaponJson.asNative("projectile.damage_penalty");
        JsonArray projSounds = weaponJson.asArray("projectile.sounds");
        JsonNative reloadTime = weaponJson.asNative("reload.time");
        JsonObject reloadSounds = weaponJson.asObject("reload.sounds");
        JsonNative automatic = weaponJson.asNative("automatic");
        JsonNative cadence = weaponJson.asNative("cadence");

        ensureString(modelType, modelName, projType);
        ensureNumber(magChamber, magCapacity, recoilMin, recoilMax,
                projSpeed, projDist, projDistPenalty,
                projDamage, projDamagePenalty);
        ensureBoolean(automatic);

        mapDescription(descriptionArray);
        this.model = new CraftModel(ModelType.valueOf(modelType.getString().toUpperCase()),
                modelName.getString());

        CraftSoundSet magazineSoundSet = buildSoundSet(magEmptySounds, "0");
        this.magazine = new CraftMagazine(magChamber.getInteger(), magCapacity.getInteger(),
                magazineSoundSet);

        String projectileName = projType.getString();
        ProjectileType type = ProjectileType.SHELL;
        if (projectileName.endsWith(".json")) {
            type = ProjectileType.WEAPON;
        } else if (projectileName.equalsIgnoreCase("rocket")) {
            type = ProjectileType.ROCKET;
        }

        CraftSoundSet projSoundSet = buildSoundSet(projSounds, "0");
        this.projectile = new CraftProjectile(type, projectileName, projSpeed.getDouble(),
                projDist.getInteger(), projDistPenalty.getInteger(), projDamage.getDouble(),
                projDamagePenalty.getDouble(), projSoundSet);

        this.recoil = new CraftRecoil(recoilMin.getDouble(), recoilMax.getDouble());

        CraftSoundSet reloadSoundSet = buildSoundSet(reloadSounds, reloadTime.asString());
        this.reload = new CraftReload(reloadTime.asString(), reloadSoundSet);

        this.automatic = automatic.getBoolean();
        this.cadence = CraftComponent.parseTime(cadence.asString());
    }
}
