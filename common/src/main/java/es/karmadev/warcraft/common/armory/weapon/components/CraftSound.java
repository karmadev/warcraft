package es.karmadev.warcraft.common.armory.weapon.components;

import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.reload.WeaponReload;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSound;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;

public class CraftSound implements WeaponSound {

    private CraftSoundSet soundSet;

    private final String time, sound;
    private final float pitch, volume;

    private long timeExact;

    public CraftSound(final String time, final String sound,
                      final float pitch, final float volume) {
        this.time = time;
        this.sound = sound;
        this.pitch = pitch;
        this.volume = volume;
    }

    public CraftSound(final long time, final String sound,
                      final float pitch, final float volume) {
        this.time = String.valueOf(time);
        this.sound = sound;
        this.pitch = pitch;
        this.volume = volume;
        this.timeExact = time;
    }

    CraftSound atSet(final CraftSoundSet set) {
        this.soundSet = set;

        resolveTimeExact();
        return this;
    }

    /**
     * Get the soundset this sound
     * pertains to
     *
     * @return the sound soundset
     */
    @Override
    public WeaponSoundSet getSoundSet() {
        return this.soundSet;
    }

    /**
     * Get when the sound should be
     * played
     *
     * @return the sound time
     */
    @Override
    public String getTime() {
        return this.time;
    }

    /**
     * Get when the sound should be
     * played, relative to the sound
     * specific time
     *
     * @return the exact time when the sound
     * should be played
     */
    @Override
    public long getTimeExact() {
        return this.timeExact;
    }

    /**
     * Get the sound name to play
     *
     * @return the sound name
     */
    @Override
    public String getSoundName() {
        return this.sound;
    }

    /**
     * Get the sound pitch
     *
     * @return the sound pitch
     */
    @Override
    public float getPitch() {
        return this.pitch;
    }

    /**
     * Get the sound volume
     *
     * @return the sound volume
     */
    @Override
    public float getVolume() {
        return this.volume;
    }

    private void resolveTimeExact() {
        if (this.soundSet == null) return;
        if (this.time.matches("^([0-9]{1,2}|100)%$")) {
            int rawPercentage = Integer.parseInt(this.time.replace("%", ""));

            WeaponComponent parent = this.soundSet.getComponent();
            switch (parent.getComponentType()) {
                case RELOAD:
                    WeaponReload reload = (WeaponReload) parent;
                    double time = reload.getTime();
                    double timeExact = (rawPercentage * time) / 100;

                    this.timeExact = (long) timeExact * 1000;
                    break;
                case SOUND:
                    WeaponSound sound = (WeaponSound) parent;
                    long otherSoundExact = sound.getTimeExact();

                    this.timeExact = (rawPercentage * otherSoundExact) / 100;
                    break;
                case MAGAZINE:
                case PROJECTILE:
                case MODEL:
                case RECOIL:
                default:
                    this.timeExact = 0;
                    break;
            }
        } else if (this.time.contains(".") || this.time.contains(",")) {
            double time = Double.parseDouble(this.time);
            this.timeExact = (long) time * 1000;
        } else {
            this.timeExact = Long.parseLong(this.time);
        }
    }
}
