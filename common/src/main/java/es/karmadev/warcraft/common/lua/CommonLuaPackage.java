package es.karmadev.warcraft.common.lua;

public class CommonLuaPackage extends es.karmadev.warcraft.api.script.LuaPackage {

    /**
     * Get if the package allows
     * lua API access
     *
     * @return if the package allows
     * lua access
     */
    @Override
    public boolean isAllowed() {
        return true;
    }
}
