package es.karmadev.warcraft.common.armory.weapon.components.recoil;

import es.karmadev.warcraft.api.armory.weapon.components.recoil.WeaponRecoil;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;

public class CraftRecoil extends CraftComponent implements WeaponRecoil {

    private final double min, max;

    public CraftRecoil(final double min, final double max) {
        this.min = min;
        this.max = max;
    }

    /**
     * Get the weapon minimum recoil
     *
     * @return the min recoil
     */
    @Override
    public double getMin() {
        return this.min;
    }

    /**
     * Get the weapon max recoil
     *
     * @return the max recoil
     */
    @Override
    public double getMax() {
        return this.max;
    }
}
