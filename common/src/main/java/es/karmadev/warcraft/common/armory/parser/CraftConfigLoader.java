package es.karmadev.warcraft.common.armory.parser;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.warcraft.api.WarCraft;
import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.type.WeaponType;
import es.karmadev.warcraft.api.exception.armory.InvalidArmoryException;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.CraftArmory;
import es.karmadev.warcraft.common.armory.weapon.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static es.karmadev.warcraft.common.armory.parser.CraftArmoryLoader.*;

public class CraftConfigLoader implements ArmoryLoader {

    private final Map<WeaponType, Set<Weapon>> weapons = new LinkedHashMap<>();
    private final JsonObject config;

    public CraftConfigLoader(final JsonObject config) {
        this.config = config;
    }

    @Override
    public void mapTo(final CraftArmory armory) throws InvalidArmoryException, InvalidWeaponException {
        String configMissing = findMissingProperty(this.config, JsonNative.class, "path");
        if (configMissing == null)
            configMissing = findMissingProperty(this.config, JsonArray.class, "structure");

        if (configMissing != null)
            throw new InvalidArmoryException("json",
                    "Missing property " + configMissing + " from armory " + armory.getName() + " configuration");

        JsonNative path = this.config.asNative("path");
        JsonArray structure = this.config.asArray("structure");

        Map<WeaponType, String> properties = new EnumMap<>(WeaponType.class);
        for (JsonInstance element : structure) {
            if (!element.isObjectType()) continue;
            JsonObject elObject = element.asObject();

            String objectMissing = findMissingProperty(elObject, JsonNative.class, "dir", "type");
            if (objectMissing != null) continue;

            String directory = elObject.asString("dir");
            String rawType = elObject.asString("type");
            assert rawType != null;

            try {
                WeaponType type = WeaponType.valueOf(rawType.toUpperCase());
                properties.put(type, directory);
            } catch (IllegalArgumentException ignored) {}
        }

        for (WeaponType type : WeaponType.values()) {
            String value = properties.get(type);
            if (value == null)
                throw new InvalidArmoryException("json",
                        "Missing specification for " + type.name().toLowerCase() + " on armory " + armory.getName() + " configuration");
        }

        String dir = path.getString();
        if (dir.equals("default"))
            dir = armory.getUUNS().toString().replaceFirst("=", "/");

        Path initial = WarCraft.getInstance().getWeaponsDirectory()
                .resolve(dir);

        mapWeapons(initial, properties, armory);
        if (this.weapons.isEmpty()) return;

        armory.populate(this.weapons);
    }

    private void mapWeapons(final Path path, final Map<WeaponType, String> map, final CraftArmory armory) throws InvalidWeaponException,
            InvalidArmoryException {
        Map<String, Weapon> basedWeapons = new HashMap<>();
        WarCraft plugin = WarCraft.getInstance();

        for (WeaponType type : map.keySet()) {
            String dir = map.get(type);
            Path container = path.resolve(dir);

            if (!Files.exists(container)) continue;
            try (Stream<Path> files = Files.list(container)
                    .filter((f) -> !Files.isDirectory(f) && f.getFileName()
                            .toString().endsWith(".json"))) {

                Iterator<Path> file = files.iterator();
                while (file.hasNext()) {
                    Path element = file.next();
                    parseWeaponFile(armory, type, element, plugin, basedWeapons);
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private void parseWeaponFile(CraftArmory armory, WeaponType type, Path element, WarCraft plugin, Map<String, Weapon> basedWeapons) throws IOException, InvalidWeaponException, InvalidArmoryException {
        try(Reader reader = Files.newBufferedReader(element)) {
            JsonInstance instance = JsonReader.read(reader);
            if (instance == null || !instance.isObjectType()) return;

            JsonObject object = instance.asObject();
            String name = "";
            if (!type.equals(WeaponType.THROWABLE)) {
                name = getWeaponName(armory, object, element);
            }

            switch (type) {
                case GUN:
                    this.parseGunWeapon(armory, type, name, object, plugin, basedWeapons, element);
                    break;
                case EXPLOSIVE:
                    this.parseExplosiveWeapon(armory, type, name, object, plugin, basedWeapons, element);
                    break;
                case MELEE:
                    this.parseMeleeWeapon(armory, type, name, object, plugin, basedWeapons, element);
                    break;
                case THROWABLE:
                    this.parseThrowableWeapon(type, basedWeapons, element, object, plugin);
                    break;
                case CONSTRUCTION:
                    this.parseConstructionWeapon(armory, type, element, name, object);
                    break;
            }
        }
    }

    private static @NotNull String getWeaponName(CraftArmory armory, JsonObject object, Path element) throws InvalidWeaponException, InvalidArmoryException {
        String name;
        String missing = findMissingProperty(object, JsonNative.class, "name");
        if (missing != null)
            throw new InvalidWeaponException(armory, "<unknown>", "Property \"name\" is missing from weapon file " + element);

        JsonNative nameNative = object.asNative("name");
        ensureString("Property \"{}\" is required to be a string at weapon " + element, nameNative);

        name = nameNative.getString();
        return name;
    }

    private void parseConstructionWeapon(CraftArmory armory, WeaponType type, Path element, String name, JsonObject object) throws IOException, InvalidWeaponException {
        String fileName = element.getFileName().toString();
        String pattern = fileName.replace(".json", "_[0-9]+\\.json$");

        Path levels = element.getParent().resolve("levels");
        if (!Files.exists(levels)) return;

        parseBuildings(levels, pattern, armory, name, object, type);
    }

    private void parseThrowableWeapon(WeaponType type, Map<String, Weapon> basedWeapons, Path element, JsonObject object, WarCraft plugin) throws InvalidWeaponException {
        Weapon defined = basedWeapons.get(element.getFileName().toString());
        if (defined == null) return;

        CraftThrowable throwable = new CraftThrowable(defined, object);
        if (plugin.getArmoryManager().getWeapons().contains(throwable)) return;

        Set<Weapon> throwableWeapons = this.weapons.computeIfAbsent(type, (s) -> new LinkedHashSet<>());
        throwableWeapons.add(throwable);
    }

    private void parseMeleeWeapon(CraftArmory armory, WeaponType type, String name, JsonObject object, WarCraft plugin, Map<String, Weapon> basedWeapons, Path element) throws InvalidWeaponException {
        CraftMelee melee = new CraftMelee(armory, name, object);
        if (plugin.getArmoryManager().getWeapons().contains(melee)) return;

        Set<Weapon> meleeWeapons = this.weapons.computeIfAbsent(type, (s) -> new LinkedHashSet<>());
        meleeWeapons.add(melee);
        basedWeapons.put(element.getFileName().toString(), melee);
    }

    private void parseExplosiveWeapon(CraftArmory armory, WeaponType type, String name, JsonObject object, WarCraft plugin, Map<String, Weapon> basedWeapons, Path element) throws InvalidWeaponException {
        CraftExplosive explosive = new CraftExplosive(armory, name, object);
        if (plugin.getArmoryManager().getWeapons().contains(explosive)) return;

        Set<Weapon> expWeapons = this.weapons.computeIfAbsent(type, (s) -> new LinkedHashSet<>());
        expWeapons.add(explosive);
        basedWeapons.put(element.getFileName().toString(), explosive);
    }

    private void parseGunWeapon(CraftArmory armory, WeaponType type, String name, JsonObject object, WarCraft plugin, Map<String, Weapon> basedWeapons, Path element) throws InvalidWeaponException {
        CraftGun gun = new CraftGun(armory, name, object);
        if (plugin.getArmoryManager().getWeapons().contains(gun)) return;

        Set<Weapon> gunWeapons = this.weapons.computeIfAbsent(type, (s) -> new LinkedHashSet<>());
        gunWeapons.add(gun);
        basedWeapons.put(element.getFileName().toString(), gun);
    }

    private void parseBuildings(final Path levels, final String pattern, final CraftArmory armory, final String name,
                                       final JsonObject object, final WeaponType type) throws IOException, InvalidWeaponException {
        WarCraft plugin = WarCraft.getInstance();

        try (Stream<Path> levelsStream = Files.list(levels)
                .filter((fl) -> fl.getFileName().toString().matches(pattern))) {

            List<JsonObject> levelObjects = new ArrayList<>();
            Iterator<Path> levelIterator = levelsStream.iterator();
            while (levelIterator.hasNext()) {
                Path element = levelIterator.next();
                try(Reader levelReader = Files.newBufferedReader(element)) {
                    JsonInstance levelInstance = JsonReader.read(levelReader);
                    if (levelInstance == null || !levelInstance.isObjectType()) continue;

                    JsonObject levelObject = levelInstance.asObject();
                    levelObjects.add(levelObject);
                }
            }

            CraftConstruction construction = new CraftConstruction(armory, name, object, levelObjects);
            if (plugin.getArmoryManager().getWeapons().contains(construction)) return;

            Set<Weapon> constructionWeapons = this.weapons.computeIfAbsent(type, (s) -> new LinkedHashSet<>());
            constructionWeapons.add(construction);
        }
    }
}
