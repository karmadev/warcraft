package es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser;

import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.DispenserRefiller;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;

public abstract class CraftRefiller extends CraftComponent implements DispenserRefiller {

    private final double amount;
    private final long interval;

    public CraftRefiller(final double amount, final String interval) {
        this.amount = amount;
        this.interval = parseTime(interval);
    }

    /**
     * Get the refill amount
     *
     * @return the amount
     */
    @Override
    public double getAmount() {
        return this.amount;
    }

    /**
     * Get the refill interval
     *
     * @return the refill interval
     */
    @Override
    public long getInterval() {
        return this.interval;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("%s{\"amount\": %.2f, \"interval\": %d}",
                this.getClass().getSimpleName(), this.amount, this.interval);
    }
}
