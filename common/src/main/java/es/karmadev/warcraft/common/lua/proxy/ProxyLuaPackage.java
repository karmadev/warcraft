package es.karmadev.warcraft.common.lua.proxy;

import es.karmadev.warcraft.api.script.LuaPackage;

public class ProxyLuaPackage extends LuaPackage {

    /**
     * Get if the package allows
     * lua API access
     *
     * @return if the package allows
     * lua access
     */
    @Override
    public boolean isAllowed() {
        return false;
    }
}
