package es.karmadev.warcraft.common.armory.weapon.components.magazine;

import es.karmadev.warcraft.api.armory.weapon.components.magazine.WeaponMagazine;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;
import org.jetbrains.annotations.Contract;

public class CraftMagazine extends CraftComponent implements WeaponMagazine {

    private final int chamber, capacity;
    private final WeaponSoundSet soundSet;

    @Contract("_, _, null -> fail")
    public CraftMagazine(final int chamber, final int capacity, final CraftSoundSet soundSet) {
        if (soundSet == null)
            throw new IllegalArgumentException("Cannot create magazine of invalid soundset");

        this.chamber = chamber;
        this.capacity = capacity;
        this.soundSet = soundSet.atComponent(this);
    }

    /**
     * Get the magazine chamber
     *
     * @return the magazine chamber
     */
    @Override
    public int getChamber() {
        return this.chamber;
    }

    /**
     * Get the weapon magazine capacity
     *
     * @return the magazine capacity
     */
    @Override
    public int getCapacity() {
        return this.capacity;
    }

    /**
     * Get the sounds to play when the
     * magazine is empty
     *
     * @return the empty sounds
     */
    @Override
    public WeaponSoundSet getEmptySounds() {
        return this.soundSet;
    }
}
