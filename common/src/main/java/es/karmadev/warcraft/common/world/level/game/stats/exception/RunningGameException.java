package es.karmadev.warcraft.common.world.level.game.stats.exception;

import es.karmadev.warcraft.api.world.level.game.Game;

/**
 * Running game exception, is thrown when
 * a {@link es.karmadev.warcraft.common.world.level.game.stats.CraftGameState} is
 * tried to be created for a game which
 * is still running
 */
public class RunningGameException extends RuntimeException {

    /**
     * Create the running game exception
     *
     * @param game the game
     */
    public RunningGameException(final Game game) {
        super("Cannot create game state from " + game.getName() + " because the game is still running!");
    }
}
