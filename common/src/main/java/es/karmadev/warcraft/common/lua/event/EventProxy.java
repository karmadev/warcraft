package es.karmadev.warcraft.common.lua.event;

import org.bukkit.event.Event;

public interface EventProxy<T extends Event> {

    void handle(final T event);
}
