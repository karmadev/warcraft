package es.karmadev.warcraft.common.armory.weapon.components.building.level;

import es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry.SentryBuildingWeapon;
import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;

public class CraftSentryWeapon extends CraftComponent implements SentryBuildingWeapon {

    private final ProjectileType type;
    private final String projectile;
    private final int amo;
    private final double damage;
    private final long cadence;
    private final long reloadTime;
    private final WeaponSoundSet shootSounds;
    private final WeaponSoundSet emptyShootSounds;
    private final WeaponSoundSet reloadSounds;
    private final int amount;

    public CraftSentryWeapon(final ProjectileType type, final String projectile, final int amo,
                             final double damage, final String cadence, final String reloadTime,
                             final CraftSoundSet shootSounds, final CraftSoundSet emptyShootSounds,
                             final CraftSoundSet reloadSounds, final int amount) {
        this.type = type;
        this.projectile = projectile;
        this.amo = amo;
        this.damage = damage;
        this.cadence = parseTime(cadence);
        this.reloadTime = parseTime(reloadTime);
        this.shootSounds = shootSounds
                .atComponent(this);
        this.emptyShootSounds = emptyShootSounds
                .atComponent(this);
        this.reloadSounds = reloadSounds
                .atComponent(this);
        this.amount = amount;
    }


    /**
     * Get the weapon projectile type
     *
     * @return the projectile type
     */
    @Override
    public ProjectileType getProjectileType() {
        return this.type;
    }

    /**
     * Get the weapon projectile
     *
     * @return the weapon projectile
     */
    @Override
    public String getProjectileName() {
        return this.projectile;
    }

    /**
     * Get the gun amo
     *
     * @return the amo
     */
    @Override
    public int getAmo() {
        return this.amo;
    }

    /**
     * Get the projectile damage
     *
     * @return the damage
     */
    @Override
    public double getDamage() {
        return this.damage;
    }

    /**
     * Get the projectile cadence
     *
     * @return the cadence
     */
    @Override
    public long getCadence() {
        return this.cadence;
    }

    /**
     * Get the gun reload time
     *
     * @return the reload time
     */
    @Override
    public long getReloadTime() {
        return this.reloadTime;
    }

    /**
     * Get the weapon shoot sounds
     *
     * @return the shoot sounds
     */
    @Override
    public WeaponSoundSet getShootSounds() {
        return this.shootSounds;
    }

    /**
     * Get the weapon empty shoot
     * sounds
     *
     * @return the empty barrel sounds
     */
    @Override
    public WeaponSoundSet getEmptySounds() {
        return this.emptyShootSounds;
    }

    /**
     * Get the weapon reload sounds
     *
     * @return the reload sounds
     */
    @Override
    public WeaponSoundSet getReloadSounds() {
        return this.reloadSounds;
    }

    /**
     * Get the amount of times
     * this weapon is hold in the
     * sentry
     *
     * @return the amount of weapons of
     * this kind the sentry has
     */
    @Override
    public int getAmount() {
        return this.amount;
    }

    @Override
    public String toString() {
        return String.format("CraftSentryWeapon{\"projectile type\": %s, \"projectile\": \"%s\", \"amo\": %d, " +
                "\"damage\": %.2f, \"cadence\": %d, \"reload time\": %d, \"shoot sounds\": %d, \"empty sounds\": %d, " +
                "\"reload sounds\": %d, \"amount\": %d}",
                this.type, this.projectile, this.amo, this.damage, this.cadence,
                this.reloadTime, this.shootSounds.size(), this.emptyShootSounds.size(),
                this.reloadSounds.size(), this.amount);
    }
}
