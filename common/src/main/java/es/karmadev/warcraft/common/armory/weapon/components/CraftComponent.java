package es.karmadev.warcraft.common.armory.weapon.components;

public abstract class CraftComponent {

    public static long parseTime(String rawTime) {
        if (rawTime.trim().isEmpty())
            return 0L;

        int multiplier = 1;
        if (rawTime.startsWith("-")) {
            multiplier = -1;
            rawTime = rawTime.substring(1);
        }

        if (!rawTime.matches("^[0-9]+$")) {
            if (rawTime.matches("^[0-9]+(\\.[0-9]+)?[smx]$")) {
                char letter = rawTime.charAt(rawTime.length() - 1);
                double number = Double.parseDouble(rawTime.substring(0, rawTime.length() - 1));

                switch (letter) {
                    case 'x':
                        if (rawTime.contains(".")) {
                            return Math.max(0, (long) (number * 1000));
                        }

                        return Math.max(0, (long) number);
                    case 'm':
                        return Math.max(0, (long) (number * 60) * 1000);
                    case 's':
                    default:
                        return Math.max(0, (long) (number * 1000));
                }
            } else if (rawTime.matches("^[0-9]+(\\.[0-9]+)?$")) {
                double number = Double.parseDouble(rawTime);
                return Math.max(0, (long) (number * 1000));
            } else {
                throw new IllegalStateException("Cannot create explosion because fuse time is malformed: " + rawTime);
            }
        } else {
            return Math.max(-1, Long.parseLong(rawTime) * multiplier);
        }
    }
}
