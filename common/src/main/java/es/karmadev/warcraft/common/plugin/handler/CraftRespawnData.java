package es.karmadev.warcraft.common.plugin.handler;

import es.karmadev.warcraft.api.world.level.game.respawn.RespawnData;
import es.karmadev.warcraft.api.world.level.player.Client;

public class CraftRespawnData implements RespawnData {

    private final Client client;
    private final long start = System.currentTimeMillis();
    private long end;

    public CraftRespawnData(final Client client) {
        this(client, INFINITE);
    }

    public CraftRespawnData(final Client client, final long end) {
        this.client = client;
        this.end = end;
    }

    /**
     * Get the respawn data client
     *
     * @return the data client
     */
    @Override
    public Client getClient() {
        return this.client;
    }

    /**
     * Get when the respawn timer
     * was set on the client
     *
     * @return the respawn timer start
     */
    @Override
    public long getRespawnStart() {
        return this.start;
    }

    /**
     * Get when the client will respawn
     *
     * @return the respawn timer end
     */
    @Override
    public long getRespawnEnd() {
        return this.end;
    }

    /**
     * Tries to set the client
     * respawn end. The operation might
     * fail in the following cases:
     * <ul>
     *     <li>If the respawn end time is before the respawn start time</li>
     *     <li>If the respawn end time is the same as the current one</li>
     *     <li>If the respawn end time is marked to {@link RespawnData#INFINITE}</li>
     * </ul>
     *
     * @param newTime the new end time
     * @return if the operation was success
     */
    @Override
    public boolean setRespawnEnd(final long newTime) {
        if (this.end == INFINITE) return false;
        if (newTime == this.end) return false;
        if (newTime < this.start) return false;

        this.end = newTime;
        return true;
    }
}
