package es.karmadev.warcraft.common.armory.weapon.components.building;

import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.DispenserBuildingLevel;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingAmo;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingHealing;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingMetal;

public class CraftDispenserLevel extends CraftLevel implements DispenserBuildingLevel {

    private final DispenserBuildingHealing heal;
    private final DispenserBuildingAmo amo;
    private final DispenserBuildingMetal metal;
    private final int maxMetal;

    public CraftDispenserLevel(final int level, final double radius,
                               final double health,
                               final DispenserBuildingHealing heal,
                               final DispenserBuildingAmo amo,
                               final DispenserBuildingMetal metal,
                               final int maxMetal) {
        super(level, radius, health);
        this.heal = heal;
        this.amo = amo;
        this.metal = metal;
        this.maxMetal = maxMetal;
    }

    /**
     * Get the dispenser healer
     *
     * @return the healer
     */
    @Override
    public DispenserBuildingHealing getHealRefill() {
        return this.heal;
    }

    /**
     * Get the dispenser amo re-filler
     *
     * @return the amo re-filler
     */
    @Override
    public DispenserBuildingAmo getAmoRefill() {
        return this.amo;
    }

    /**
     * Get the dispenser metal re-filler
     *
     * @return the metal re-filler
     */
    @Override
    public DispenserBuildingMetal getMetalRefill() {
        return this.metal;
    }

    /**
     * Get the dispenser metal capacity
     *
     * @return the metal capacity
     */
    @Override
    public int getMaxMetal() {
        return this.maxMetal;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("CraftDispenserLevel{\"level\": %d, \"radius\": %.2f, \"healer\": %s, \"amo\": %s, \"metal\": %s, " +
                "\"max metal\": %d}", this.getLevel(), this.getRadius(), this.heal, this.amo, this.metal,
                this.maxMetal);
    }
}
