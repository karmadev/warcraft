package es.karmadev.warcraft.common.plugin.handler;

import es.karmadev.warcraft.api.world.level.game.respawn.RespawnData;
import es.karmadev.warcraft.api.world.level.game.respawn.RespawnHandler;
import es.karmadev.warcraft.api.world.level.player.Client;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Respawn handler for games
 */
public final class CraftSpawnHandler implements RespawnHandler {

    private final Map<Client, RespawnData> dataMap = new HashMap<>();
    private boolean instantRespawn = false;

    /**
     * Add the client to the respawn
     * que
     *
     * @param client      the client to add
     * @param respawnTime the respawn time
     */
    @Override
    public void addToSpawn(final Client client, final int respawnTime) {
        if (respawnTime == RespawnData.INFINITE) {
            this.dataMap.put(client, new CraftRespawnData(client));
            //Infinite respawn time overwrites any defined respawn time
            return;
        }

        long now = System.currentTimeMillis();
        long end = now + (respawnTime * 1000L);

        RespawnData data = this.dataMap.remove(client);
        if (data != null) {
            if (data.getRemainingTime() > 0) {
                if (data.setRespawnEnd(end))
                    return; //Return only if success
            }
        }

        data = new CraftRespawnData(client, end);
        this.dataMap.put(client, data);
    }

    /**
     * Removes a client from the
     * respawn queue
     *
     * @param client the client to remove
     */
    @Override
    public void removeFromSpawn(final Client client) {
        this.dataMap.remove(client);
    }

    /**
     * Get if the client is currently in
     * the respawn handler que
     *
     * @param client the client
     * @return if the client is in the queue
     */
    @Override
    public boolean isInQueue(final Client client) {
        RespawnData data = this.dataMap.get(client);
        int seconds = data.getRemainingTime();
        if (seconds == RespawnData.INFINITE)
            return true;

        return seconds > 0;
    }

    /**
     * Get a client respawn data
     *
     * @param client the client
     * @return the client respawn data
     */
    @Nullable
    @Override
    public RespawnData getRespawnData(final Client client) {
        return this.dataMap.get(client);
    }

    /**
     * Get if the game supports instant
     * respawn times
     *
     * @return if the game respawns the
     * clients instantly
     */
    @Override
    public boolean isInstantRespawn() {
        return this.instantRespawn;
    }

    /**
     * Set if the clients respawn
     * instantly
     *
     * @param status whether the clients should
     *               respawn instantly
     */
    @Override
    public void setInstantRespawn(final boolean status) {
        this.instantRespawn = status;
    }

    /**
     * Get the respawn queue
     *
     * @return the queue
     */
    @Override
    public Collection<Client> getQueue() {
        return this.dataMap.keySet().stream()
                .filter(this::isInQueue)
                .collect(Collectors.toList());
    }
}