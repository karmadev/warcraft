package es.karmadev.warcraft.common.armory.parser;

import es.karmadev.warcraft.api.exception.armory.InvalidArmoryException;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.CraftArmory;

public interface ArmoryLoader {

    void mapTo(final CraftArmory armory) throws InvalidArmoryException,
            InvalidWeaponException;
}
