package es.karmadev.warcraft.common.armory.weapon.components.reload;

import es.karmadev.warcraft.api.armory.weapon.components.reload.WeaponReload;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;
import org.jetbrains.annotations.Contract;

public class CraftReload extends CraftComponent implements WeaponReload {

    private final long time;
    private final WeaponSoundSet soundSet;

    @Contract("_, null -> fail")
    public CraftReload(final String time, final CraftSoundSet soundSet) {
        if (soundSet == null)
            throw new IllegalArgumentException("Cannot create reload of invalid soundset");

        this.time = parseTime(time);
        this.soundSet = soundSet.atComponent(this);
    }


    /**
     * Get the time to take for
     * each reload
     *
     * @return the time for reload
     */
    @Override
    public long getTime() {
        return time;
    }

    /**
     * Get the weapon reload sounds
     *
     * @return the reload sounds
     */
    @Override
    public WeaponSoundSet getSounds() {
        return this.soundSet;
    }
}
