package es.karmadev.warcraft.common.world.level.game.stats;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.world.level.game.stats.GameState;
import es.karmadev.warcraft.api.world.level.game.stats.GameStats;

public final class CraftGameStats implements GameStats {



    /**
     * Get the client most used weapon. The
     * most used weapon is counted as the most
     * shoot one
     *
     * @return the most used client weapon
     */
    @Override
    public Weapon getFavouriteWeapon() {
        return null;
    }

    /**
     * Get the client favourite game
     *
     * @return the favourite game
     */
    @Override
    public GameState getFavouriteGame() {
        return null;
    }

    /**
     * Get the client total damage
     *
     * @return the client total
     * damage
     */
    @Override
    public double getTotalDamage() {
        return 0;
    }

    /**
     * Get the amount of times
     * the client killed someone
     *
     * @return the amount of client kills
     */
    @Override
    public int getKills() {
        return 0;
    }

    /**
     * Get the amount of times
     * the client died by someone
     *
     * @return the amount of client deaths
     */
    @Override
    public int getDeaths() {
        return 0;
    }

    /**
     * Get the client base precision,
     * based on missed shoots
     *
     * @return the client base precision
     */
    @Override
    public float getPrecission() {
        return 0;
    }
}
