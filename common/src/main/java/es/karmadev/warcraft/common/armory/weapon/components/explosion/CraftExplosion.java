package es.karmadev.warcraft.common.armory.weapon.components.explosion;

import es.karmadev.warcraft.api.armory.weapon.components.explosion.WeaponExplosion;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;

public class CraftExplosion extends CraftComponent implements WeaponExplosion {

    private final long fuseTime;
    private final int distance, distancePenalty;
    private final double damage, damagePenalty;
    private final WeaponSoundSet sounds;

    public CraftExplosion(final long fuseTime, final int distance, final int distancePenalty,
                          final double damage, final double damagePenalty, final CraftSoundSet sounds) {
        this.fuseTime = fuseTime;
        this.distance = distance;
        this.distancePenalty = distancePenalty;
        this.damage = damage;
        this.damagePenalty = damagePenalty;
        this.sounds = sounds.atComponent(this);
    }

    public CraftExplosion(final String fuseTime, final int distance, final int distancePenalty,
                          final double damage, final double damagePenalty, final CraftSoundSet sounds) {
        this(parseTime(fuseTime), distance, distancePenalty, damage, damagePenalty, sounds);
    }

    /**
     * Get the explosive fuse time.
     * A value of zero stands for
     * no fuse
     *
     * @return the explosive fuse time
     */
    @Override
    public long getFuseTime() {
        return this.fuseTime;
    }

    /**
     * Get the explosive explosion
     * effective distance
     *
     * @return the explosion distance
     */
    @Override
    public int getDistance() {
        return this.distance;
    }

    /**
     * Get the explosion radius distance
     * penalty. Each this amount of blocks,
     * the damage will be reduced by the
     * value of {@link #getDamagePenalty()}
     *
     * @return the distance penalty
     */
    @Override
    public int getDistancePenalty() {
        return this.distancePenalty;
    }

    /**
     * Get the base explosion damage
     *
     * @return the base damage
     */
    @Override
    public double getDamage() {
        return this.damage;
    }

    /**
     * Get the explosion damage
     * penalty
     *
     * @return the damage penalty
     */
    @Override
    public double getDamagePenalty() {
        return this.damagePenalty;
    }

    /**
     * Get the explosion sounds
     *
     * @return the explosion sounds
     */
    @Override
    public WeaponSoundSet getSounds() {
        return this.sounds;
    }
}
