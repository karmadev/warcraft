package es.karmadev.warcraft.common.world.level.game.stats;

import es.karmadev.api.kson.processor.JsonSerializable;
import es.karmadev.api.kson.processor.construct.JsonConstructor;
import es.karmadev.api.kson.processor.construct.JsonParameter;
import es.karmadev.api.kson.processor.conversor.InstantTransformer;
import es.karmadev.api.kson.processor.field.JsonElement;
import es.karmadev.api.kson.processor.field.Transformer;
import es.karmadev.warcraft.api.WarCraft;
import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import es.karmadev.warcraft.api.util.uuns.UUNSJsonTransformer;
import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.api.world.level.game.Game;
import es.karmadev.warcraft.api.world.level.game.GameType;
import es.karmadev.warcraft.api.world.level.game.stats.GameState;
import es.karmadev.warcraft.api.world.level.player.Client;
import es.karmadev.warcraft.common.world.level.TeamJsonTransformer;
import es.karmadev.warcraft.common.world.level.game.stats.exception.RunningGameException;

import java.time.Instant;

@JsonSerializable
public final class CraftGameState implements GameState {

    @JsonElement
    @Transformer(transformer = UUNSJsonTransformer.class)
    private final UUNS uuns;

    @JsonElement
    private final String name;

    @JsonElement(name = "game_type") //Since KSon 1.0.7 JsonElement supports enum transformations natively
    private final GameType type;

    @JsonElement
    @Transformer(transformer = InstantTransformer.class)
    private final Instant date;

    @JsonElement
    private final int players;

    @JsonElement(name = "damage")
    private final double totalDamage;

    @JsonElement(name = "precision")
    private final float precision;

    @JsonElement(name = "position")
    private final int position;

    @JsonElement(name = "team_position")
    private final int teamPosition;

    @JsonElement(name = "team")
    @Transformer(transformer = TeamJsonTransformer.class)
    private final Team team;

    @JsonElement(name = "weapon")
    @Transformer(transformer = UUNSJsonTransformer.class)
    private final UUNS weapon;

    private CraftGameState(final UUNS uuns,
                           final String name,
                           final GameType type,
                           final int players,
                           final double damage,
                           final float precision,
                           final int position,
                           final int teamPosition,
                           final Team team,
                           final Weapon weapon) {
        this(uuns, name, type, Instant.now(), players,
                damage, precision, position, teamPosition,
                team, weapon);
    }

    @JsonConstructor
    private CraftGameState(final @JsonParameter(readFrom = "uuns") UUNS uuns,
                           final @JsonParameter(readFrom = "name") String name,
                           final @JsonParameter(readFrom = "game_type") GameType type,
                           final @JsonParameter(readFrom = "date") Instant date,
                           final @JsonParameter(readFrom = "players") int players,
                           final @JsonParameter(readFrom = "damage") double damage,
                           final @JsonParameter(readFrom = "precision") float precision,
                           final @JsonParameter(readFrom = "position") int position,
                           final @JsonParameter(readFrom = "team_position") int teamPosition,
                           final @JsonParameter(readFrom = "team") Team team,
                           final @JsonParameter(readFrom = "weapon") Weapon weapon) {
        this.uuns = uuns;
        this.name = name;
        this.type = type;
        this.date = date;
        this.players = players;
        this.totalDamage = damage;
        this.precision = precision;
        this.position = position;
        this.teamPosition = teamPosition;
        this.team = team;
        this.weapon = weapon.getUUNS();
    }

    /**
     * Get the game UUNS
     *
     * @return the game UUNS
     */
    @Override
    public UUNS getUUNS() {
        return this.uuns;
    }

    /**
     * Get the game name
     *
     * @return the game name
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Get the game type
     *
     * @return the game type
     */
    @Override
    public GameType getType() {
        return this.type;
    }

    /**
     * Get the game state date
     *
     * @return the game state date
     */
    @Override
    public Instant getDate() {
        return this.date;
    }

    /**
     * Get the amount of players which
     * played on this game
     *
     * @return the amount of players in
     * this game
     */
    @Override
    public int getPlayers() {
        return this.players;
    }

    /**
     * Get the client total damage
     * in this game
     *
     * @return the client total damage
     */
    @Override
    public double getTotalDamage() {
        return this.totalDamage;
    }

    /**
     * Get the client precision during
     * this game
     *
     * @return the client precision
     */
    @Override
    public float getPrecision() {
        return this.precision;
    }

    /**
     * Get the client position in
     * the game, a position of 1 means
     * he was the MVP
     *
     * @return the client position
     */
    @Override
    public int getScorePosition() {
        return this.position;
    }

    /**
     * Get the client position in
     * his team, a position of 1 means
     * he was the MVP
     *
     * @return the client position
     */
    @Override
    public int getTeamPosition() {
        return this.teamPosition;
    }

    /**
     * Get the client team on this
     * game
     *
     * @return the client team
     */
    @Override
    public Team getTeam() {
        return this.team;
    }

    /**
     * Get the favourite weapon of
     * the client in this game
     *
     * @return the client's favourite
     * weapon
     */
    @Override
    public Weapon getFavouriteWeapon() {
        return WarCraft.getInstance()
                .getArmoryManager()
                .getWeapons()
                .get(this.weapon);
    }

    public static CraftGameState fromGame(final Game game, final Client client, final Weapon weapon) throws RunningGameException {
        if (!game.hasFinished())
            throw new RunningGameException(game);


        return null; //TODO: Create
    }
}
