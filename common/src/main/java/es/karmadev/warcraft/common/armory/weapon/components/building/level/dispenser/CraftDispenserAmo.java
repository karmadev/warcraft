package es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser;

import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingAmo;

public class CraftDispenserAmo extends CraftRefiller implements DispenserBuildingAmo {

    public CraftDispenserAmo(final int amount, final String interval) {
        super(amount, interval);
    }
}
