package es.karmadev.warcraft.common.armory;

import es.karmadev.warcraft.api.armory.ArmoryAuthor;

public class CraftArmoryAuthor implements ArmoryAuthor {

    private final static String BLANK_NAME = "<blank name>";
    private final static String BLANK_WEB = "<blank website>";
    private final static String BLANK_CONTACT = "<blank contact>";

    private final String name;
    private final String website;
    private final String contact;

    public CraftArmoryAuthor(final String name, final String website, final String contact) {
        this.name = name;
        this.website = website;
        this.contact = contact;
    }

    /**
     * Get the author name
     *
     * @return the author name
     */
    @Override
    public String getName() {
        return (this.name == null || this.name.trim().isEmpty() ? BLANK_NAME : this.name);
    }

    /**
     * Get the author website
     *
     * @return the author website
     */
    @Override
    public String getWebsite() {
        return (this.website == null || this.website.trim().isEmpty() ? BLANK_WEB : this.website);
    }

    /**
     * Get the author contact information
     *
     * @return the contact information
     */
    @Override
    public String getContact() {
        return (this.contact == null || this.contact.trim().isEmpty() ? BLANK_CONTACT : this.contact);
    }

    @Override
    public String toString() {
        return String.format("CraftArmoryAuthor{\"name\": \"%s\", \"website\": \"%s\", \"contact\": \"%s\"}",
                this.getName(), this.getWebsite(), this.getContact());
    }
}
