package es.karmadev.warcraft.common.armory;

import es.karmadev.api.kson.*;
import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.ArmoryAuthor;
import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.type.WeaponType;
import es.karmadev.warcraft.api.exception.armory.InvalidArmoryException;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import es.karmadev.warcraft.common.armory.parser.CraftArmoryLoader;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Warcraft armory
 */
public class CraftArmory implements Armory {

    private final UUNS uuns;
    private final String name;
    private final String version;
    private final String description;
    private final int apiVersion;
    private final ArmoryAuthor author;

    protected final Map<WeaponType, Set<Weapon>> weapons = new EnumMap<>(WeaponType.class);
    protected final Collection<Weapon> weaponCollection = new ArrayList<>();

    /**
     * Create a new armory
     *
     * @param uuns the armory UUNS
     * @param name the armory name
     * @throws IllegalArgumentException if the armory uuns or name are empty or null
     */
    @Contract("null, null, _, _, _, _ -> fail")
    public CraftArmory(final UUNS uuns, final String name, final String version,
                       final String description, final int apiVersion,
                       final ArmoryAuthor author) throws IllegalArgumentException {
        if (uuns == null)
            throw new IllegalArgumentException("Cannot create armory with an invalid uuns");
        if (name == null)
            throw new IllegalArgumentException("Cannot create armory with an invalid name");

        this.uuns = uuns;
        this.name = name;
        this.version = version;
        this.apiVersion = apiVersion;
        this.author = author;

        this.description = description
                .replace("@uuns", uuns.toString())
                .replace("@name", name)
                .replace("@version", (version == null || version.trim().isEmpty() ? "<blank version>" : version))
                .replace("@api_version", String.valueOf(apiVersion))
                .replace("@author", (author == null ? "<blank author>" : author.getName()));
    }

    /**
     * Tries to populate the armory with
     * weapons
     *
     * @param data the data to populate with
     * @throws IllegalStateException if the armory is already populated
     */
    public void populate(final Map<WeaponType, Set<Weapon>> data) throws IllegalStateException {
        if (!this.weaponCollection.isEmpty())
            throw new IllegalStateException("Cannot populate " + this.name + " because it is already populated");

        this.weapons.putAll(data);
        this.weaponCollection.addAll(data.values().stream()
                .flatMap(Set::stream).collect(Collectors.toSet()));
    }

    /**
     * Get the armory UUNS
     *
     * @return the armory UUNS
     */
    @Override
    public UUNS getUUNS() {
        return this.uuns;
    }

    /**
     * Get the armory name
     *
     * @return the armory name
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Get the armory description
     *
     * @return the armory description
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the armory version
     *
     * @return the version
     */
    @Override
    public String getVersion() {
        return this.version;
    }

    /**
     * Get the armory api version
     *
     * @return the API version of
     * the armory package
     */
    @Override
    public int getApiVersion() {
        return this.apiVersion;
    }

    /**
     * Get the armory author
     *
     * @return the author
     */
    @Override
    public ArmoryAuthor getAuthor() {
        return this.author;
    }

    /**
     * Get the amount of weapons
     *
     * @return the total amount of weapons
     */
    @Override
    public int size() {
        return weaponCollection.size();
    }

    /**
     * Get the amount of weapons for the
     * specified type
     *
     * @param type the weapon type
     * @return the type weapons amount
     */
    @Override
    public int size(final WeaponType type) {
        return this.weapons.getOrDefault(type, Collections.emptySet())
                .size();
    }

    /**
     * Get if the armory has a weapon
     * of the specified type
     *
     * @param type   the weapon type
     * @param weapon the weapon
     * @return if the armory contains the weapon
     */
    @Override
    public boolean hasWeapon(final WeaponType type, final Weapon weapon) {
        if (!this.weapons.containsKey(type)) return false;
        return this.weapons.getOrDefault(type, Collections.emptySet())
                .contains(weapon);
    }

    /**
     * Get all the armory weapons
     *
     * @return the armory weapons
     */
    @Override
    public Collection<Weapon> getWeapons() {
        return Collections.unmodifiableCollection(this.weaponCollection);
    }

    /**
     * Get all the armory weapons of the
     * specified type
     *
     * @param type the type to filter for
     * @return the weapons of the type
     */
    @Override
    public Collection<Weapon> getWeapons(final WeaponType type) {
        return Collections.unmodifiableSet(this.weapons.getOrDefault(
                type, Collections.emptySet()
        ));
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @NotNull
    @Override
    public Iterator<Weapon> iterator() {
        return new ArrayList<>(this.weaponCollection).iterator();
    }

    /**
     * Get the armory hashcode
     *
     * @return the armory hashcode
     */
    @Override
    public int hashCode() {
        return this.uuns.hashCode();
    }

    /**
     * Get if the specified object is
     * the same as the provided one
     *
     * @param obj the other object
     * @return if the objects are the
     *      *            same
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Armory)) return false;

        Armory other = (Armory) obj;
        return this.uuns.equals(other.getUUNS());
    }

    /**
     * Get the string representation of the
     * armory
     *
     * @return the armory string representation
     */
    @Override
    public String toString() {
        return String.format("CraftArmory{\"weapons\": %d, \"uuns\": \"%s\", \"name\": \"%s\", \"description\": %s, \"version\": %s, \"api version\": %d, " +
                        "\"author\": %s}",
                this.size(), this.uuns, this.name, this.description
                        .replace("\n", "\\n"), this.version, this.apiVersion, this.author);
    }

    /**
     * Create a craft armory from a file
     *
     * @param file the file
     * @return the craft armory
     * @throws IllegalArgumentException if the file is null or is not a
     * json file
     * @throws InvalidArmoryException if the json is invalid armory
     * @throws InvalidWeaponException if any of the armory weapons are not valid
     */
    @Contract("null -> fail")
    public static CraftArmory fromFile(final Path file) throws IOException, IllegalArgumentException, InvalidArmoryException, InvalidWeaponException {
        CraftArmoryLoader loader = new CraftArmoryLoader(file);
        loader.parse();

        return loader.getArmory();
    }

    /**
     * Create a craft armory from a json. It's highly
     * recommended that if the json is parsed from a file,
     * to use the method {@link #fromFile(Path)} instead
     *
     * @param object the json object
     * @return the craft armory
     * @throws IllegalArgumentException if the json is null
     * @throws InvalidArmoryException if the json is invalid armory
     * @throws InvalidWeaponException if any of the armory weapons are not valid
     */
    @Contract("null -> fail")
    public static CraftArmory fromJson(final JsonObject object) throws IllegalArgumentException, InvalidArmoryException, InvalidWeaponException {
        if (object == null)
            throw new IllegalArgumentException("Cannot create armory from null json file");

        CraftArmoryLoader loader = new CraftArmoryLoader(object.toString());
        loader.parse();

        return loader.getArmory();
    }
}
