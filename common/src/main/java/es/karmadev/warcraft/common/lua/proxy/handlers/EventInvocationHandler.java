package es.karmadev.warcraft.common.lua.proxy.handlers;

import io.github.taoguan.luaj.LuaValue;
import party.iroiro.luajava.JuaAPI;
import party.iroiro.luajava.luaj.LuaJ;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class EventInvocationHandler implements InvocationHandler {

    private final LuaJ lua;
    private final LuaValue arg1;

    public EventInvocationHandler(final LuaJ lua, final LuaValue arg1) {
        this.lua = lua;
        this.arg1 = arg1;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        switch (method.getName()) {
            case "toString":
                return this.toString();
            case "equals":
                Object other = args[0];
                return this.hashCode() == other.hashCode();
            case "hashCode":
                return this.hashCode();
            case "handle":
                if (args.length != 1) return null;
                arg1.call(
                        JuaAPI.valueOfObject(
                                lua.getId(), args[0]
                        )
                );
            default:
                return null;
        }
    }
}
