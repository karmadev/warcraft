package es.karmadev.warcraft.common.lua.world;

import es.karmadev.warcraft.common.world.level.CraftTeam;

public class Team extends CraftTeam {

    public Team(final String name) {
        super(name);
    }
}
