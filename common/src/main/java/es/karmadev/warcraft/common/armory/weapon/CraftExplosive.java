package es.karmadev.warcraft.common.armory.weapon;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.explosion.WeaponExplosion;
import es.karmadev.warcraft.api.armory.weapon.components.model.ModelType;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.api.armory.weapon.type.ExplosiveWeapon;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.explosion.CraftExplosion;
import es.karmadev.warcraft.common.armory.weapon.components.model.CraftModel;
import org.jetbrains.annotations.Contract;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;

@SuppressWarnings("unused")
public class CraftExplosive extends CraftWeapon implements ExplosiveWeapon {

    private final static EnumSet<ComponentType> supportedComponents = EnumSet.of(ComponentType.MODEL,
            ComponentType.EXPLOSION);

    private WeaponModel model;
    private WeaponExplosion explosion;
    private double visibility;
    private int units;
    private double cadence;

    /**
     * Creates an explosive
     *
     * @param armory the explosive armory
     * @param name the explosive name
     * @param weaponObject the explosive json
     * @throws InvalidWeaponException if the weapon data
     * is invalid
     */
    public CraftExplosive(final Armory armory, final String name, final JsonObject weaponObject) throws InvalidWeaponException {
        super(armory, name);
        map(weaponObject);
    }

    /**
     * Get the weapon model
     *
     * @return the weapon model
     */
    @Override
    public WeaponModel getModel() {
        return this.model;
    }

    /**
     * Get the explosive weapon
     * explosion
     *
     * @return the explosion
     */
    @Override
    public WeaponExplosion getExplosion() {
        return this.explosion;
    }

    /**
     * Get the weapon visibility
     * scale
     *
     * @return the visibility scale
     */
    @Override
    public double getVisibilityScale() {
        return this.visibility;
    }

    /**
     * Get the amount of units of
     * this weapon per instance
     *
     * @return the amount of explosives
     * per instance
     */
    @Override
    public int getUnits() {
        return this.units;
    }

    /**
     * Get the explosive placement
     * cadence. In case of explosives,
     * cadence is the amount of time it
     * takes to place/throw the explosive,
     * and not the time to wait after placing
     * or throwing the explosive
     *
     * @return the explosive placement
     * cadence.
     */
    @Override
    public double getCadence() {
        return this.cadence;
    }

    /**
     * Get a weapon component by
     * component type
     *
     * @param type  the component type
     * @param cType the component object type
     * @return the weapon component
     */
    @Override
    public <T extends WeaponComponent> T getComponent(final ComponentType type, final Class<T> cType) {
        if (type == null || cType == null)
            return null;

        if (!CraftExplosive.supportedComponents.contains(type))
            throw new IllegalArgumentException("Component " + type + " is not supported");

        switch (type) {
            case MODEL:
                if (this.model == null) return null;
                if (cType.isAssignableFrom(this.model.getClass()))
                    return cType.cast(this.model);

                throw new IllegalArgumentException("Cannot get component model of type " + cType.getSimpleName() + " for explosive");
            case EXPLOSION:
                if (this.explosion == null) return null;
                if (cType.isAssignableFrom(this.explosion.getClass()))
                    return cType.cast(this.explosion);

                throw new IllegalArgumentException("Cannot get component explosion of type " + cType.getSimpleName() + " for explosive");
            default:
                throw new IllegalArgumentException("No such component of type " + cType.getSimpleName() + " for explosive");
        }
    }

    /**
     * Get all the weapon components
     *
     * @return the weapon components
     */
    @Override
    public Collection<? extends WeaponComponent> getComponents() {
        return Arrays.asList(this.model, this.explosion);
    }

    /**
     * Get if the weapon has available
     * the specified component
     *
     * @param type the component type
     * @return if the weapon has teh component
     */
    @Override
    public boolean hasComponent(final ComponentType type) {
        return CraftExplosive.supportedComponents.contains(type);
    }

    /**
     * Get the string representation of
     * the gun
     *
     * @return the gun as a string
     */
    @Override
    public String toString() {
        return String.format("CraftExplosiveWeapon{\"armory\": %s, \"uuns\": \"%s\", \"name\": \"%s\", \"model type\": %s, \"model\": \"%s\", " +
                        "\"explosion fuse\": %d, \"explosion distance\": %d, \"explosion distance penalty\": %d, \"explosion damage\": %.2f, " +
                        "\"explosion damage penalty\": %.2f, \"explosion sounds\": %d, \"visibility\" %.1f, \"units\": %d, \"cadence\": %.2f}",
                this.armory, this.getUUNS(), this.name, this.model.getType(), this.model.getModelData(),
                this.explosion.getFuseTime(), this.explosion.getDistance(), this.explosion.getDistancePenalty(),
                this.explosion.getDamage(), this.explosion.getDamagePenalty(), this.explosion.getSounds().size(),
                this.visibility, this.units, this.cadence);
    }

    /**
     * Map the weapon
     *
     * @param weaponJson the weapon json
     * @throws InvalidWeaponException if the weapon is invalid
     */
    @Contract("null -> fail")
    private void map(final JsonObject weaponJson) throws InvalidWeaponException {
        if (weaponJson == null)
            throw new InvalidWeaponException(this.armory, this.name, "Cannot parse explosive for invalid data");

        ensureNotMissing(weaponJson,
                "description",
                "model", "model.type", "model.name",
                "explosion", "explosion.fuse", "explosion.effective_distance",
                "explosion.distance_penalty", "explosion.damage", "explosion.damage_penalty",
                "explosion.sounds", "visibility", "units", "cadence"
        );

        ensurePropType(weaponJson, JsonArray.class, "description");
        ensurePropType(weaponJson, JsonObject.class,
                "model", "explosion.sounds");
        ensurePropType(weaponJson, JsonNative.class,
                "model.type", "model.name",
                "explosion.fuse", "explosion.effective_distance", "explosion.distance_penalty",
                "explosion.damage", "explosion.damage_penalty", "visibility",
                "units", "cadence");

        JsonArray descriptionArray = weaponJson.asArray("description");
        JsonNative modelType = weaponJson.asNative("model.type");
        JsonNative modelName = weaponJson.asNative("model.name");
        JsonNative expFuse = weaponJson.asNative("explosion.fuse");
        JsonNative expDist = weaponJson.asNative("explosion.effective_distance");
        JsonNative expDistPen = weaponJson.asNative("explosion.distance_penalty");
        JsonObject expSounds = weaponJson.asObject("explosion.sounds");
        JsonNative expDam = weaponJson.asNative("explosion.damage");
        JsonNative expDamPen = weaponJson.asNative("explosion.damage_penalty");
        JsonNative visibility = weaponJson.asNative("visibility");
        JsonNative units = weaponJson.asNative("units");
        JsonNative cadence = weaponJson.asNative("cadence");

        if (!expFuse.isNumber() && !expFuse.isString())
            throw new InvalidWeaponException(this.armory, this.name,
                    "Invalid property explosion.fuse type. Expected number|string and got boolean");

        ensureString(modelType, modelName);
        ensureNumber(expDist, expDistPen, expDam,
                expDamPen, visibility, units, cadence);

        mapDescription(descriptionArray);
        this.model = new CraftModel(ModelType.valueOf(modelType.getString().toUpperCase()),
                modelName.getString());

        CraftSoundSet explosionSoundSet = buildSoundSet(expSounds, "0");
        this.explosion = new CraftExplosion(
                expFuse.asString(),
                expDist.getInteger(),
                expDistPen.getInteger(),
                expDam.getDouble(),
                expDamPen.getDouble(),
                explosionSoundSet
        );

        this.visibility = visibility.getDouble();
        this.units = units.getInteger();
        this.cadence = cadence.getDouble();
    }
}
