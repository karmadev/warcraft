package es.karmadev.warcraft.common.armory.weapon;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSound;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;

import java.util.*;

public abstract class CraftWeaponContainer implements Weapon {

    protected final Armory armory;
    protected final String name;

    protected Collection<String> description = Collections.emptyList();

    public CraftWeaponContainer(final Armory armory, final String name) {
        this.armory = armory;
        this.name = name;
    }

    /**
     * Get the hashcode for the
     * gun
     *
     * @return the gun hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.armory, this.getUUNS());
    }

    /**
     * Get if this gun is the same
     * as the specified object gun
     *
     * @param obj the object to check with
     * @return if the guns are the same
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Weapon)) return false;

        Weapon other = (Weapon) obj;
        return other.getWeaponType().equals(this.getWeaponType()) &&
                other.getUUNS().equals(this.getUUNS()) &&
                other.getName().equals(this.getName());
    }

    protected final void mapDescription(final JsonArray description) {
        this.description = new ArrayList<>();
        for (JsonInstance element : description) {
            if (!element.isNativeType()) continue;
            JsonNative jsonNative = element.asNative();
            if (!jsonNative.isString()) continue;

            this.description.add(jsonNative.getString());
        }
    }

    protected final CraftSoundSet buildSoundSet(final JsonInstance sounds, final String relativeTo) throws InvalidWeaponException {
        if (!sounds.isObjectType() && !sounds.isArrayType()) return null;

        List<CraftSound> soundList = new ArrayList<>();
        Iterable<JsonInstance> iterable = (sounds.isArrayType() ? sounds.asArray() : sounds.asObject());
        String key = "0";

        for (JsonInstance instance : iterable) {
            if (!instance.isArrayType() && !instance.isObjectType()) continue;
            if (sounds.isObjectType())
                key = instance.getKey();

            handleSoundSet(key, instance, soundList, relativeTo);
        }

        return new CraftSoundSet(null, soundList);
    }

    protected final void handleSoundSet(final String key, final JsonInstance instance,
                                        final Collection<CraftSound> soundList, final String relativeTo) throws InvalidWeaponException {
        Object keyToUse = key;
        try {
            keyToUse = Long.parseLong(key);
        } catch (NumberFormatException ignored) {}

        if (instance.isObjectType()) {
            JsonObject object = instance.asObject();
            soundList.addAll(parseSound(keyToUse, object, relativeTo));
        } else {
            JsonArray array = instance.asArray();

            for (JsonInstance a : array) {
                if (!a.isObjectType()) continue;

                JsonObject object = a.asObject();
                soundList.addAll(parseSound(keyToUse, object, relativeTo));
            }
        }
    }

    protected final Collection<CraftSound> parseSound(final Object key, final JsonObject object, final String relativeTo) throws InvalidWeaponException {
        ensureNotMissing(object, "name", "pitch", "volume");

        ensurePropType(object, JsonNative.class, "name", "pitch", "volume");

        JsonNative soundName = object.asNative("name");
        JsonNative soundPitch = object.asNative("pitch");
        JsonNative soundVolume = object.asNative("volume");

        ensureString(soundName);
        ensureNumber(soundPitch, soundVolume);

        long relative = CraftComponent.parseTime(relativeTo);
        List<CraftSound> sounds = new ArrayList<>();
        if (key instanceof String) {
            String str = (String) key;

            if (str.matches("^([0-9]{1,2}|100)-([0-9]{1,2}|100)%$")) {
                String[] data = str.substring(0, str.length() - 1)
                        .split("-");

                int p1 = Integer.parseInt(data[0]);
                int p2 = Integer.parseInt(data[1]);

                for (int i = Math.min(p1, p2); i <= Math.max(p1, p2); i++) {
                    sounds.add(
                            new CraftSound(String.format("%d%%", i), soundName.getString(),
                                    soundPitch.getFloat(), soundVolume.getFloat())
                    );
                }
            } else {
                return Collections.singleton(new CraftSound(str, soundName.getString(),
                        soundPitch.getFloat(), soundVolume.getFloat()));
            }
        } else {
            long value = (long) key;
            if (value < 0) {
                for (double d = 0.0d; d <= (double) (relative / 1000); d++) {
                    sounds.add(
                            new CraftSound((long) d * 1000, soundName.getString(),
                                    soundPitch.getFloat(), soundVolume.getFloat())
                    );
                }
            } else {
                return Collections.singleton(new CraftSound(value, soundName.getString(),
                        soundPitch.getFloat(), soundVolume.getFloat()));
            }
        }

        return sounds;
    }

    protected final void ensureNotMissing(final JsonObject object, final String... entries) throws InvalidWeaponException {
        for (String entry : entries)
            if (!object.hasChild(entry))
                throw new InvalidWeaponException(this.armory, this.name, "Missing property: " + entry);
    }

    protected final void ensurePropType(final JsonObject object, final Class<? extends JsonInstance> type, final String... entries) throws InvalidWeaponException {
        for (String entry : entries) {
            JsonInstance instance = object.getChild(entry);
            if (!type.isAssignableFrom(instance.getClass()))
                throw new InvalidWeaponException(this.armory, this.name, "Invalid property " + entry + " type. Expected " + type.getSimpleName() + " and got " +
                        instance.getClass().getSimpleName());
        }
    }

    protected final void ensureString(final JsonNative... elements) throws InvalidWeaponException {
        for (JsonNative element : elements) {
            if (!element.isString()) {
                String type = element.isBoolean() ? "boolean" : element.isNumber() ? "number" : "null";
                throw new InvalidWeaponException(this.armory, this.name, "Invalid property " + element.getKey() + " type. Expected string and got " + type);
            }
        }
    }

    protected final void ensureNumber(final JsonNative... elements) throws InvalidWeaponException {
        for (JsonNative element : elements) {
            if (!element.isNumber()) {
                String type = element.isBoolean() ? "boolean" : element.isString() ? "string" : "null";
                throw new InvalidWeaponException(this.armory, this.name, "Invalid property " + element.getKey() + " type. Expected number and got " + type);
            }
        }
    }

    protected final void ensureBoolean(final JsonNative... elements) throws InvalidWeaponException {
        for (JsonNative element : elements) {
            if (!element.isBoolean()) {
                String type = element.isString() ? "string" : element.isNumber() ? "number" : "null";
                throw new InvalidWeaponException(this.armory, this.name, "Invalid property " + element.getKey() + " type. Expected boolean and got " + type);
            }
        }
    }
}
