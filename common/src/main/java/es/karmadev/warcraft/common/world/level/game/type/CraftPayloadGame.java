package es.karmadev.warcraft.common.world.level.game.type;

import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.api.world.level.game.exception.AsyncGameOperation;
import es.karmadev.warcraft.api.world.level.game.type.PayloadGame;
import es.karmadev.warcraft.api.world.level.payload.Payload;
import es.karmadev.warcraft.api.world.level.payload.trace.PayloadTrace;
import es.karmadev.warcraft.common.world.level.game.CraftGame;

import java.util.concurrent.TimeUnit;

/**
 * Represents a payload game
 */
public class CraftPayloadGame extends CraftGame implements PayloadGame {

    private final int prepareTime;

    private long preparationRemaining;
    private float speed;

    public CraftPayloadGame(final String name, final int prepareTime, final int maxPlayers,
                            final long timeLimit, final Team first, final Team... other) {
        super(name, maxPlayers, timeLimit, first, other);
        this.prepareTime = prepareTime;
        this.preparationRemaining = TimeUnit.SECONDS.toMillis(prepareTime);
    }

    /**
     * Get the payload
     *
     * @return the game payload
     */
    @Override
    public Payload getPayload() {
        return null;
    }

    /**
     * Set the payload speed for the current
     * tick
     *
     * @param speed the speed
     * @throws AsyncGameOperation if the speed is set
     *                            out of the game defined tick
     */
    @Override
    public void setPayloadSpeed(final float speed) throws AsyncGameOperation {
        if (tickManager.isSynchronized()) {
            this.speed = speed;
            return;
        }

        throw new AsyncGameOperation("setPayloadSpeed", "Payload speed is defined on each tick, it cannot be set out of tick rate!");
    }

    /**
     * Get the payload speed for the
     * current tick
     *
     * @return the speed
     */
    @Override
    public float getPayloadSpeed() {
        return 0;
    }

    /**
     * Get the payload trace
     *
     * @return the payload trace
     */
    @Override
    public PayloadTrace getPayloadTrace() {
        return null;
    }

    /**
     * Get the time before the game
     * actually starts, in seconds
     *
     * @return the preparation time
     */
    @Override
    public int getPrepareTime() {
        return this.prepareTime;
    }

    /**
     * Get the remaining seconds before
     * the preparation time ends
     *
     * @return the preparation live time
     */
    @Override
    public int getPrepareRemaining() {
        return (int) TimeUnit.MILLISECONDS.toSeconds(this.preparationRemaining);
    }

    /**
     * Execute a tick
     */
    @Override
    public void runTick() {
        if (!this.playing) {
            if (--this.preparationRemaining <= 0) {
                this.playing = true;
            }
        }
    }

    /**
     * Get if the game is in the
     * preparation stage
     *
     * @return if the game is in the
     * preparations
     */
    @Override
    public boolean isPreparing() {
        return this.preparationRemaining > 0;
    }
}
