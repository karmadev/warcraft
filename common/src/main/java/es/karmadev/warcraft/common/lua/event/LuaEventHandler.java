package es.karmadev.warcraft.common.lua.event;

import org.bukkit.event.Event;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class LuaEventHandler {

    private final static Collection<EventProxy<?>> proxies = ConcurrentHashMap.newKeySet();

    public static void add(final EventProxy<?> proxy) {
        proxies.add(proxy);
    }

    public static void remove(final EventProxy<?> proxy) {
        proxies.remove(proxy);
    }

    public static void trigger(final Event event) {
        proxies.forEach((p) -> {
            Class<?> clazz = p.getClass();
            try {
                Method m = clazz.getDeclaredMethod("handle", event.getClass());
                m.invoke(p, event);
            } catch (Throwable ignored) {}
        });
    }
}
