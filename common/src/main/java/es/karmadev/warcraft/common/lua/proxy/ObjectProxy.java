package es.karmadev.warcraft.common.lua.proxy;

import es.karmadev.warcraft.common.lua.proxy.handlers.EventInvocationHandler;
import es.karmadev.warcraft.common.lua.proxy.handlers.InterfaceInvocationHandler;
import io.github.taoguan.luaj.LuaValue;
import party.iroiro.luajava.luaj.LuaJ;

import java.lang.reflect.Proxy;

public class ObjectProxy {

    public static Object forEventHandler(final LuaJ lua, final LuaValue arg1, final Class<?>... interfaces) throws ReflectiveOperationException {
        return Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), interfaces, new EventInvocationHandler(lua, arg1));
    }

    public static Object ofInstance(final Object instance, final Class<?>[] interfaces) throws ReflectiveOperationException {
        return Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), interfaces, new InterfaceInvocationHandler(instance, interfaces));
    }
}
