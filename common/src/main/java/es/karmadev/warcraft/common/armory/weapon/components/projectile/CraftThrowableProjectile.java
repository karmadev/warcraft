package es.karmadev.warcraft.common.armory.weapon.components.projectile;

import es.karmadev.warcraft.api.armory.weapon.components.projectile.ThrowableProjectile;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;

public class CraftThrowableProjectile extends CraftComponent implements ThrowableProjectile {

    private final boolean bounce;
    private final double speed;
    private final boolean constantSpeed;
    private final boolean explodeOnHit;

    public CraftThrowableProjectile(final boolean bounce, final double speed, final boolean constantSpeed, final boolean explodeOnHit) {
        this.bounce = bounce;
        this.speed = speed;
        this.constantSpeed = constantSpeed;
        this.explodeOnHit = explodeOnHit;
    }

    /**
     * Get if the projectile bounces
     *
     * @return if the bounce logic
     * is enabled for this throwable
     * weapon
     */
    @Override
    public boolean isBounce() {
        return this.bounce;
    }

    /**
     * Get the projectile speed
     *
     * @return the projectile speed
     */
    @Override
    public double getSpeed() {
        return this.speed;
    }

    /**
     * Get if the projectile speed is
     * constant
     *
     * @return if the speed is constant
     */
    @Override
    public boolean isConstantSpeed() {
        return this.constantSpeed;
    }

    /**
     * Get if the projectile explodes
     * as soon as it hits a solid
     * object
     *
     * @return if the projectile explodes
     * when it hits something
     */
    @Override
    public boolean isExplodeOnHit() {
        return this.explodeOnHit;
    }
}
