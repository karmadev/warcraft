package es.karmadev.warcraft.common.armory.weapon.components.building;

import es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry.SentryBuildingWeapon;
import es.karmadev.warcraft.api.armory.weapon.components.building.type.sentry.level.SentryBuildingLevel;

import java.util.Collection;
import java.util.Collections;

public class CraftSentryLevel extends CraftLevel implements SentryBuildingLevel {

    private final Collection<SentryBuildingWeapon> weapons;

    public CraftSentryLevel(final int level, final double radius, final double health, final Collection<SentryBuildingWeapon> weapons) {
        super(level, radius, health);
        this.weapons = Collections.unmodifiableCollection(weapons);
    }

    /**
     * Get the level weapons
     *
     * @return the sentry gun level
     * weapons
     */
    @Override
    public Collection<SentryBuildingWeapon> getWeapons() {
        return this.weapons;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("CraftSentryLevel{\"level\": %d, \"radius\": %.2f, \"weapons\": %s}",
                this.getLevel(), this.getRadius(), this.weapons);
    }
}
