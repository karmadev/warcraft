package es.karmadev.warcraft.common.armory.weapon.components;

import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSound;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.stream.Collectors;

public class CraftSoundSet implements WeaponSoundSet {

    private final Collection<CraftSound> sounds;
    private final WeaponComponent parent;

    public CraftSoundSet(final WeaponComponent parent, final Collection<CraftSound> sounds) {
        this.parent = parent;
        Collection<CraftSound> parsed = sounds;
        if (parent != null) {
            parsed = sounds.stream().map((cs) -> cs.atSet(this))
                    .collect(Collectors.toSet());
        }

        this.sounds = parsed;
    }

    public WeaponSoundSet atComponent(final WeaponComponent component) {
        Collection<CraftSound> collection = Collections
                .unmodifiableCollection(this.sounds);

        return new CraftSoundSet(component, collection);
    }

    /**
     * Get the component this sound set
     * pertains to
     *
     * @return the soundset component
     */
    @Override
    public WeaponComponent getComponent() {
        return this.parent;
    }

    /**
     * Get all the sounds in the sound
     * set
     *
     * @return the sounds
     */
    @Override
    public Collection<? extends WeaponSound> getSounds() {
        return Collections.unmodifiableCollection(this.sounds);
    }

    /**
     * Get all the sounds in the sound
     * set which play in the specified time
     *
     * @param time the time when the
     *             song plays
     * @return the time sounds
     */
    @Override
    public Collection<? extends WeaponSound> getSounds(long time) {
        return getSounds().stream()
                .filter((s) -> s.getTimeExact() == time)
                .collect(Collectors.toSet());
    }

    /**
     * Get the soundset size
     *
     * @return the soundset size
     */
    @Override
    public int size() {
        return this.sounds.size();
    }

    /**
     * Get if the sounds are empty
     *
     * @return if the soundset is empty
     */
    @Override
    public boolean isEmpty() {
        return this.sounds.isEmpty();
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @NotNull
    @Override
    public Iterator<WeaponSound> iterator() {
        Collection<WeaponSound> collection = Collections
                .unmodifiableCollection(this.sounds);
        return collection.iterator();
    }
}
