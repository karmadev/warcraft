package es.karmadev.warcraft.common.armory.parser;

import es.karmadev.api.kson.*;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.warcraft.api.WarCraft;
import es.karmadev.warcraft.api.exception.armory.InvalidArmoryException;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.api.plugin.armory.ArmoryManager;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import es.karmadev.warcraft.common.armory.CraftArmory;
import es.karmadev.warcraft.common.armory.CraftArmoryAuthor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Self-explanatory
 */
public class CraftArmoryLoader {

    private final static String PATH_UUNS_DEFAULT = "warcraft";
    private final JsonObject data;

    private CraftArmory armory;

    /**
     * Create an armory loader for
     * the specified raw armory
     *
     * @param raw the raw armory
     * @throws NullPointerException if the raw data is null
     * @throws IllegalStateException if there's a problem while
     * parsing the raw data
     * @throws InvalidArmoryException if the raw data does not provide
     * a valid armory
     */
    public CraftArmoryLoader(final String raw) throws NullPointerException, IllegalStateException, InvalidArmoryException {
        if (raw == null) throw new NullPointerException();
        byte[] data = raw.getBytes();

        this.data = instanceToObject(data);
    }

    /**
     * Create an armory loader for
     * the specified file
     *
     * @param file the file to read
     * @throws IOException if there's a problem reading the file
     * @throws IllegalStateException if there's a problem while parsing the
     * file data
     * @throws InvalidArmoryException if the file data does not provide
     * a valid armory
     */
    public CraftArmoryLoader(final Path file) throws IOException, IllegalStateException, InvalidArmoryException {
        this(new String(
                Files.readAllBytes(file)
        ));
    }

    /**
     * Parses the armory
     *
     * @throws IllegalStateException if the armory data is invalid
     * @throws InvalidArmoryException if the armory itself is bad
     * formatted
     * @throws InvalidWeaponException if there's an invalid weapon
     */
    public void parse() throws IllegalStateException, InvalidArmoryException, InvalidWeaponException {
        if (this.data == null)
            throw new IllegalStateException("Cannot create armory from null armory json");

        ensureNoPropertyIsMissingFromJson();

        String name = this.readPropertyName();
        String description = this.readPropertyDescription();
        String version = this.readPropertyVersion();
        int apiVersion = this.readPropertyApiVersion();
        CraftArmoryAuthor author = this.readPropertyAuthor();
        UUNS uuns = this.readPropertyUUNS();
        boolean bundled = this.readPropertyIsBundled();

        ensureArmoryDoesNotExist(uuns);

        armory = new CraftArmory(uuns, name, version, description, apiVersion, author);
        mapArmory(armory, bundled);
    }

    public CraftArmory getArmory() {
        return this.armory;
    }

    private void mapArmory(final CraftArmory armory, final boolean isBundled) throws InvalidArmoryException, InvalidWeaponException, IllegalStateException {
        ArmoryLoader loader;
        if (isBundled) {
            String missingBundler = findMissingProperty(this.data, JsonObject.class, "bundler");
            if (missingBundler != null)
                throw new InvalidArmoryException("json", "Cannot parse armory " + armory.getName() + " because property \"bundler\" is missing");

            JsonObject bundler = this.data.asObject("bundler");
            loader = new CraftBundlerLoader(bundler);
        } else {
            String configurationMissing = findMissingProperty(this.data, JsonObject.class, "configuration");
            if (configurationMissing != null)
                throw new InvalidArmoryException("json", "Cannot parse armory " + armory.getName() + " because property \"configuration\" is missing");

            JsonObject configuration = this.data.asObject("configuration");
            loader = new CraftConfigLoader(configuration);
        }

        loader.mapTo(armory);
    }

    private void ensureNoPropertyIsMissingFromJson() throws InvalidArmoryException {
        String missing = findMissingProperty(this.data, JsonNative.class, "name", "version", "api_version",
                "author.name", "author.site", "author.contact",
                "uuns.path", "uuns.value", "bundled");

        if (missing == null)
            missing = findMissingProperty(this.data, JsonArray.class, "description");

        if (missing != null)
            throw new InvalidArmoryException("json", "Missing property " + missing);
    }

    private void ensureArmoryDoesNotExist(final UUNS uuns) throws IllegalStateException, InvalidArmoryException {
        WarCraft plugin = WarCraft.getInstance();
        if (plugin == null)
            throw new IllegalStateException("Cannot validate armory existence because WarCraft plugin has not been initialized");

        ArmoryManager manager = plugin.getArmoryManager();
        if (manager.contains(uuns))
            throw new InvalidArmoryException("json", "Another armory with the same name (" + uuns + ") has been already registered");
    }

    private String readPropertyName() throws InvalidArmoryException {
        return readPropertyString("name");
    }

    private String readPropertyDescription() {
        JsonArray description = this.data.asArray("description");

        StringBuilder builder = new StringBuilder();
        for (JsonInstance instance : description)
            builder.append(instance.asString()).append('\n');

        return (builder.length() == 0 ? "" : builder.substring(0, builder.length() - 1));
    }

    private String readPropertyVersion() throws InvalidArmoryException {
        return readPropertyString("version");
    }

    private int readPropertyApiVersion() throws InvalidArmoryException {
        JsonNative property = this.data.asNative("api_version");
        ensureNumber("Property \"{}\" is required to be a number", property);

        return property.getNumber().intValue();
    }

    private String readPropertyAuthorName() throws InvalidArmoryException {
        return readPropertyString("author.name");
    }

    private String readPropertyAuthorSite() throws InvalidArmoryException {
        return readPropertyString("author.site");
    }

    private String readPropertyAuthorContact() throws InvalidArmoryException {
        return readPropertyString("author.contact");
    }

    private CraftArmoryAuthor readPropertyAuthor() throws InvalidArmoryException {
        String name = this.readPropertyAuthorName();
        String site = this.readPropertyAuthorSite();
        String contact = this.readPropertyAuthorContact();

        return new CraftArmoryAuthor(name, site, contact);
    }

    private UUNS readPropertyUUNS() throws InvalidArmoryException {
        String path = readPropertyString("uuns.path");
        String value = readPropertyString("uuns.value");

        if (path.equalsIgnoreCase("default"))
            path = PATH_UUNS_DEFAULT;

        return UUNS.create(path, value);
    }

    private boolean readPropertyIsBundled() throws InvalidArmoryException {
        JsonNative bundled = this.data.asNative("bundled");
        ensureBoolean("Property \"{}\" is required to be a boolean", bundled);

        return bundled.getBoolean();
    }

    private String readPropertyString(final String propName) throws InvalidArmoryException {
        JsonNative property = this.data.asNative(propName);
        ensureString("Property \"{}\" is required to be a string", property);

        return property.getString();
    }

    protected static JsonObject instanceToObject(final byte[] data) throws InvalidArmoryException {
        try {
            JsonInstance instance = JsonReader.parse(data);

            if (!instance.isObjectType())
                throw new InvalidArmoryException("json", "Unexpected armory format, expected a json object ({), but got array ([)");

            return instance.asObject();
        } catch (KsonException ex) {
            IllegalStateException unexpected = new IllegalStateException("Unexpected exception while reading " + new String(data));
            unexpected.addSuppressed(ex);

            throw unexpected;
        }
    }

    protected static String findMissingProperty(final JsonObject object, final Class<? extends JsonInstance> requiredType,
                                              final String... properties) {
        for (String property : properties) {
            if (!object.hasChild(property))
                return property;

            JsonInstance instance = object.getChild(property);
            if (!requiredType.isAssignableFrom(instance.getClass()))
                return property;
        }

        return null;
    }

    protected static void ensureString(final String message,
                                     final JsonNative... natives) throws InvalidArmoryException {
        for (JsonNative nat : natives) {
            if (!nat.isString())
                throw new InvalidArmoryException("json", message.replace("{}", nat.getKey()));
        }
    }

    protected static void ensureNumber(final String message,
                                     final JsonNative... natives) throws InvalidArmoryException {
        for (JsonNative nat : natives) {
            if (!nat.isNumber())
                throw new InvalidArmoryException("json", message.replace("{}", nat.getKey()));
        }
    }

    protected static void ensureBoolean(final String message,
                                      final JsonNative... natives) throws InvalidArmoryException {
        for (JsonNative nat : natives) {
            if (!nat.isBoolean())
                throw new InvalidArmoryException("json", message.replace("{}", nat.getKey()));
        }
    }
}