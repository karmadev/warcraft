package es.karmadev.warcraft.common.world.level;

import es.karmadev.warcraft.api.world.level.TeamScore;
import es.karmadev.warcraft.api.world.level.player.Client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CraftTeamScore implements TeamScore {

    private float score;
    private final Map<Client, Float> scores = new ConcurrentHashMap<>();

    /**
     * Get the team score
     *
     * @return the team total score
     */
    @Override
    public float getScore() {
        return this.score;
    }

    /**
     * Get the score a client issued on
     * this team
     *
     * @param client the client
     * @return the score the client
     * issued
     */
    @Override
    public float getScore(final Client client) {
        return this.scores.get(client);
    }

    /**
     * Get the team score leaderboard
     *
     * @param limit the leaderboard limit
     * @return the leaderboard
     */
    @Override
    public Client[] getLeaderBoard(final int limit) {
        return new Client[0];
    }

    /**
     * Add score to the team
     *
     * @param client the client who issued
     *               the score
     * @param score  the score amount
     */
    @Override
    public void addScore(final Client client, final float score) {
        this.scores.compute(client, (k, v) -> v == null ? score : v + score);
        this.score += score;
    }

    /**
     * Add assisted score to the
     * team
     *
     * @param clients the clients involved in the
     *                score
     * @param score   the score amount, which gets shared
     *                between all the clients
     */
    @Override
    public void assistedScore(final Client[] clients, final float score) {
        float scoreToAdd = score / clients.length;
        for (Client client : clients)
            this.addScore(client, scoreToAdd);
    }
}
