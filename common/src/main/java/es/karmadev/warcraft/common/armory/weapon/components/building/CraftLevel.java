package es.karmadev.warcraft.common.armory.weapon.components.building;

import es.karmadev.warcraft.api.armory.weapon.components.building.BuildingLevel;

public class CraftLevel implements BuildingLevel {

    private final int level;
    private final double radius;
    private final double health;

    public CraftLevel(final int level, final double radius, final double health) {
        this.level = level;
        this.radius = radius;
        this.health = health;
    }

    /**
     * Get the level
     *
     * @return the level
     */
    @Override
    public int getLevel() {
        return this.level;
    }

    /**
     * Get the building health
     *
     * @return the building health
     * override
     */
    @Override
    public double getHealth() {
        return this.health;
    }

    /**
     * Get the building radius override
     *
     * @return the level radius override
     */
    @Override
    public double getRadius() {
        return this.radius;
    }
}
