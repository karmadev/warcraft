package es.karmadev.warcraft.common.armory.weapon.components.fuse;

import es.karmadev.warcraft.api.armory.weapon.components.fuse.WeaponFuse;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;

public class CraftFuse extends CraftComponent implements WeaponFuse {

    private final long time;
    private final WeaponSoundSet tickSounds;
    private final WeaponSoundSet explodeSounds;

    public CraftFuse(final String time, final CraftSoundSet tickSounds, final CraftSoundSet explodeSounds) {
        this(parseTime(time), tickSounds, explodeSounds);
    }

    public CraftFuse(final long time, final CraftSoundSet tickSounds, final CraftSoundSet explodeSounds) {
        this.time = time;
        this.tickSounds = tickSounds.atComponent(this);
        this.explodeSounds = explodeSounds.atComponent(this);
    }

    /**
     * Get the weapon fuse time
     *
     * @return the fuse time
     */
    @Override
    public long getFuseTime() {
        return this.time;
    }

    /**
     * Get the weapon ticking sound
     *
     * @return the ticking sounds
     */
    @Override
    public WeaponSoundSet getTickSounds() {
        return this.tickSounds;
    }

    /**
     * Get the weapon explosion sounds
     *
     * @return the explosion sounds
     */
    @Override
    public WeaponSoundSet getExplodeSounds() {
        return this.explodeSounds;
    }
}
