package es.karmadev.warcraft.common.world.level;

import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.processor.conversor.FieldTransformer;
import es.karmadev.warcraft.api.world.level.Team;

public class TeamJsonTransformer extends FieldTransformer<Team, JsonObject> {


    /**
     * Transform the element into the
     * transformer value
     *
     * @param element the element
     * @return the element value element
     */
    @Override
    public JsonObject transformToValue(final Object element) {
        if (!(element instanceof Team)) return null;
        Team team = (Team) element;

        JsonObject object = JsonObject.newObject("", "");
        object.put("name", team.getName());
        object.put("players", team.getMaxPlayers());

        return object;
    }

    /**
     * Transform the string into the
     * element
     *
     * @param element the element
     * @return the transformed element
     */
    @Override
    public Team transformFromValue(final Object element) {
        if (!(element instanceof JsonObject)) return null;

        JsonObject object = (JsonObject) element;
        if (!object.hasChild("name") || !object.hasChild("players"))
            return null;

        JsonInstance nameInstance = object.getChild("name");
        JsonInstance playerInstance = object.getChild("players");

        if (!nameInstance.isNativeType() || !playerInstance.isNativeType())
            return null;

        JsonNative name = nameInstance.asNative();
        JsonNative maxPlayers = playerInstance.asNative();

        if (!name.isString() || !maxPlayers.isNumber())
            return null;

        return new CraftTeam(name.getString(), maxPlayers.getNumber().intValue());
    }

    /**
     * Build a value out of a json instance
     *
     * @param instance the instance
     * @return the element
     */
    @Override
    public JsonObject fromElement(final JsonInstance instance) {
        if (!instance.isObjectType()) return null;
        return instance.asObject();
    }
}
