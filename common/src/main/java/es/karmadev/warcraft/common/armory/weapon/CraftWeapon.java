package es.karmadev.warcraft.common.armory.weapon;

import es.karmadev.warcraft.api.armory.Armory;

import java.util.*;

/**
 * Craft weapon implementation
 */
public abstract class CraftWeapon extends CraftWeaponContainer {

    public CraftWeapon(final Armory armory, final String name) {
        super(armory, name);
    }

    /**
     * Get the weapon armory
     *
     * @return the armory
     */
    @Override
    public Armory getArmory() {
        return this.armory;
    }

    /**
     * Get the weapon name
     *
     * @return the weapon name
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Get the weapon description
     *
     * @return the description
     */
    @Override
    public Collection<String> getDescription() {
        return Collections.unmodifiableCollection(this.description);
    }
}
