package es.karmadev.warcraft.common.lua.proxy.handlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class InterfaceInvocationHandler implements InvocationHandler {

    private final Object instance;
    private final Class<?>[] interfaces;

    public InterfaceInvocationHandler(final Object instance, final Class<?>... interfaces) {
        this.instance = instance;
        this.interfaces = interfaces;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws ReflectiveOperationException {
        String mName = method.getName();
        switch (mName) {
            case "toString":
            case "equals":
            case "hashCode":
                return method.invoke(instance, args);
            default:
                for (Class<?> i : interfaces) {
                    Method[] iMethods = i.getDeclaredMethods();
                    for (Method iSubMethod : iMethods) {
                        if (iSubMethod.equals(method)) {
                            return iSubMethod.invoke(instance, args);
                        }
                    }
                }
                return null;
        }
    }
}
