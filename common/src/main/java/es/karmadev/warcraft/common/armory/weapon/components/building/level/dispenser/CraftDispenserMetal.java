package es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser;

import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingMetal;

public class CraftDispenserMetal extends CraftRefiller implements DispenserBuildingMetal {

    public CraftDispenserMetal(final int amount, final String interval) {
        super(amount, interval);
    }
}
