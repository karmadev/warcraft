package es.karmadev.warcraft.common.lua.event.proxy;

import es.karmadev.warcraft.common.lua.event.EventProxy;
import org.bukkit.event.player.PlayerQuitEvent;

public interface PlayerLeave extends EventProxy<PlayerQuitEvent> {

    @Override
    void handle(final PlayerQuitEvent event);
}