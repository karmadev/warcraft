package es.karmadev.warcraft.common.world.level.game;

import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.api.world.level.TeamScore;
import es.karmadev.warcraft.api.world.level.game.Game;
import es.karmadev.warcraft.api.world.level.game.respawn.RespawnHandler;
import es.karmadev.warcraft.api.world.level.game.tick.GameTickManager;
import es.karmadev.warcraft.api.world.level.player.Client;
import es.karmadev.warcraft.common.plugin.handler.CraftSpawnHandler;
import es.karmadev.warcraft.common.world.level.CraftTeam;
import es.karmadev.warcraft.common.world.level.game.tick.CraftTickManager;

import java.util.*;
import java.util.concurrent.TimeUnit;

public abstract class CraftGame implements Game {

    private final String name;
    private final Collection<Team> teams = new ArrayList<>();
    private final Map<Team, TeamScore> scores = new HashMap<>();
    private final Map<Client, Team> clientTeams = new HashMap<>();

    private final int maxPlayers;
    private final long timeLimit;

    protected final GameTickManager tickManager;
    protected final CraftSpawnHandler respawnHandler = new CraftSpawnHandler();

    private int players;
    private long timeRemaining;

    protected boolean playing = false;
    protected boolean finished = false;

    public CraftGame(final int tickJump, final String name, final int maxPlayers, final long timeLimit, final Team first, final Team... other) {
        this.name = name;
        if (maxPlayers <= 2)
            throw new IllegalStateException("Cannot create a game with max players of " + maxPlayers + ". At least two required");

        this.maxPlayers = maxPlayers;
        this.timeLimit = timeLimit;
        this.timeRemaining = timeLimit;

        this.tickManager = new CraftTickManager(tickJump, this);

        if (this.timeLimit <= TimeUnit.MINUTES.toMillis(1))
            throw new IllegalStateException("Cannot create a game with time limit of " +
                    TimeUnit.MILLISECONDS.toSeconds(timeLimit) + ". At least 1 minute is required");

        List<Team> validTeams = new ArrayList<>();
        if (first != null) validTeams.add(first);

        if (other != null) {
            for (Team tm : other) {
                if (tm == null) continue;
                validTeams.add(tm);
            }
        }

        if (validTeams.isEmpty()) throw new IllegalStateException("Cannot create a game without at least one team");
        this.teams.addAll(validTeams);
    }

    public CraftGame(final String name, final int maxPlayers, final long timeLimit, final Team first, final Team... other) {
        this(1, name, maxPlayers, timeLimit, first, other);
    }

    /**
     * Get the game name
     *
     * @return the game name
     */
    @Override
    public final String getName() {
        return this.name;
    }

    /**
     * Get the game respawn handler
     *
     * @return the game respawn
     * handler
     */
    @Override
    public final RespawnHandler getRespawn() {
        return respawnHandler;
    }

    /**
     * Get the maximum amount of clients
     * in the game
     *
     * @return the maximum amount of
     * clients
     */
    @Override
    public final int getMaxClients() {
        return this.maxPlayers;
    }

    /**
     * Get the amount of clients
     * in the game
     *
     * @return the amount of clients
     */
    @Override
    public final int getClientCount() {
        return this.players;
    }

    /**
     * Get the game time limit, in
     * milliseconds
     *
     * @return the game time limit
     */
    @Override
    public final long getTimeLimit() {
        return this.timeLimit;
    }

    /**
     * Get the game time remaining, in
     * milliseconds
     *
     * @return the game remaining time
     */
    @Override
    public final long getTimeRemaining() {
        return this.timeRemaining;
    }

    /**
     * Get the game tick manager
     *
     * @return the game tick manager
     */
    @Override
    public final GameTickManager getTickManager() {
        return this.tickManager;
    }

    /**
     * Perform a tick in the game
     */
    @Override
    public final void tick() {
        if (playing)
            Game.super.tick();

        this.tickManager.tickFunction();
    }

    /**
     * Execute a tick
     */
    public abstract void runTick();

    /**
     * Set the remaining time of
     * the game
     *
     * @param timeRemaining the new time remaining
     */
    @Override
    public final void setTime(final long timeRemaining) {
        this.timeRemaining = Math.max(0, Math.min(this.timeLimit, timeRemaining));
    }

    /**
     * Get if the game is currently
     * playing, which means the time is
     * counting down
     *
     * @return if the game time remaining is
     * counting down
     */
    @Override
    public final boolean isPlaying() {
        return this.playing;
    }

    /**
     * Get if the game has finished
     *
     * @return if the game has finished
     */
    @Override
    public final boolean hasFinished() {
        return this.finished;
    }

    /**
     * Get the game teams
     *
     * @return the game teams
     */
    @Override
    public final Collection<Team> getTeams() {
        return Collections.unmodifiableCollection(this.teams);
    }

    /**
     * Get the score of a team
     *
     * @param team the team
     * @return the team score
     */
    @Override
    public final TeamScore getScore(final Team team) {
        return this.scores.computeIfAbsent(team, (sc) -> null);
    }

    /**
     * Get all the clients on the provided
     * team
     *
     * @param team the team
     * @return the team clients
     */
    @Override
    public final Collection<Client> getClients(final Team team) {
        return Collections.emptyList();
    }

    /**
     * Get the team of a client
     *
     * @param client the client
     * @return the client team
     */
    @Override
    public final Team getTeam(final Client client) {
        return this.clientTeams.getOrDefault(client, CraftTeam.NONE);
    }

    /**
     * Tries to set a client team
     *
     * @param client  the client
     * @param newTeam the client new team
     * @return if the team was changed
     * successfully
     */
    @Override
    public final boolean setTeam(final Client client, final Team newTeam) {
        Team current = getTeam(client);
        return this.clientTeams.replace(client, current, newTeam); //This should return false if a concurrent operation
    }

    /**
     * Tries to remove a client from
     * the game
     *
     * @param client the client to remove
     * @return if the client was removed
     */
    @Override
    public final boolean remove(final Client client) {
        if (!this.clientTeams.containsKey(client)) return false;
        this.clientTeams.remove(client);

        --this.players;
        return true;
    }

    /**
     * Tries to add a client into
     * the game
     *
     * @param client the client to add
     * @return if the operation was successful
     */
    @Override
    public final boolean add(final Client client) {
        Team[] teams = this.teams.toArray(new Team[0]);
        Team lessPlayerTeam = teams[0]; //We know at least have one team

        if (teams.length == 1)
            return this.add(client, lessPlayerTeam); //Only one team available, direct assignment

        int lessCount = this.getClients(lessPlayerTeam).size();
        for (int teamIndex = 1; teamIndex < teams.length; teamIndex++) {
            Team tm = teams[teamIndex];
            int tmCount = this.getClients(tm).size();
            if (isTeamFull(tm)) continue;

            if (tmCount < lessCount) {
                lessPlayerTeam = tm;
                lessCount = tmCount;
            }
        }

        return this.add(client, lessPlayerTeam);
    }

    /**
     * Tries to add a client into
     * the game
     *
     * @param client the client to add
     * @param team   the team to set the client
     *               to
     * @return if the operation was successful
     */
    @Override
    public final boolean add(final Client client, final Team team) {
        if (this.players == this.maxPlayers) return false;
        if (isTeamFull(team)) return false;

        this.clientTeams.put(client, team);
        this.players++;

        return true;
    }

    protected final boolean isTeamFull(final Team team) {
        int count = this.getClients(team).size();
        int max = team.getMaxPlayers();
        if (max == 0) return false;
        if (max < 0) {
            int maxPlayers = this.getMaxClients();
            int autoAssigmentTeams = 0;
            for (Team tm : this.getTeams()) {
                if (tm.getMaxPlayers() < 0) autoAssigmentTeams++;
                else {
                    maxPlayers -= getClients(team).size();
                }
            }

            int finalMaxClients = (maxPlayers / autoAssigmentTeams);
            return count == finalMaxClients;
        }

        return count == max;
    }
}