package es.karmadev.warcraft.common.armory.weapon.components.building.level.dispenser;

import es.karmadev.warcraft.api.armory.weapon.components.building.type.dispenser.level.component.DispenserBuildingHealing;

public class CraftDispenserHeal extends CraftRefiller implements DispenserBuildingHealing {

    public CraftDispenserHeal(final double amount, final String interval) {
        super(amount, interval);
    }
}
