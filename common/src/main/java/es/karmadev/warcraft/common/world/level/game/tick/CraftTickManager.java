package es.karmadev.warcraft.common.world.level.game.tick;

import es.karmadev.warcraft.api.world.level.game.tick.FutureGameTick;
import es.karmadev.warcraft.api.world.level.game.tick.GameIntervalTask;
import es.karmadev.warcraft.api.world.level.game.tick.GameTickManager;
import es.karmadev.warcraft.common.world.level.game.CraftGame;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CraftTickManager implements GameTickManager {

    private final long tickJump;
    private final CraftGame game;

    private long tick;
    private long nextTick;

    private final Map<Long, FutureGameTick> futureTicks = new HashMap<>();
    private final Map<Long, GameIntervalTask> nextTicks = new ConcurrentHashMap<>();

    public CraftTickManager(final long tickJump, final CraftGame game) {
        this.tickJump = tickJump;
        this.game = game;

        this.tick = 0;
        this.nextTick = tickJump;
    }

    /**
     * Get the current tick
     * in the game
     *
     * @return the current game tick
     */
    @Override
    public long getTick() {
        return this.tick;
    }

    /**
     * Get the next tick
     * in the game
     *
     * @return the next game tick
     */
    @Override
    public long getNextTick() {
        return this.nextTick;
    }

    /**
     * Execute the game tick manage
     * tick function. This task should only
     * run tick tasks and change current
     * tick
     */
    @Override
    public void tickFunction() {
        long tickStore = this.tick;
        GameIntervalTask task = this.nextTicks.get(tickStore);
        if (task != null) {
            this.nextTicks.put(this.tick + task.getInterval(), task);
            task.run();
        }

        if (this.tick == nextTick) {
            FutureGameTick tickTask = this.futureTicks.remove(this.tick++);
            if (tickTask != null) {
                tickTask.run();
            }

            nextTick += tickJump;
            if (tickTask != null && tickTask.isPreventDefault())
                this.nextTicks.remove(tickStore);

            this.game.runTick();
        } else {
            this.nextTicks.remove(tickStore);
        }
    }

    /**
     * Get if the game is currently synchronized
     * with the game tick
     *
     * @return if the game is synchronized
     */
    @Override
    public boolean isSynchronized() {
        return this.tick == (this.nextTick - this.tickJump) ||
                this.nextTicks.containsKey(this.tick);
    }

    /**
     * Adds an interval to
     * the game
     *
     * @param interval the interval
     * @param task     the interval task
     */
    @Override
    public void addInterval(final long interval, final Runnable task) {
        nextTicks.put(this.tick + interval, new GameIntervalTask(interval, task));
    }

    /**
     * Schedule an action for the
     * next tick in the game
     *
     * @param jumps    the amount of jumps for
     *                 the next tick
     * @param tickTask the task to run
     */
    @Override
    public void nextTick(final long jumps, final FutureGameTick tickTask) {
        long ticksToNextTick = this.nextTick - this.tick;
        long predictedTick = this.tick + (this.tickJump * tick);

        if (ticksToNextTick != this.tickJump) {
            predictedTick = (this.tick + (this.tickJump * (tick - 1)))
                    + ticksToNextTick;
        }

        this.futureTicks.put(predictedTick, tickTask);
    }

    /**
     * Delay the tick prosecution
     *
     * @param jumps the amount of tick
     *              jumps to perform
     */
    @Override
    public void delayTick(long jumps) {
        this.nextTick += (this.tickJump * Math.max(1, jumps));
    }
}
