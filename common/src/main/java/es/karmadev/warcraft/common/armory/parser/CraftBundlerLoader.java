package es.karmadev.warcraft.common.armory.parser;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.warcraft.api.WarCraft;
import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.armory.weapon.type.WeaponType;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.CraftArmory;
import es.karmadev.warcraft.common.armory.weapon.*;

import java.util.*;

public class CraftBundlerLoader implements ArmoryLoader {

    private final Map<WeaponType, Set<Weapon>> weapons = new LinkedHashMap<>();
    private final JsonObject bundler;

    public CraftBundlerLoader(final JsonObject bundler) {
        this.bundler = bundler;
    }

    @Override
    public void mapTo(final CraftArmory armory) throws InvalidWeaponException {
        if (this.bundler.hasChild("gun") && this.bundler.getChild("gun").isArrayType())
            map(bundler.asArray("gun"), WeaponType.GUN, armory);

        if (this.bundler.hasChild("explosive") && this.bundler.getChild("explosive").isArrayType())
            map(this.bundler.asArray("explosive"), WeaponType.EXPLOSIVE, armory);

        if (this.bundler.hasChild("melee") && this.bundler.getChild("melee").isArrayType())
            map(this.bundler.asArray("melee"), WeaponType.MELEE, armory);

        if (this.bundler.hasChild("throwable") && this.bundler.getChild("throwable").isArrayType())
            map(this.bundler.asArray("throwable"), WeaponType.THROWABLE, armory);

        if (this.bundler.hasChild("construction") && this.bundler.getChild("construction").isArrayType())
            mapConstruction(this.bundler.asArray("construction"), armory);

        if (this.weapons.isEmpty() || this.weapons.values().stream().allMatch(Set::isEmpty))
            return;

        armory.populate(this.weapons);
    }

    private void map(final JsonArray container, final WeaponType type, final CraftArmory target) throws InvalidWeaponException {
        WarCraft plugin = WarCraft.getInstance();

        for (JsonInstance el : container) {
            if (!el.isObjectType()) continue;
            JsonObject object = el.asObject();

            if (!object.hasChild("name") || !object.getChild("name").isNativeType()) continue;

            JsonNative nameNative = object.asNative("name");
            if (!nameNative.isString()) continue;

            String name = nameNative.getString();
            Weapon wp;
            switch (type) {
                case GUN:
                    wp = new CraftGun(target, name, object);
                    break;
                case EXPLOSIVE:
                    wp = new CraftExplosive(target, name, object);
                    break;
                case THROWABLE:
                    Weapon existingWeapon = this.weapons.values().stream()
                            .flatMap(Set::stream)
                            .filter((weapon) -> weapon.getName().equals(name))
                            .findAny().orElse(null);
                    if (existingWeapon == null) continue;

                    wp = new CraftThrowable(existingWeapon, object);
                    break;
                case MELEE:
                default:
                    wp = new CraftMelee(target, name, object);
            }

            if (plugin.getArmoryManager().getWeapons().contains(wp)) continue;
            this.weapons.computeIfAbsent(type, (s) -> new LinkedHashSet<>())
                    .add(wp);
        }
    }

    private void mapConstruction(final JsonArray container, final CraftArmory target) throws InvalidWeaponException {
        WarCraft plugin = WarCraft.getInstance();

        for (JsonInstance el : container) {
            if (!el.isObjectType()) continue;
            JsonObject object = el.asObject();

            if (!object.hasChild("name") || !object.getChild("name").isNativeType()) continue;
            if (!object.hasChild("levels") || !object.getChild("levels").isArrayType()) continue;

            JsonNative nameNative = object.asNative("name");
            JsonArray levels = object.asArray("levels");

            if (!nameNative.isString()) continue;

            List<JsonObject> levelValues = new ArrayList<>();
            for (JsonInstance instance : levels) {
                if (!instance.isObjectType()) continue;
                levelValues.add(instance.asObject());
            }

            CraftConstruction construction = new CraftConstruction(target, nameNative.getString(),
                    object, levelValues);

            if (plugin.getArmoryManager().getWeapons().contains(construction)) continue;
            this.weapons.computeIfAbsent(WeaponType.CONSTRUCTION, (s) -> new LinkedHashSet<>())
                    .add(construction);
        }
    }
}
