package es.karmadev.warcraft.common.armory.weapon;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.armory.weapon.components.ComponentType;
import es.karmadev.warcraft.api.armory.weapon.components.WeaponComponent;
import es.karmadev.warcraft.api.armory.weapon.components.model.ModelType;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.api.armory.weapon.type.MeleeWeapon;
import es.karmadev.warcraft.api.exception.weapon.InvalidWeaponException;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.model.CraftModel;
import org.jetbrains.annotations.Contract;

import java.util.Collection;
import java.util.Collections;

/**
 * Represents a melee weapon
 */
public class CraftMelee extends CraftWeapon implements MeleeWeapon {

    private WeaponModel model;
    private double damage;
    private long speed;
    private boolean canStab;
    private int metalUsage;

    /**
     * Create the melee weapon
     *
     * @param armory the weapon armory
     * @param name the weapon name
     * @param weaponJson the weapon json data
     * @throws InvalidWeaponException if the weapon json is
     * invalid
     */
    public CraftMelee(final Armory armory, final String name, final JsonObject weaponJson) throws InvalidWeaponException {
        super(armory, name);
        map(weaponJson);
    }

    /**
     * Get the melee weapon damage
     *
     * @return the weapon damage
     */
    @Override
    public double getDamage() {
        return this.damage;
    }

    /**
     * Get the melee weapon usage
     * speed
     *
     * @return the weapon usage speed
     */
    @Override
    public long getSpeed() {
        return this.speed;
    }

    /**
     * Get if the weapon can
     * stab
     *
     * @return if the weapon can
     * perform stab damage
     */
    @Override
    public boolean canStab() {
        return this.canStab;
    }

    /**
     * Get the weapon metal usage, if
     * this value is over zero, it means
     * it's a wrench. Other values get
     * ignored
     *
     * @return the metal usage
     */
    @Override
    public int getMetalUsage() {
        return this.metalUsage;
    }

    /**
     * Get a weapon component by
     * component type
     *
     * @param type  the component type
     * @param cType the component object type
     * @return the weapon component
     */
    @Override
    public <T extends WeaponComponent> T getComponent(final ComponentType type, final Class<T> cType) {
        if (!type.equals(ComponentType.MODEL))
            throw new IllegalArgumentException("Component " + type + " is not supported");

        return cType.cast(model);
    }

    /**
     * Get all the weapon components
     *
     * @return the weapon components
     */
    @Override
    public Collection<? extends WeaponComponent> getComponents() {
        return Collections.singleton(model);
    }

    /**
     * Get if the weapon has available
     * the specified component
     *
     * @param type the component type
     * @return if the weapon has teh component
     */
    @Override
    public boolean hasComponent(final ComponentType type) {
        return type.equals(ComponentType.MODEL);
    }

    @Override
    public String toString() {
        return String.format("CraftMelee{\"armory\": %s, \"uuns\": \"%s\", \"name\": \"%s\", \"model type\": %s, \"model\": \"%s\", " +
                        "\"damage\": %.2f, \"speed\": %d, \"can stab\": %s, \"metal usage\": %d}",
                this.armory, this.getUUNS(), this.name, this.model.getType(), this.model.getModelData(),
                this.damage, this.speed, this.canStab, this.metalUsage
        );
    }

    /**
     * Map the weapon
     *
     * @param weaponJson the weapon json
     * @throws InvalidWeaponException if the weapon is invalid
     */
    @Contract("null -> fail")
    private void map(final JsonObject weaponJson) throws InvalidWeaponException {
        if (weaponJson == null)
            throw new InvalidWeaponException(this.armory, this.name, "Cannot parse melee weapon for invalid data");

        ensureNotMissing(weaponJson,
                "description", "model",
                "damage", "speed", "can_stab", "metal_usage"
        );

        ensurePropType(weaponJson, JsonObject.class, "model");
        ensurePropType(weaponJson, JsonArray.class, "description");
        ensurePropType(weaponJson, JsonNative.class,
                "model.type", "model.name",
                "damage", "speed", "can_stab", "metal_usage");

        JsonArray description = weaponJson.asArray("description");
        JsonNative modelType = weaponJson.asNative("model.type");
        JsonNative modelName = weaponJson.asNative("model.name");
        JsonNative damage = weaponJson.asNative("damage");
        JsonNative speed = weaponJson.asNative("speed");
        JsonNative canStab = weaponJson.asNative("can_stab");
        JsonNative metalUsage = weaponJson.asNative("metal_usage");

        ensureString(modelType, modelName);
        ensureNumber(damage, metalUsage);
        ensureBoolean(canStab);

        mapDescription(description);
        this.model = new CraftModel(ModelType.valueOf(modelType.getString().toUpperCase()),
                modelName.getString());

        this.damage = damage.getDouble();
        this.speed = CraftComponent.parseTime(speed.asString());
        this.canStab = canStab.getBoolean();
        this.metalUsage = metalUsage.getInteger();
    }
}
