package es.karmadev.warcraft.common.armory.weapon.components.model;

import es.karmadev.warcraft.api.armory.weapon.components.model.ModelType;
import es.karmadev.warcraft.api.armory.weapon.components.model.WeaponModel;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import org.jetbrains.annotations.Contract;

public class CraftModel extends CraftComponent implements WeaponModel {

    private final ModelType type;
    private final String name;

    @Contract("null, null -> fail")
    public CraftModel(final ModelType type, final String name) {
        if (type == null)
            throw new IllegalArgumentException("Cannot create model with invalid type");
        if (name == null || name.trim().isEmpty())
            throw new IllegalArgumentException("Cannot create model with invalid name");

        this.type = type;
        this.name = name;
    }

    /**
     * Get the weapon model type
     *
     * @return the weapon model type
     */
    @Override
    public ModelType getType() {
        return this.type;
    }

    /**
     * Get the weapon model data
     *
     * @return the weapon model data
     */
    @Override
    public String getModelData() {
        return this.name;
    }
}
