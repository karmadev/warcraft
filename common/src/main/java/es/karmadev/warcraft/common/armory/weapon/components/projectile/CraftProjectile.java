package es.karmadev.warcraft.common.armory.weapon.components.projectile;

import es.karmadev.warcraft.api.projectile.ProjectileType;
import es.karmadev.warcraft.api.armory.weapon.components.projectile.WeaponProjectile;
import es.karmadev.warcraft.api.armory.weapon.components.sound.WeaponSoundSet;
import es.karmadev.warcraft.common.armory.weapon.components.CraftComponent;
import es.karmadev.warcraft.common.armory.weapon.components.CraftSoundSet;
import org.jetbrains.annotations.Contract;

public class CraftProjectile extends CraftComponent implements WeaponProjectile {

    private final ProjectileType type;
    private final String name;
    private final double speed, damage, damagePenalty;
    private final int distance, distancePenalty;
    private final WeaponSoundSet soundSet;

    @Contract("null, null, _, _, _, _, _, null -> fail")
    public CraftProjectile(final ProjectileType type, final String name, final double speed, final int distance,
                           final int distancePenalty, final double damage, final double damagePenalty,
                           final CraftSoundSet soundSet) {
        if (type == null)
            throw new IllegalArgumentException("Cannot create projectile of invalid type");
        if (name == null || name.trim().isEmpty())
            throw new IllegalArgumentException("Cannot create projectile of invalid name");
        if (soundSet == null)
            throw new IllegalArgumentException("Cannot create projectile of invalid soundset");

        this.type = type;
        this.name = name;
        this.speed = speed;
        this.distance = distance;
        this.distancePenalty = distancePenalty;
        this.damage = damage;
        this.damagePenalty = damagePenalty;
        this.soundSet = soundSet.atComponent(this);
    }

    /**
     * Get the weapon projectile type
     *
     * @return the projectile type
     */
    @Override
    public ProjectileType getType() {
        return this.type;
    }

    /**
     * Get the weapon projectile name
     *
     * @return the weapon projectile name
     */
    @Override
    public String getProjectileName() {
        return this.name;
    }

    /**
     * Get the projectile speed
     * multiplier
     *
     * @return the speed multiplier
     */
    @Override
    public double getSpeed() {
        return this.speed;
    }

    /**
     * Get the projectile effective distance
     *
     * @return the effective distance in where
     * the projectile deals 100% of its damage
     */
    @Override
    public int getEffectiveDistance() {
        return this.distance;
    }

    /**
     * Get the projectile distance penalty, in
     * where the damage gets reduced for each
     * amount of meters travelled
     *
     * @return the distance penalty
     */
    @Override
    public int getDistancePenalty() {
        return this.distancePenalty;
    }

    /**
     * Get the weapon base damage
     *
     * @return the base damage
     */
    @Override
    public double getDamage() {
        return this.damage;
    }

    /**
     * Get the weapon damage penalty for
     * each {@link #getDistancePenalty()}
     *
     * @return the damage penalty
     */
    @Override
    public double getDamagePenalty() {
        return this.damagePenalty;
    }

    /**
     * Get the weapon projectile sounds
     *
     * @return the projectile sounds
     */
    @Override
    public WeaponSoundSet getSounds() {
        return this.soundSet;
    }
}