armory = warcraft.armory()
result = try(function()
    test = armory.lookup('warcraft=default_package')
    return test
end, function(_)
    return nil
end)

return result ~= nil