package es.karmadev.craft.impl;

import es.karmadev.craft.impl.manager.ArmoryManagerTest;
import es.karmadev.warcraft.api.WarCraft;
import es.karmadev.warcraft.api.plugin.armory.ArmoryManager;
import es.karmadev.warcraft.api.plugin.projectile.ProjectileManager;
import es.karmadev.warcraft.api.plugin.scheduler.Scheduled;
import es.karmadev.warcraft.api.plugin.scheduler.Scheduler;
import es.karmadev.warcraft.api.world.level.game.stats.GameStats;
import org.bukkit.OfflinePlayer;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class TestCraftWar extends WarCraft {

    private final ArmoryManager armoryManager = new ArmoryManagerTest();
    private final Path weaponsDirectory;

    public TestCraftWar(final Path weaponsDirectory) {
        this.weaponsDirectory = weaponsDirectory;
        this.register();
    }

    /**
     * Get the plugin weapons directory
     *
     * @return the plugin weapons
     * directory
     */
    @Override
    public Path getWeaponsDirectory() {
        return this.weaponsDirectory;
    }

    /**
     * Get the plugin scheduler
     *
     * @return the plugin scheduler
     */
    @Override
    public Scheduler getScheduler() {
        return new Scheduler() {
            @Override
            public void executeSync(Runnable task) {

            }

            @Override
            public void executeAsync(final Runnable task) {
                CompletableFuture.runAsync(task);
            }

            @Override
            public void scheduleSync(Consumer<Scheduled> runner, long delay, TimeUnit unit) {

            }

            @Override
            public void scheduleAsync(Consumer<Scheduled> runner, long delay, TimeUnit unit) {

            }

            @Override
            public void scheduleRepeatingSync(Consumer<Scheduled> runner, long delay, long interval, TimeUnit unit) {

            }

            @Override
            public void scheduleRepeatingAsync(Consumer<Scheduled> runner, long delay, long interval, TimeUnit unit) {

            }

            @Override
            public boolean cancelTask(long id) {
                return false;
            }

            @Override
            public void cancelNow(long id) {

            }

            @Override
            public boolean isRunning(long id) {
                return false;
            }

            @Override
            public boolean isCancelled(long id) {
                return false;
            }

            @Override
            public void cancelAllTasks() {

            }

            @Override
            public Collection<? extends Scheduled> getTasks() {
                return Collections.emptyList();
            }
        };
    }

    /**
     * Get the plugin projectile manager
     *
     * @return the projectile manager
     */
    @Override
    public ProjectileManager getProjectileManager() {
        return null;
    }

    /**
     * Get the plugin armory manager
     *
     * @return the armory manager
     */
    @Override
    public ArmoryManager getArmoryManager() {
        return this.armoryManager;
    }

    /**
     * Logs a message
     *
     * @param message      the message
     * @param replacements the message replacements
     */
    @Override
    public void logDebug(String message, Object... replacements) {
        System.out.println(format("[{}] {}", "DEBUG", format(message, replacements)));
    }

    /**
     * Logs a message
     *
     * @param message      the message
     * @param replacements the message replacements
     */
    @Override
    public void logInfo(String message, Object... replacements) {
        System.out.println(format("[{}] {}", "INFO", format(message, replacements)));
    }

    /**
     * Logs a message
     *
     * @param message      the message
     * @param replacements the message replacements
     */
    @Override
    public void logWarning(String message, Object... replacements) {
        System.out.println(format("[{}] {}", "WARNING", format(message, replacements)));
    }

    /**
     * Logs a message
     *
     * @param message      the message
     * @param replacements the message replacements
     */
    @Override
    public void logSevere(String message, Object... replacements) {
        System.out.println(format("[{}] {}", "SEVERE", format(message, replacements)));
    }

    /**
     * Logs a message
     *
     * @param error        the error
     * @param message      the message
     * @param replacements the message replacements
     */
    @Override
    public void logError(Throwable error, String message, Object... replacements) {
        System.out.println(format("[{}] {}", "ERROR", format(message, replacements)));
        error.printStackTrace();
    }

    /**
     * Get the stats of a player
     *
     * @param player the player to get
     *               stats from
     * @return the player stats
     */
    @Override
    public GameStats getStats(OfflinePlayer player) {
        return null;
    }

    private String format(final String message, final Object... replacements) {
        if (replacements == null || replacements.length == 0) return message;

        StringBuilder builder = new StringBuilder(message);
        int rIndex = 0;
        int index = builder.indexOf("{}");

        while (index != -1 || rIndex < replacements.length) {
            builder.replace(index, index + 2, String.valueOf(replacements[rIndex++]));
            index = builder.indexOf("{}");
        }

        return builder.toString();
    }
}
