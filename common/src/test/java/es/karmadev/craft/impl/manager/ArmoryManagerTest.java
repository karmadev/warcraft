package es.karmadev.craft.impl.manager;

import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.plugin.armory.ArmoryManager;
import es.karmadev.warcraft.api.plugin.armory.WeaponManager;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ArmoryManagerTest implements ArmoryManager {

    private final Set<Armory> armories = new HashSet<>();
    private final WeaponManagerTest weaponManager = new WeaponManagerTest();

    /**
     * Register an armory into the
     * plugin
     *
     * @param armory the armory to register
     * @return if the armory was registered
     */
    @Override
    public boolean register(final Armory armory) {
        if (this.armories.add(armory)) {
            this.weaponManager.addAll(armory.getWeapons());
            return true;
        }

        return false;
    }

    /**
     * Get if the armory manager contains
     * the armory with the specified name
     *
     * @param name the armory name
     * @return if there's an armory with that
     * name registered
     */
    @Override
    public boolean contains(final UUNS name) {
        return this.armories.stream().anyMatch((armory) -> armory.getUUNS().equals(name));
    }

    /**
     * Get all the registered armories
     *
     * @return the registered armories
     */
    @Override
    public Collection<Armory> getArmories() {
        return Collections.unmodifiableCollection(this.armories);
    }

    /**
     * Get an armory by its name
     *
     * @param name the armory name
     * @return the armory
     */
    @Override
    public @Nullable Armory getArmory(final UUNS name) {
        return this.armories.stream().filter((armory) -> armory.getUUNS().equals(name))
                .findAny().orElse(null);
    }

    /**
     * Get the globally registered
     * weapons
     *
     * @return the weapons
     */
    @Override
    public WeaponManager getWeapons() {
        return this.weaponManager;
    }
}
