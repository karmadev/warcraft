package es.karmadev.craft.impl.manager;

import es.karmadev.warcraft.api.armory.weapon.Weapon;
import es.karmadev.warcraft.api.plugin.armory.WeaponManager;
import es.karmadev.warcraft.api.util.uuns.UUNS;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class WeaponManagerTest implements WeaponManager {

    private final Collection<Weapon> weapons = new HashSet<>();

    /**
     * Add all the weapons from the collection
     * to the weapon manager
     *
     * @param weapons the weapons to add
     */
    protected void addAll(final Collection<Weapon> weapons) {
        this.weapons.addAll(weapons);
    }

    /**
     * Get if the weapon manager contains
     * a weapon with the specified name
     *
     * @param name the weapon name
     * @return if there's a weapon with the
     * specified name
     */
    @Override
    public boolean contains(final UUNS name) {
        return this.weapons.stream().anyMatch((weapon -> weapon.getUUNS().equals(name)));
    }

    /**
     * Get all the weapons
     *
     * @return the weapons
     */
    @Override
    public Collection<Weapon> getAll() {
        return Collections.unmodifiableCollection(this.weapons);
    }
}
