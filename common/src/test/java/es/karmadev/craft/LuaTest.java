package es.karmadev.craft;

import es.karmadev.craft.impl.TestCraftWar;
import es.karmadev.warcraft.api.WarCraft;
import es.karmadev.warcraft.api.armory.Armory;
import es.karmadev.warcraft.api.script.LuaPackage;
import es.karmadev.warcraft.api.script.ScriptClassLoader;
import es.karmadev.warcraft.api.util.uuns.UUNS;
import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.common.armory.CraftArmory;
import es.karmadev.warcraft.common.lua.CommonLuaPackage;
import es.karmadev.warcraft.common.lua.event.LuaEventHandler;
import es.karmadev.warcraft.common.lua.proxy.ObjectProxy;
import es.karmadev.warcraft.common.lua.proxy.ProxyLuaPackage;
import es.karmadev.warcraft.common.world.level.CraftTeam;
import io.github.taoguan.luaj.*;
import io.github.taoguan.luaj.lib.OneArgFunction;
import io.github.taoguan.luaj.lib.TwoArgFunction;
import io.github.taoguan.luaj.lib.VarArgFunction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import party.iroiro.luajava.JuaAPI;
import party.iroiro.luajava.luaj.LuaJ;
import party.iroiro.luajava.luaj.LuaJNatives;
import party.iroiro.luajava.luaj.LuaJState;
import party.iroiro.luajava.luaj.values.JavaObject;
import party.iroiro.luajava.util.ClassUtils;
import party.iroiro.luajava.value.APILuaValue;

import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class LuaTest {

    private final static Path directory = Paths.get("")
            .resolve("src")
            .resolve("test")
            .resolve("resources")
            .toAbsolutePath();

    private final Path defaultPack = directory.resolve("armories")
            .resolve("default_package.json");

    private static ScriptClassLoader secureLoader;

    @BeforeAll
    public static void init() {
        ClassUtils.setAlias("lang", "java.lang");
        ClassUtils.setAlias("util", "java.util");

        LuaPackage luaPackage = new CommonLuaPackage();
        ProxyLuaPackage proxyLuaPackage = new ProxyLuaPackage();
        luaPackage.register();
        proxyLuaPackage.register();

        secureLoader = new ScriptClassLoader();
        secureLoader.release("org.bukkit");

        JuaAPI.defineImportClassLoader(secureLoader);
        LuaJNatives.setup();
    }

    @BeforeEach
    public void setup() {
        new TestCraftWar(directory);
    }

    @Test
    public void testThatArmoryCanBeAccessed() throws Exception {
        Armory armory = CraftArmory.fromFile(this.defaultPack);
        assertNotNull(armory);
        WarCraft plugin = WarCraft.getInstance();
        assertTrue(plugin.getArmoryManager().register(armory));

        Path file = directory.resolve("armory_example.lua");
        String raw = new String(Files.readAllBytes(file));

        try (LuaJ lua = new LuaJ()) {
            mapWarcraftFunction(secureLoader, lua);
            APILuaValue[] values = lua.execute(raw);

            assertEquals(1, values.length);

            APILuaValue fVal = values[0];
            assertNotNull(fVal);

            Object jObj = fVal.toJavaObject();
            assertNotNull(jObj);

            assertInstanceOf(Boolean.class, jObj);
            assertTrue((Boolean) jObj);
        }
    }

    private static void mapWarcraftFunction(final ClassLoader loader, final LuaJ lua) {
        Map<String, LuaValue> functions = new HashMap<>();

        functions.put("uuns", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                if (args.narg() == 0)
                    return null;

                if (args.narg() == 1) {
                    LuaValue arg1 = args.arg(1);
                    if (!arg1.isstring())
                        return null;

                    return JuaAPI.valueOfObject(lua.getId(), UUNS.parse(arg1.tojstring()));
                } else {
                    LuaValue arg1 = args.arg(1);
                    LuaValue arg2 = args.arg(1);

                    if (!arg1.isstring() || !arg2.isstring())
                        return null;

                    return JuaAPI.valueOfObject(lua.getId(), UUNS.create(arg1.tojstring(), arg2.tojstring()));
                }
            }
        });
        functions.put("arrayOf", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                if (arg instanceof JavaObject) {
                    JavaObject object = (JavaObject) arg;
                    Object instance = object.m_instance;

                    return JuaAPI.valueOfArray(lua.getId(), instance);
                }

                return null;
            }
        });
        functions.put("team", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, final LuaValue arg2) {
                if (!arg1.isstring())
                    return null;

                CraftTeam tm;
                if (arg2.isnumber()) {
                    tm = new CraftTeam(arg1.tojstring(), arg2.checknumber().toint());
                } else {
                    tm = new CraftTeam(arg1.tojstring());
                }

                LuaJState state = JuaAPI.getLuaState(lua.getId());
                state.pushFrame();

                try {
                    Object proxy = ObjectProxy.ofInstance(tm, new Class[]{Team.class});
                    return JuaAPI.valueOfObject(lua.getId(), proxy);
                } catch (ReflectiveOperationException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        functions.put("armory", new LuaFunction() {
            @Override
            public LuaValue call() {
                LuaTable subMethods = new LuaTable();
                subMethods.set("lookup", new OneArgFunction() {
                    @Override
                    public LuaValue call(LuaValue arg) {
                        if (!arg.isstring()) {
                            if (arg instanceof JavaObject) {
                                JavaObject jObj = (JavaObject) arg;
                                Object obj = jObj.m_instance;

                                if (obj instanceof UUNS)
                                    arg = LuaString.valueOf(((UUNS) obj).toString());
                            }
                        }

                        if (!arg.isstring()) return null;
                        String raw = arg.tojstring();

                        UUNS uuns = UUNS.parse(raw);
                        WarCraft plugin = WarCraft.getInstance();

                        Armory armory = plugin.getArmoryManager().getArmory(uuns);
                        try {
                            Object proxy = ObjectProxy.ofInstance(armory, new Class[]{Armory.class});
                            return JuaAPI.valueOfObject(lua.getId(), proxy);
                        } catch (ReflectiveOperationException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                });

                return subMethods;
            }
        });
        functions.put("handler", new LuaFunction() {
            @Override
            public LuaValue call() {
                LuaJState state = JuaAPI.getLuaState(lua.getId());
                state.pushFrame();

                lua.pushJavaClass(LuaEventHandler.class);
                return state.toLuaValue(1).arg1();
            }
        });
        functions.put("handle", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, LuaValue arg2) {
                if (!arg1.isfunction() || !arg2.isstring())
                    return null;

                LuaClosure closure = (LuaClosure) arg1;
                if (closure.p.numparams != 1)
                    return null;

                LuaTable callback = new LuaTable();
                callback.set("handle", arg1);

                String className = String.format("es.karmadev.warcraft.common.lua.event.proxy.%s",
                        arg2.tojstring().replace("/", "."));

                LuaJState state = JuaAPI.getLuaState(lua.getId());
                state.pushFrame();
                try {
                    Object proxy = ObjectProxy.forEventHandler(lua, arg1, Class.forName(className));
                    return JuaAPI.valueOfObject(lua.getId(), proxy);
                } catch (ReflectiveOperationException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        functions.put("request", new OneArgFunction() {
            @Override
            public LuaValue call(final LuaValue arg) {
                if (!arg.isstring()) return null;

                String name = arg.tojstring();
                LuaJState state = JuaAPI.getLuaState(lua.getId());
                try {
                    Class<?> clazz = ClassUtils.forName(name, loader);
                    state.pushFrame();
                    lua.pushJavaClass(clazz);
                    return state.toLuaValue(1).arg1();
                } catch (ClassNotFoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        functions.put("throws", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                Class<Throwable> target = Throwable.class;
                Constructor<Throwable> constructor;

                try {
                    switch (args.narg()) {
                        case 0:
                            constructor = target.getDeclaredConstructor();
                            break;
                        case 1:
                            if (args.arg(1).isstring()) {
                                constructor = target.getDeclaredConstructor(String.class);
                            } else if (args.arg(1).isuserdata(Throwable.class)) {
                                constructor = target.getDeclaredConstructor(Throwable.class);
                            } else {
                                return LuaNil.valueOf("No matching throwable constructor");
                            }
                            break;
                        case 2:
                            if (args.arg(1).isstring() && args.arg(2).isuserdata(Throwable.class)) {
                                constructor = target.getDeclaredConstructor(String.class, Throwable.class);
                            } else {
                                return LuaNil.valueOf("No matching throwable constructor");
                            }
                            break;
                        case 4:
                            if (args.arg(1).isstring() && args.arg(2).isuserdata(Throwable.class) &&
                                    args.arg(3).isboolean() && args.arg(4).isboolean()) {
                                constructor = target.getDeclaredConstructor(String.class, Throwable.class,
                                        boolean.class, boolean.class);
                            } else {
                                return LuaNil.valueOf("No matching throwable constructor");
                            }
                            break;
                        default:
                            if (args.narg() > 4) {
                                if (args.arg(1).isstring() && args.arg(2).isuserdata(Throwable.class) &&
                                        args.arg(3).isboolean() && args.arg(4).isboolean()) {
                                    constructor = target.getDeclaredConstructor(String.class, Throwable.class,
                                            boolean.class, boolean.class);
                                } else {
                                    return LuaNil.valueOf("No matching throwable constructor");
                                }
                            } else {
                                if (args.arg(1).isstring() && args.arg(2).isuserdata(Throwable.class)) {
                                    constructor = target.getDeclaredConstructor(String.class, Throwable.class);
                                } else {
                                    return LuaNil.valueOf("No matching throwable constructor");
                                }
                            }
                            break;
                    }

                    Throwable instance;
                    if (args.arg(1).isnil()) {
                        instance = constructor.newInstance();
                    } else if (args.arg(1).isstring()) {
                        if (args.arg(2).isnil()) {
                            instance = constructor.newInstance(args.arg(1).tojstring());
                        } else if (args.arg(3).isnil()) {
                            instance = constructor.newInstance(args.arg(1).tojstring(),
                                    args.arg(2).touserdata(Throwable.class));
                        } else {
                            instance = constructor.newInstance(args.arg(1).tojstring(),
                                    args.arg(2).touserdata(Throwable.class),
                                    args.arg(3).toboolean(),
                                    args.arg(3).toboolean());
                        }
                    } else {
                        instance = constructor.newInstance(args.arg(1).touserdata(Throwable.class));
                    }

                    return JuaAPI.valueOfObject(lua.getId(), instance);
                } catch (ReflectiveOperationException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        JuaAPI.registerLibImpl(lua.getId(), "warcraft", functions);

        Globals globals = lua.getLuaNative().getGlobals(lua.getId());
        globals.set("instanceof", new TwoArgFunction() {
            @Override
            public LuaValue call(final LuaValue arg1, final LuaValue arg2) {
                Object uData = arg1.touserdata();
                Object uData2 = arg2.touserdata();

                Class<?> class1 = uData instanceof Class<?> ? (Class<?>) uData : uData.getClass();
                Class<?> class2 = uData2 instanceof Class<?> ? (Class<?>) uData2 : uData2.getClass();

                return LuaBoolean.valueOf(class1.isAssignableFrom(class2) ||
                        class2.isAssignableFrom(class1) || class1.equals(class2) ||
                        class1.isInstance(uData2) || class2.isInstance(uData));
            }
        });

        globals.set("Exception", JuaAPI.valueOfObject(lua.getId(), Throwable.class));
        globals.set("throwable", new LuaFunction() {
            @Override
            public LuaValue call() {
                return JuaAPI.toLuaImport(lua.getId(), Throwable.class);
            }
        });

        globals.set("try", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                if (args.narg() < 2)
                    return LuaNil.valueOf(false);


                if (!args.arg(1).isfunction() || !args.arg(2).isfunction())
                    return LuaNil.valueOf(false);

                if (args.narg() >= 3 && !args.arg(3).isfunction())
                    return LuaNil.valueOf(false);

                LuaFunction tryBlock = (LuaFunction) args.arg(1);
                LuaFunction catchBlock = (LuaFunction) args.arg(2);

                try {
                    return tryBlock.call();
                } catch (Throwable ex) {
                    return catchBlock.call(JuaAPI.valueOfObject(lua.getId(), ex));
                } finally {
                    if (!args.arg(3).isnil())
                        args.arg(3).call();
                }
            }
        });
    }
}
