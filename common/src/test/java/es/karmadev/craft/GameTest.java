package es.karmadev.craft;

import es.karmadev.warcraft.api.world.level.Team;
import es.karmadev.warcraft.api.world.level.game.type.PayloadGame;
import es.karmadev.warcraft.common.world.level.CraftTeam;
import es.karmadev.warcraft.common.world.level.game.type.CraftPayloadGame;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {

    private static Team blu;
    private static Team red;

    @BeforeAll
    public static void setup() {
        blu = new CraftTeam("Blu");
        red = new CraftTeam("Red");
    }

    @Test
    public void testThatGameWithLessThanTwoPlayersCausesAnError() {
        assertThrows(IllegalStateException.class, () -> new CraftPayloadGame("test", 30, 1,
                TimeUnit.MINUTES.toMillis(15), blu) {});
    }

    @Test
    public void testThatGameWithLessThanAMinuteCausesAnError() {
        assertThrows(IllegalStateException.class, () -> new CraftPayloadGame("test", 30, 32,
                TimeUnit.SECONDS.toMillis(30), blu) {});
    }

    @Test
    public void testThatGameCanBeCreatedWithOneTeam() {
        assertDoesNotThrow(() -> new CraftPayloadGame("test", 30, 32,
                TimeUnit.MINUTES.toMillis(15), blu) {});
    }

    @Test
    public void testThatGameCanBeCreatedWithTwoTeams() {
        assertDoesNotThrow(() -> new CraftPayloadGame("test", 30, 32,
                TimeUnit.MINUTES.toMillis(15), blu, red) {});
    }

    @Test
    public void testGameSetup() {
        PayloadGame game = new CraftPayloadGame("test", 30, 32,
                TimeUnit.MINUTES.toMillis(15), blu, red) {};

        AtomicInteger lastPre = new AtomicInteger(game.getPrepareRemaining());
        AtomicBoolean running = new AtomicBoolean(true);

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                game.tick();
                if (lastPre.get() != game.getPrepareRemaining()) {
                    System.out.printf("Time: %d%n", lastPre.get());
                    lastPre.set(game.getPrepareRemaining());
                }

                if (game.isPlaying()) {
                    timer.cancel();
                    running.set(false);
                }
            }
        };

        timer.scheduleAtFixedRate(task, 1, 1);
        while (running.get()) {
            synchronized (Thread.currentThread()) {
                try {
                    Thread.currentThread().wait(1);
                } catch (Throwable ignored) {}
            }
        }

        assertEquals(0, game.getPrepareRemaining());
    }
}
