package es.karmadev.craft;

import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.craft.impl.TestCraftWar;
import es.karmadev.warcraft.common.armory.CraftArmory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class ArmoryTest {

    private final static Path directory = Paths.get("")
            .resolve("src")
            .resolve("test")
            .resolve("resources")
            .toAbsolutePath();

    private final Path defaultPack = directory.resolve("armories")
            .resolve("default_package.json");

    private final JsonObject weaponPackageJsonObject = JsonReader
            .parse("{\"name\":\"Default Package\",\"version\":\"1.0.0\",\"api_version\":1,\"description\":[\"@name weapon package\",\"By @author\",\"Version: @version\"],\"uuns\":{\"path\":\"warcraft\",\"value\":\"Default Package\"},\"author\":{\"name\":\"KarmaDev\",\"site\":\"https://karmadev.es/\",\"contact\":\"karmadev.es@gmail.com\"},\"bundled\":false,\"configuration\":{\"path\":\"default\",\"structure\":[{\"dir\":\"weapons\",\"type\":\"gun\"},{\"dir\":\"explosives\",\"type\":\"explosive\"},{\"dir\":\"throwable\",\"type\":\"throwable\"},{\"dir\":\"melee\",\"type\":\"melee\"},{\"dir\":\"buildings\",\"type\":\"construction\"}]}}"
                    .getBytes()).asObject();

    @BeforeEach
    public void init() {
        new TestCraftWar(directory);
    }

    @Test
    public void testArmoryParsesJsonObjet() {
        assertDoesNotThrow(() -> CraftArmory.fromJson(this.weaponPackageJsonObject));
    }

    @Test
    public void testArmoryParsesFile() {
        assertDoesNotThrow(() -> CraftArmory.fromFile(this.defaultPack));
    }

    @Test
    public void testArmoryParsedJsonObject_IsNot_Null() throws Exception {
        CraftArmory armory = CraftArmory.fromJson(this.weaponPackageJsonObject);
        assertNotNull(armory);
    }

    @Test
    public void testArmoryParsedFile_IsNot_Null() throws Exception {
        CraftArmory armory = CraftArmory.fromFile(this.defaultPack);
        assertNotNull(armory);
    }
}
