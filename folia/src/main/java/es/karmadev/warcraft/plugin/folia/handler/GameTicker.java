package es.karmadev.warcraft.plugin.folia.handler;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.BoundingBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Minecraft tick synchronization
 * with WarCraft game ticks
 */
public abstract class GameTicker {

    private final Plugin plugin;
    private final World world;
    private final BoundingBox area;

    private final Set<Entity> tickingEntities = ConcurrentHashMap.newKeySet();

    public GameTicker(final Plugin plugin, final World world, final BoundingBox area) {
        this.plugin = plugin;
        this.world = world;
        this.area = area;
    }

    public CompletableFuture<Void> start(final Entity... entities) {
        return this.start().thenAccept((v) -> GameTicker.this.startEntity(entities));
    }

    private CompletableFuture<Void> start() {
        return detectChunksWithinArea(world, area).thenAccept(chunks -> {
            for (Chunk chunk : chunks) {
                plugin.getServer().getRegionScheduler().runAtFixedRate(plugin, world, chunk.getX(), chunk.getZ(),
                        (task) -> GameTicker.this.tickChunk(chunk), 0, 1);

                GameTicker.this.startEntity(chunk.getEntities());
            }
        });
    }

    private void startEntity(final Entity... entities) {
        if (entities == null || entities.length == 0) return;
        this.tickingEntities.addAll(Arrays.asList(entities));

        for (Entity entity : entities) {
            entity.getScheduler().runAtFixedRate(plugin, (task) -> this.tickEntity(entity), null,
                    0, 1);
        }
    }

    public abstract void tickChunk(final Chunk chunk);
    public abstract void tickEntity(final Entity entity);

    private CompletableFuture<List<Chunk>> detectChunksWithinArea(final World world, final BoundingBox area) {
        double minX = area.getMinX();
        double minZ = area.getMinZ();
        double maxX = area.getMaxX();
        double maxZ = area.getMaxZ();

        CompletableFuture<Chunk> chunkMinFuture = world.getChunkAtAsync((int) minX, (int) minZ);
        CompletableFuture<Chunk> chunkMaxFuture = world.getChunkAtAsync((int) maxX, (int) maxZ);

        return CompletableFuture.allOf(chunkMinFuture, chunkMaxFuture).thenCompose(v -> {
            try {
                Chunk minChunk = chunkMinFuture.get();
                Chunk maxChunk = chunkMaxFuture.get();

                int minChunkX = minChunk.getX();
                int minChunkZ = minChunk.getZ();
                int maxChunkX = maxChunk.getX();
                int maxChunkZ = maxChunk.getZ();

                List<CompletableFuture<Chunk>> futures = new ArrayList<>();
                for (int x = minChunkX; x <= maxChunkX; x++) {
                    for (int z = minChunkZ; z <= maxChunkZ; z++) {
                        futures.add(world.getChunkAtAsync(x, z));
                    }
                }

                return CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                        .thenApply(v2 -> futures.stream()
                                .map(CompletableFuture::join)
                                .collect(Collectors.toList()));
            } catch (InterruptedException | ExecutionException e) {
                CompletableFuture<List<Chunk>> failedFuture = new CompletableFuture<>();
                failedFuture.completeExceptionally(e);
                return failedFuture;
            }
        });
    }
}
