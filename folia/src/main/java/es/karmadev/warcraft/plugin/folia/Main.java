package es.karmadev.warcraft.plugin.folia;

import es.karmadev.warcraft.api.world.level.game.Game;
import es.karmadev.warcraft.common.world.level.CraftTeam;
import es.karmadev.warcraft.common.world.level.game.type.CraftPayloadGame;
import es.karmadev.warcraft.plugin.folia.handler.GameTicker;
import org.bukkit.Chunk;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        Game game = new CraftPayloadGame("test", 30, 24, 300,
                new CraftTeam("red"), new CraftTeam("blu"));
        new GameTicker(this, null, null) {
            @Override
            public void tickChunk(final Chunk chunk) {

            }

            @Override
            public void tickEntity(final Entity entity) {

            }
        }.start();

        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
