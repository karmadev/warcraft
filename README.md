# WarCraft

![WarCraft Logo](link_to_your_logo)

WarCraft is an open-source project developed by KarmaDev. It is a plugin which contains unique and well known game modes, with an unique weapon system and map maker. The project aims to include a bunch of game modes into a single plugin with tons of features without affecting performance too much.

## Features

- Completely unique game modes
- Build your own weapons without touching a line of code
- Supports lua for making scripts without touching java!
- Optimized for [Folia](https://github.com/PaperMC/Folia)
- [Make a suggestion?](https://gitlab.com/karmadev/warcraft/-/issues)

## Installation
To install the plugin all you need to do is to drag & drop the plugin .jar file into your plugins folder

## Compiling from source

Before continuing, please consider that:
- If you compile the plugin by yourself, you won't be provided of support of any kind
- When compiling the project by yourself, you automatically accept the [license agreement](https://www.gnu.org/licenses/agpl-3.0.en.html)

### Prerequisites

> If you have an intelligent IDE, such as IntellIJ, you can skip all those steps
>
> When using IntellIJ, you can download JDK automatically, and it comes with a gradle wraper

Before proceding with compilation, you must make sure you have all the required tools to start.
First of all, you need a valid jdk version, java version >~ 17, you can download jdk17 from [OpenJDK](https://www.oracle.com/java/technologies/downloads/#java17) 
or from [Azul Zulu](https://www.azul.com/downloads/?version=java-17-lts&package=jdk#zulu).

After you've downloaded and installed jdk, make sure it is accessible through the PATH (A.K System Environment Variables).
Then, download and install [gradle](https://gradle.org/install/).

Now, to compile the plugin, first clone the repository, or fork it and clone your fork
```git
git clone https://gitlab.com/karmadev/warcraft.git
```

Then navigate to the directory where you cloned the repository, and run the gradle command
```bash
cd warcraft
gradle clean build
```

## Contributing

We welcome contributions from everyone! Please see our [CONTRIBUTING.md](/CONTRIBUTING.md) file for guidelines on how to contribute to the project

## License

WarCraft is licensed under the [GNU Affero General Public License v3](https://www.gnu.org/licenses/agpl-3.0.en.html)

## Contact
If you have any questions, suggestions, or feedback, feel free to reach out to us:

- **Website** [KarmaDev](https://karmadev.es)
- **Donations** [Buy Me a Coffee](https://buymeacoffee.com/karmadev)
- **GitLab** [Open an Issue](https://gitlab.com/karmadev/warcraft/-/issues)

## Acknowledgements

**This project uses a modification of the following libraries**
- [LuaJava](https://github.com/gudzpoz/luajava)

# Warrany disclaimer
> THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.